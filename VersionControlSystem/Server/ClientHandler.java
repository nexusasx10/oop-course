import serializer.Serializer;
import threadManager.IWorker;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;


public class ClientHandler implements IWorker {

    private int id;
    private boolean connected;
    private Socket handlerSocket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private Serializer serializer;

    public ClientHandler(int id, Socket handlerSocket, Serializer serializer) {
        this.id = id;
        this.handlerSocket = handlerSocket;
        try {
            this.inputStream = handlerSocket.getInputStream();
            this.outputStream = handlerSocket.getOutputStream();
        } catch (IOException exception) {

        }
        this.serializer = serializer;
        this.connected = true;
    }

    private byte[] read(int length) {
        int pointer = 0;
        byte[] buffer = new byte[length];
        try {
            while (pointer != length) {
                if (this.inputStream.available() > 0) {
                    int value = this.inputStream.read();
                    buffer[pointer] = (byte) value;
                    pointer++;
                }
            }
        } catch (IOException exception) {
            try {
                this.inputStream.close();
            } catch (IOException exception2) {

            }
            this.connected = false;
            return null;
        }
        return buffer;
    }

    private Packet receive() {
        byte[] lengthBytes = read(4);
        if (lengthBytes == null) {
            return null;
        }
        int length = ByteBuffer.wrap(lengthBytes)
                .order(ByteOrder.LITTLE_ENDIAN)
                .getInt();
        byte[] buffer = read(length);
        if (buffer == null) {
            return null;
        }
        return (Packet) this.serializer.deserialize(buffer);
    }

    private void send(Packet packet) {
        byte[] data = serializer.serialize(packet);
        byte[] length = ByteBuffer
                .allocate(Integer.BYTES)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putInt(data.length)
                .array();
        try {
            this.outputStream.write(length);
            this.outputStream.write(data);
        } catch (IOException exception) {
            System.out.println("bad");
        }
    }

    @Override
    public void run() {
        while (this.connected) {
            parsePacket();
        }
    }

    private void parsePacket() {
        Packet packet = receive();
        if (packet == null) {
            return;
        }
        RepositoryManager repositoryManager = null;
        if (packet.repository != null) {
            repositoryManager = RepositoryManager.getManager(packet.repository);
        }
        if (packet.command.equals("connect")) {
            send(new Packet(null, null, new String[]{"confirm"}, null, null));
            System.out.println(String.format("Connection with client-%s established", this.id));
        } else if (packet.command.equals("disconnect")) {
            this.connected = false;
            try {
                this.inputStream.close();
                this.outputStream.close();
                this.handlerSocket.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        } else if (packet.command.equals("init")) {
            if (repositoryManager.createRepository()) {
                send(new Packet(null, packet.repository, new String[] {"confirm"}, null, null));
            } else {
                send(new Packet(null, packet.repository, new String[] {"deny"}, null, null));
            }
        } else if (packet.command.equals("commit")) {
            if (!repositoryManager.existsRepository()) {
                send(new Packet(null, packet.repository, new String[] {"deny"}, null, null));
                return;
            }
            ArrayList<FileContainer> files = new ArrayList<>();
            while (packet.flags != null && Arrays.asList(packet.flags).contains("moreFiles")) {
                files.add(packet.file);
                packet = receive();
            }
            if (packet.flags != null) {
                if (!Arrays.asList(packet.flags).contains("empty")) {
                    files.add(packet.file);
                }
            } else {
                files.add(packet.file);
            }
            if (repositoryManager.commit(files.toArray(new FileContainer[files.size()]))) {
                send(new Packet(null, packet.repository, new String[] {"confirm"}, null, null));
            } else {
                send(new Packet(null, packet.repository, new String[] {"deny"}, null, null));
            }
        } else if (packet.command.equals("pull")) {
            if (!repositoryManager.existsRepository()) {
                send(new Packet(null, packet.repository, new String[] {"deny"}, null, null));
                return;
            }
            FileContainer[] files = repositoryManager.pull(Integer.parseInt(packet.info));
            if (files.length == 0) {
                send(new Packet(null, packet.repository, new String[] {"confirm", "empty"}, null, null));
            }
            for (int i = 0; i < files.length; i++) {
                if (i == files.length - 1) {
                    send(new Packet(null, packet.repository, new String[] {"confirm"}, null, files[i]));
                } else {
                    send(new Packet(null, packet.repository, new String[] {"confirm", "moreFiles"}, null, files[i]));
                }
            }
        } else if (packet.command.equals("log")) {
            if (!repositoryManager.existsRepository()) {
                send(new Packet(null, packet.repository, new String[] {"deny"}, null, null));
                return;
            }
            String log = repositoryManager.log();
            send(new Packet(null, packet.repository, new String[] {"confirm"}, log, null));
        }
    }

    @Override
    public int getID() {
        return this.id;
    }

    @Override
    public String getName() {
        return String.format("ClientHandler-%s", this.id - 1);
    }
}
