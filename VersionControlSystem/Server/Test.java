import java.io.IOException;


public class Test {

    public static void main(String[] args) throws IOException {
        Server server = new Server(new DiskDataProvider("src/working_directory"));
        server.start();
    }
}
