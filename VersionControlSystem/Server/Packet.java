import serializer.ISerializable;

public class Packet implements ISerializable {
    public String command;
    public String repository;
    public String[] flags;
    public String info;
    public FileContainer file;

    public Packet() {}

    public Packet(String command, String repository, String[] flags, String info, FileContainer file) {
        this.command = command;
        this.repository = repository;
        this.flags = flags;
        this.info = info;
        this.file = file;
    }
}
