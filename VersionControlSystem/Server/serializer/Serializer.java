package serializer;

import serializer.utils.Pointer;
import serializer.utils.Utils;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class Serializer {
    private HashMap<String, String> typeMapping;

    public Serializer() {
        this.typeMapping = new HashMap<>();
    }

    private static HashMap<Class, Byte> codes = new HashMap<Class, Byte>() {
        {
            put(ISerializable.class, (byte)0);
            put(byte.class, (byte)1);
            put(short.class, (byte)2);
            put(int.class, (byte)3);
            put(long.class, (byte)4);
            put(float.class, (byte)5);
            put(double.class, (byte)6);
            put(boolean.class, (byte)7);
            put(char.class, (byte)8);
            put(String.class, (byte)9);
        }
    };

    private static HashMap<Byte, Class> types = new HashMap<Byte, Class>() {
        {
            put((byte)0, ISerializable.class);
            put((byte)1, byte.class);
            put((byte)2, short.class);
            put((byte)3, int.class);
            put((byte)4, long.class);
            put((byte)5, float.class);
            put((byte)6, double.class);
            put((byte)7, boolean.class);
            put((byte)8, char.class);
            put((byte)9, String.class);
        }
    };

    public boolean learn(String dataFilePath) {
        StringBuilder builder = new StringBuilder();
        FileReader reader;
        try {
            reader = new FileReader(dataFilePath);
            while (reader.ready()) {
                builder.append((char) reader.read());
            }
        } catch (IOException exception) {
            return false;
        }
        String[] lines = builder.toString().split("\r\n");
        for (String line : lines) {
            String[] pair = line.split(" ");
            this.typeMapping.put(pair[0], pair[1]);
        }
        return true;
    }

    private String chooseClsName(String clsName) {
        if (typeMapping.containsKey(clsName)) {
            return typeMapping.get(clsName);
        }
        return clsName;
    }

    private byte[] serializeArray(Object value) {
        Class cls = value.getClass().getComponentType();
        ArrayList<Byte> bytes = new ArrayList<>();
        bytes.add((byte)10);
        Byte code = codes.get(cls);
        if (cls.isArray()) {
            bytes.add((byte)10);
        } else if (code != null) {
            bytes.add(code);
        }
        int size = 0;
        int back = bytes.size();
        if (cls == byte.class) {
            size = ((byte[])value).length;
            for (byte b : (byte[])value) {
                for (byte ib : serialize(b)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == short.class) {
            size = ((short[])value).length;
            for (short s : (short[])value) {
                for (byte ib : serialize(s)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == int.class) {
            size = ((int[])value).length;
            for (int i : (int[])value) {
                for (byte ib : serialize(i)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == long.class) {
            size = ((long[])value).length;
            for (long l : (long[])value) {
                for (byte ib : serialize(l)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == float.class) {
            size = ((float[])value).length;
            for (float f : (float[])value) {
                for (byte ib : serialize(f)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == double.class) {
            size = ((double[])value).length;
            for (double d : (double[])value) {
                for (byte ib : serialize(d)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == boolean.class) {
            size = ((boolean[])value).length;
            for (boolean b : (boolean[])value) {
                for (byte ib : serialize(b)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == char.class) {
            size = ((char[])value).length;
            for (char c : (char[])value) {
                for (byte ib : serialize(c)) {
                    bytes.add(ib);
                }
            }
        } else if (cls == String.class) {
            size = ((String[])value).length;
            for (String s : (String[])value) {
                for (byte ib : serialize(s)) {
                    bytes.add(ib);
                }
            }
        } else {
            for (Object o : (Object[])value) {
                size = ((Object[])value).length;
                for (byte ib : serialize(o)) {
                    bytes.add(ib);
                }
            }
        }
        byte[] arraySize = ByteBuffer
            .allocate(Integer.BYTES)
            .order(ByteOrder.LITTLE_ENDIAN)
            .putInt(size)
            .array();
        bytes.add(back, arraySize[3]);
        bytes.add(back, arraySize[2]);
        bytes.add(back, arraySize[1]);
        bytes.add(back, arraySize[0]);
        byte[] result = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            result[i] = bytes.get(i);
        }
        return result;
    }

    private byte[] serialize(ISerializable serializable) {
        ArrayList<Byte> bytes = new ArrayList<>();
        HashSet<String> writtenFields = new HashSet<>();
        // Объявление типа
        bytes.add((byte)0);
        Class cls = serializable.getClass();
        // Записываем имя класса
        for (byte b : Utils.stringToBytes(cls.getName())) {
            bytes.add(b);
        }
        Field[] fields = cls.getFields();
        int back = bytes.size();

        // Записываем описание полей
        for (Field field : fields) {
            // Проверка на перезапись поля
            if (writtenFields.contains(field.getName())) {
                continue;
            } else {
                writtenFields.add(field.getName());
            }
            // Записываем имя поля
            for (byte b : Utils.stringToBytes(field.getName())) {
                bytes.add(b);
            }
            boolean accessible;
            try {
                if (!(accessible = field.isAccessible())) {
                    field.setAccessible(true);
                }
                // Записываем значение поля, полученное рекурсивно
                for (byte b : serialize(field.get(serializable))) {
                    bytes.add(b);
                }
                if (!accessible) {
                    field.setAccessible(false);
                }
            } catch (IllegalAccessException exception) {
                return null;
            }
        }
        // Записываем количество полей
        bytes.add(back, (byte)writtenFields.size());
        byte[] result = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            result[i] = bytes.get(i);
        }
        return result;
    }

    public byte[] serialize(Object value) {
        if (value == null) {
            return new byte[] {-1};
        } else if (value instanceof ISerializable) {
            return serialize((ISerializable)value);
        } else if (value instanceof Byte) {
            return new byte[] { 1, (byte)value };
        } else if (value instanceof Short) {
            return Utils.concatenate(
                new byte[] { 2 },
                ByteBuffer
                    .allocate(Short.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .putShort((short)value)
                    .array()
            );
        } else if (value instanceof Integer) {
            return Utils.concatenate(
                new byte[] { 3 },
                ByteBuffer
                    .allocate(Integer.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .putInt((int)value)
                    .array()
            );
        } else if (value instanceof Long) {
            return Utils.concatenate(
                new byte[] { 4 },
                ByteBuffer
                    .allocate(Long.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .putLong((long)value)
                    .array()
            );
        } else if (value instanceof Float) {
            return Utils.concatenate(
                new byte[] { 5 },
                ByteBuffer
                    .allocate(Float.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .putFloat((float)value)
                    .array()
            );
        } else if (value instanceof Double) {
            return Utils.concatenate(
                new byte[] { 6 },
                ByteBuffer
                    .allocate(Double.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .putDouble((double)value)
                    .array()
            );
        } else if (value instanceof Boolean) {
            return new byte[] { 7, (boolean)value ? (byte) 1 : (byte) 0};
        } else if (value instanceof Character) {
            return Utils.concatenate(
                new byte[] { 8 },
                ByteBuffer
                    .allocate(Character.BYTES)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .putChar((char)value)
                    .array()
            );
        } else if (value instanceof String) {
            return Utils.concatenate(
                    new byte[] { 9 },
                    Utils.stringToBytes((String)value)
            );
        } else if (value.getClass().isArray()) {
            return serializeArray(value);
        }
        return new byte[0];
    }

    private static HashMap<Integer, Integer> sizes = new HashMap<Integer, Integer>() {
        {
            put(1, 1);
            put(2, 2);
            put(3, 4);
            put(4, 8);
            put(5, 4);
            put(6, 8);
            put(7, 1);
            put(8, 2);
        }
    };

    public Object deserialize(byte[] bytes) {
        return deserialize(bytes, new Pointer(0));
    }

    private Object deserialize(byte[] bytes, Pointer pointer) {
        if (bytes[pointer.getValue()] == -1) {
            pointer.shift(1);
            return null;
        } else if (bytes[pointer.getValue()] == 0) {
            pointer.shift(1);
            return deserializeIS(bytes, pointer);
        } else if (bytes[pointer.getValue()] == 1) {
            pointer.shift(1);
            return bytes[pointer.getValue()];
        } else if (bytes[pointer.getValue()] == 2) {
            pointer.shift(1);
            return ByteBuffer.wrap(bytes)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .getShort(pointer.getValue());
        } else if (bytes[pointer.getValue()] == 3) {
            pointer.shift(1);
            return ByteBuffer.wrap(bytes)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .getInt(pointer.getValue());
        } else if (bytes[pointer.getValue()] == 4) {
            pointer.shift(1);
            return ByteBuffer.wrap(bytes)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .getLong(pointer.getValue());
        } else if (bytes[pointer.getValue()] == 5) {
            pointer.shift(1);
            return ByteBuffer.wrap(bytes)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .getFloat(pointer.getValue());
        } else if (bytes[pointer.getValue()] == 6) {
            pointer.shift(1);
            return ByteBuffer.wrap(bytes)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .getDouble(pointer.getValue());
        } else if (bytes[pointer.getValue()] == 7) {
            pointer.shift(1);
            return bytes[pointer.getValue()] == 1;
        } else if (bytes[pointer.getValue()] == 8) {
            pointer.shift(1);
            return ByteBuffer.wrap(bytes)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .getChar(pointer.getValue());
        } else if (bytes[pointer.getValue()] == 9) {
            pointer.shift(1);
            return Utils.stringFromBytes(bytes, pointer);
        } else if (bytes[pointer.getValue()] == 10) {
            pointer.shift(1);
            return deserializeArray(bytes, pointer);
        }
        return null;
    }

    private Object deserializeArray(byte[] bytes, Pointer pointer) {
        Class cls = types.get(bytes[pointer.getValue()]);
        pointer.shift(1);
        int size = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getInt(pointer.getValue());
        pointer.shift(4);
        Object array = Array.newInstance(cls, size);
        if (cls == byte.class) {
            for (int i = 0; i < size; i++) {
                ((byte[])array)[i] = (byte)deserialize(bytes, pointer);
                pointer.shift(sizes.get(1));
            }
        } else if (cls == short.class) {
            for (int i = 0; i < size; i++) {
                ((short[])array)[i] = (short)deserialize(bytes, pointer);
                pointer.shift(sizes.get(2));
            }
        } else if (cls == int.class) {
            for (int i = 0; i < size; i++) {
                ((int[])array)[i] = (int)deserialize(bytes, pointer);
                pointer.shift(sizes.get(3));
            }
        } else if (cls == long.class) {
            for (int i = 0; i < size; i++) {
                ((long[])array)[i] = (long)deserialize(bytes, pointer);
                pointer.shift(sizes.get(4));
            }
        } else if (cls == float.class) {
            for (int i = 0; i < size; i++) {
                ((float[])array)[i] = (float)deserialize(bytes, pointer);
                pointer.shift(sizes.get(5));
            }
        } else if (cls == double.class) {
            for (int i = 0; i < size; i++) {
                ((double[])array)[i] = (double)deserialize(bytes, pointer);
                pointer.shift(sizes.get(6));
            }
        } else if (cls == boolean.class) {
            for (int i = 0; i < size; i++) {
                ((boolean[])array)[i] = (boolean)deserialize(bytes, pointer);
                pointer.shift(sizes.get(7));
            }
        } else if (cls == char.class) {
            for (int i = 0; i < size; i++) {
                ((char[])array)[i] = (char)deserialize(bytes, pointer);
                pointer.shift(sizes.get(8));
            }
        } else if (cls == String.class) {
            for (int i = 0; i < size; i++) {
                ((String[])array)[i] = (String)deserialize(bytes, pointer);
            }
        } else {
            for (int i = 0; i < size; i++) {
                ((Object[])array)[i] = deserialize(bytes, pointer);
            }
        }
        return array;
    }

    private ISerializable deserializeIS(byte[] bytes, Pointer pointer) {
        Class cls;
        String clsName = Utils.stringFromBytes(bytes, pointer);
        clsName = chooseClsName(clsName);
        try {
            cls = Class.forName(clsName);
        } catch (ClassNotFoundException exception) {
            return null;
        }
        Object instance = null;
        boolean accessible = true;
        Constructor constructor = null;
        try {
            constructor = cls.getConstructor();
            if (!(accessible = constructor.isAccessible())) {
                constructor.setAccessible(true);
            }
            instance = constructor.newInstance();
        } catch (InstantiationException |
                IllegalAccessException |
                NoSuchMethodException |
                InvocationTargetException exception) {
            return null;
        }
        finally {
            if (!accessible) {
                constructor.setAccessible(false);
            }
        }
        int fieldCount = bytes[pointer.getValue()];
        pointer.shift(1);
        Field field = null;
        for (int i = 0; i < fieldCount; i++) {
            String fieldName = Utils.stringFromBytes(bytes, pointer);
            Integer fieldSize = sizes.get((int)bytes[pointer.getValue()]);
            Object value = deserialize(bytes, pointer);
            if (fieldSize != null) {
                pointer.shift(fieldSize);
            }
            try {
                field = cls.getField(fieldName);
                if (!(accessible = field.isAccessible())) {
                    field.setAccessible(true);
                }
                field.set(instance, value);

            } catch (NoSuchFieldException | IllegalAccessException exception) {
                return null;
            } finally {
                if (!accessible && field != null) {
                    field.setAccessible(false);
                }
            }
        }
        return (ISerializable) instance;
    }
}
