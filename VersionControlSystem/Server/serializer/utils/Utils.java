package serializer.utils;


import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {
    public static String stringFromBytes(byte[] bytes, Pointer pointer) {
        int size = ByteBuffer
            .wrap(bytes)
            .order(ByteOrder.LITTLE_ENDIAN)
            .getShort(pointer.getValue());
        pointer.shift(2);
        String result = new String(bytes, pointer.getValue(), size, Charset.forName("UTF-8"));
        pointer.shift(size);
        return result;
    }

    public static byte[] stringToBytes(String value) {
        byte[] string = value.getBytes(Charset.forName("UTF-8"));
        byte[] length = ByteBuffer
            .allocate(Short.BYTES)
            .order(ByteOrder.LITTLE_ENDIAN)
            .putShort((short)string.length)
            .array();
        return concatenate(length, string);
    }

    public static byte[] concatenate(byte[] a, byte[] b) {
        int aLen = a.length;
        int bLen = b.length;
        byte[] c = (byte[])Array.newInstance(byte.class, aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }
}
