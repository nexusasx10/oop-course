package serializer.utils;


public class Pointer {
    private int value;

    @Override
    public String toString() {
        return Integer.toString(value);
    }

    public Pointer(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void shift(int offset) {
        this.value += offset;
    }
}
