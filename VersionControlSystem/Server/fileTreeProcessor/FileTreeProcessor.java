package fileTreeProcessor;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;


public class FileTreeProcessor {
    public static void process(String path, IFileExecute executor) {
        ArrayList<String> files = walkFileTree(path);
        for (String file : files) {
            executor.execute(file);
        }
    }

    private static ArrayList<String> walkFileTree(String path) {
        ArrayList<String> result = new ArrayList<>();
        LinkedList<File> visitQueue = new LinkedList<>();
        visitQueue.add(new File(path));
        while (!visitQueue.isEmpty()) {
            File root = visitQueue.removeFirst();
            if (root.isDirectory()) {
                for (File child : root.listFiles()) {
                    visitQueue.add(child);
                }
            } else {
                result.add(root.getAbsolutePath());
            }
        }
        return result;
    }
}
