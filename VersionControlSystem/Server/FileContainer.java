import serializer.ISerializable;


public class FileContainer implements ISerializable{
    public String name;
    public byte[] data;

    public FileContainer() {}

    public FileContainer(String name, byte[] data) {
        this.name = name;
        this.data = data;
    }
}
