package threadManager;

import java.util.ArrayList;
import java.util.LinkedList;


public class ThreadManager {

    public static final int START_POOL_SIZE = 32;

    private static volatile ThreadManager instance;

    private volatile ArrayList<Executor> executors = new ArrayList<>();
    private volatile LinkedList<IWorker> taskQueue = new LinkedList<>();

    private ThreadManager() {
        for (int i = 0; i < START_POOL_SIZE; i++) {
            Executor executor = new Executor();
            Thread thread = new Thread(executor);
            this.executors.add(executor);
            thread.setDaemon(true);
            thread.start();
        }
    }

    public static ThreadManager getInstance() {
        if (instance == null) {
            synchronized (ThreadManager.class) {
                if (instance == null) {
                    instance = makeInstance();
                }
            }
        }
        return instance;
    }

    private static ThreadManager makeInstance() {
        return new ThreadManager();
    }

    public void addTask(IWorker worker) {
        synchronized (ThreadManager.class) {
            this.taskQueue.add(worker);
            ThreadManager.class.notify();
        }
    }

    private IWorker getTask() {
        if (!this.taskQueue.isEmpty()) {
            synchronized (ThreadManager.class) {
                if (! this.taskQueue.isEmpty()) {
                    return this.taskQueue.pop();
                }
            }
        }
        return null;
    }

    public void stop() {
        for (Executor executor : this.executors) {
            executor.stop();
        }
        boolean alive = true;
        while (alive) {
            synchronized (ThreadManager.class) {
                ThreadManager.class.notifyAll();
            }
            alive = false;
            for (Executor executor : this.executors) {
                alive |= executor.isAlive();
            }
        }
    }

    private class Executor implements Runnable {
        private IWorker worker;
        private boolean working;
        private boolean alive;

        @Override
        public void run() {
            this.alive = true;
            this.working = true;
            while (this.working) {
                IWorker newWorker = getTask();
                if (newWorker != null) {
                    this.worker = newWorker;
                    this.worker.run();
                } else {
                    synchronized (ThreadManager.class) {
                        try {
                            ThreadManager.class.wait();
                        } catch (InterruptedException exception) {
                        }
                    }
                }
            }
            this.alive = false;
        }

        public String getName() {
            return this.worker.getName();
        }

        public int getID() {
            return this.worker.getID();
        }

        public boolean isAlive() {
            return this.alive;
        }

        public void stop() {
            this.working = false;
        }

        @Override
        public String toString() {
            return String.format("%s - %s - %s", this.getID(), this.getName(), this.alive);
        }
    }

    private class ThreadMonitor implements IWorker {
        private volatile ArrayList<Executor> executors;

        private ThreadMonitor(ArrayList<Executor> executors) {
            this.executors = executors;
        }

        @Override
        public int getID() {
            return 0;
        }

        @Override
        public String getName() {
            return "ThreadMonitor";
        }

        @Override
        public void run() {
            while (true) {
                for (Executor executor : this.executors) {
                    if (executor.worker == null) {
                        continue;
                    }
                    System.out.println(String.format("ID: %s", executor.getID()));
                    System.out.println(String.format("Name: %s", executor.getName()));
                    System.out.println(String.format("Is alive: %s", executor.isAlive()));
                    System.out.println();
                }
            }
        }
    }

    public ThreadMonitor getThreadMonitor() {
        return new ThreadMonitor(this.executors);
    }
}
