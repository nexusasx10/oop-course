package threadManager;

public interface IWorker extends Runnable {
    int getID();
    String getName();
}
