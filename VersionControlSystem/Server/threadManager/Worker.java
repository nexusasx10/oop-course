package threadManager;


public class Worker implements IWorker {
    private static int nextNumber = 1;

    private int number;

    public Worker() {
        this.number = nextNumber;
        nextNumber++;
    }

    @Override
    public void run() {
        System.out.println(getName() + " started");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException exception) {}
        System.out.println(getName() + " ended");
    }

    @Override
    public int getID() {
        return this.number;
    }

    @Override
    public String getName() {
        return String.format("Worker-%s", this.number);
    }
}
