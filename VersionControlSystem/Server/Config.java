import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
    private IDataProvider dataProvider;
    private String configFile;
    private Properties properties;

    public Config(IDataProvider dataProvider, String configFile) {
        this.dataProvider = dataProvider;
        this.configFile = configFile;
        this.properties = new Properties();
        if (!this.dataProvider.exists(configFile)) {
            if (!this.dataProvider.writeFile(configFile, new byte[0])) {
                return;
            }
        } else {
            load();
        }
    }

    private void load() {
        byte[] data = this.dataProvider.readFile(this.configFile);
        try {
            this.properties.load(new ByteArrayInputStream(data));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void store() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            this.properties.store(outputStream, null);
            outputStream.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        byte[] data = outputStream.toByteArray();
        this.dataProvider.writeFile(this.configFile, data);
    }

    public String[] keys() {
        return this.properties.keySet().toArray(new String[this.properties.keySet().size()]);
    }

    public String get(String key) {
        return this.properties.getProperty(key);
    }

    public void set(String key, String value) {
        this.properties.setProperty(key, value);
        store();
    }
}
