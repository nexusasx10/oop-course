import serializer.Serializer;
import threadManager.IWorker;
import threadManager.ThreadManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;


public class Server implements IWorker {

    public static final int SERVER_PORT = 17407;

    private boolean error;
    private boolean running;
    private int nextID;
    private Serializer serializer;
    private ThreadManager threadManager;
    private ServerSocket listenerSocket;
    private ArrayList<ClientHandler> handlers;

    public Server(IDataProvider dataProvider) {
        this.nextID = 1;
        this.serializer = new Serializer();
        if (!this.serializer.learn("src/.config/serializer")) {
            this.error = true;
            return;
        }
        this.threadManager = ThreadManager.getInstance();
        RepositoryManager.setDataProvider(dataProvider);
        this.handlers = new ArrayList<>();
    }

    public void start() {
        this.running = true;
        this.threadManager.addTask(this);
        Scanner scanner = new Scanner(System.in);
        while (this.running && !this.error) {
            String[] input = scanner.nextLine().split(" ");
            parseCommand(input);
        }
    }

    @Override
    public void run() {
        this.running = true;
        try {
            this.listenerSocket = new ServerSocket(SERVER_PORT);
            try {
                while (this.running) {
                    Socket handlerSocket = this.listenerSocket.accept();
                    int id = this.nextID++;
                    ClientHandler handler = new ClientHandler(id, handlerSocket, this.serializer);
                    this.threadManager.addTask(handler);
                    this.handlers.add(handler);
                }
            } finally {
                this.listenerSocket.close();
            }
        } catch (IOException exception) {
        }
    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public String getName() {
        return "Server";
    }

    public void exit() {
        this.running = false;
        try {
            this.listenerSocket.close();
        } catch (IOException exception) {
        }
        this.threadManager.stop();
    }

    public void parseCommand(String[] input) {
        switch (input[0]) {
            case "":
                break;
            case "exit":
                this.exit();
                break;
            default:
                System.out.println("Unknown command");
        }
    }
}
