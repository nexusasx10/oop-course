

public interface IDataProvider {
    boolean exists(String path);
    String[] walk(String path);
    boolean isDirectory(String path);
    boolean createDirectory(String path);
    String[] getDirectoryContent(String path);
    boolean writeFile(String path, byte[] bytes);
    byte[] readFile(String path);
}
