import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;


public class DiskDataProvider implements IDataProvider {
    private String root;

    public DiskDataProvider(String root) {
        this.root = root;
    }

    private Path getPath(String path) {
        return Paths.get(this.root, path);
    }

    @Override
    public boolean exists(String path) {
        return Files.exists(getPath(path));
    }

    @Override
    public String[] walk(String path) {
        ArrayList<String> files = new ArrayList<>();
        LinkedList<String> directories = new LinkedList<>();
        directories.addLast(path);
        while (directories.size() > 0) {
            String root = directories.pollFirst();
            for (String item : getDirectoryContent(root)) {
                if (isDirectory(item)) {
                    directories.addLast(item);
                } else {
                    files.add(item);
                }
            }
        }
        return files.toArray(new String[files.size()]);
    }

    @Override
    public boolean isDirectory(String path) {
        return Files.isDirectory(getPath(path));
    }

    @Override
    public boolean createDirectory(String path) {
        try {
            Files.createDirectory(getPath(path));
        } catch (IOException exception) {
            return false;
        }
        return true;
    }

    @Override
    public String[] getDirectoryContent(String path) {
        File[] files = new File(getPath(path).toString()).listFiles();
        if (files == null) {
            return null;
        }
        String[] result = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            Path root = Paths.get(this.root).toAbsolutePath();
            Path file = Paths.get(files[i].getAbsolutePath());
            result[i] = root.relativize(file).toString();
        }
        return result;
    }

    @Override
    public boolean writeFile(String path, byte[] bytes) {
        File file = new File(getPath(path).toString());
        file.getParentFile().mkdirs();
        try {
            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(bytes);
            } finally {
                stream.flush();
                stream.close();
            }
        } catch (IOException exception) {
            return false;
        }
        return true;
    }

    @Override
    public byte[] readFile(String path) {
        File file = new File(getPath(path).toString());
        try {
            FileInputStream stream = new FileInputStream(file);
            byte[] result = new byte[stream.available()];
            try {
                int i = 0;
                int value;
                while ((value = stream.read()) >= 0) {
                    result[i] = (byte)value;
                    i++;
                }
                return result;
            } finally {
                stream.close();
            }
        } catch (IOException exception) {
            return null;
        }
    }
}
