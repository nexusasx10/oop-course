


public class RepositoryLocker {
    private volatile int lockedForRead;
    private volatile boolean lockedForWrite;
    private final Object sync;

    public RepositoryLocker() {
        this.lockedForRead = 0;
        this.lockedForWrite = false;
        this.sync = new Object();
    }

    public void obtainRead() {
        boolean lockObtained = false;
        while (!lockObtained) {
            synchronized (this.sync) {
                if (!this.lockedForWrite) {
                    this.lockedForRead++;
                    lockObtained = true;
                } else {
                    try {
                        this.sync.wait();
                    } catch (InterruptedException ex) {}
                }
            }
        }
    }

    public void releaseRead() {
        synchronized (this.sync) {
            this.lockedForRead--;
            this.sync.notifyAll();
        }
    }

    public void obtainWrite() {
        boolean lockObtained = false;
        while (!lockObtained) {
            synchronized (this.sync) {
                if (!(this.lockedForWrite || this.lockedForRead > 0)) {
                    this.lockedForWrite = true;
                    lockObtained = true;
                } else {
                    try {
                        this.sync.wait();
                    } catch (InterruptedException ex) {}
                }
            }
        }
    }

    public void releaseWrite() {
        synchronized (this.sync) {
            this.lockedForWrite = false;
            this.sync.notifyAll();
        }
    }
}
