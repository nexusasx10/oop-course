import utils.Utils;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


public class RepositoryManager {
    private static HashMap<String, RepositoryManager> managers = new HashMap<>();
    private static IDataProvider dataProvider;

    private String repository;
    private Config config;
    private Config hashes;
    private RepositoryLocker locker;

    private RepositoryManager(String repository) {
        this.repository = repository;
        this.locker = new RepositoryLocker();
        if (dataProvider.exists(this.repository)) {
            this.config = new Config(dataProvider, this.repository + "/.config");
            this.hashes = new Config(dataProvider, this.repository + "/.hash");
        }
    }

    public static synchronized RepositoryManager getManager(String repository) {
        if (managers.containsKey(repository)) {
            return managers.get(repository);
        } else {
            RepositoryManager manager = new RepositoryManager(repository);
            managers.put(repository, manager);
            return manager;
        }
    }

    public static void setDataProvider(IDataProvider dataProvider) {
        RepositoryManager.dataProvider = dataProvider;
    }

    public boolean existsRepository() {
        return dataProvider.exists(this.repository);
    }

    public boolean createRepository() {
        this.locker.obtainWrite();
        try {
            if (existsRepository()) {
                return false;
            }
            if (dataProvider.createDirectory(this.repository)) {
                this.config = new Config(dataProvider, this.repository + "/.config");
                this.config.set("version", "0");
                this.hashes = new Config(dataProvider, this.repository + "/.hash");
                return true;
            }
        } finally {
            this.locker.releaseWrite();
        }
        return false;
    }

    public boolean commit(FileContainer[] files) {
        this.locker.obtainWrite();
        ArrayList<String> arrivedFiles = new ArrayList<>();
        try {
            String version = String.valueOf(Integer.parseInt(this.config.get("version")) + 1);
            for (FileContainer file : files) {
                arrivedFiles.add(file.name);
                if (!isFileUpdated(file)) {
                    continue;
                }
                if (!dataProvider.writeFile(this.repository + "/" + version + "/" + file.name, file.data)) {
                    return false;
                }
                String newHash = DatatypeConverter.printHexBinary(Utils.SHA_1(file.data));
                this.hashes.set(file.name, newHash);
            }
            StringBuilder deleted = new StringBuilder();
            for (String fileName : this.hashes.keys()) {
                if (!arrivedFiles.contains(fileName)) {
                    if (this.hashes.get(fileName).equals("-")) {
                        continue;
                    }
                    deleted.append(fileName);
                    deleted.append("\n");
                    this.hashes.set(fileName, "-");
                }
            }
            byte[] deletedData = deleted.toString().getBytes(Charset.forName("UTF-8"));
            dataProvider.writeFile(this.repository + "/" + version + "/.deleted", deletedData);
            this.config.set("version", version);
        } finally {
            this.locker.releaseWrite();
        }
        return true;
    }

    public FileContainer[] pull(int version) {
        ArrayList<FileContainer> result = new ArrayList<>();
        this.locker.obtainRead();
        try {
            for (String fileName : this.hashes.keys()) {
                FileContainer file;
                if (version == 0) {
                    file = getFileLastVersion(fileName);
                } else {
                    file = getFileVersion(fileName, version);
                }
                if (file != null) {
                    result.add(file);
                }
            }
        } finally {
            this.locker.releaseRead();
        }
        return result.toArray(new FileContainer[result.size()]);
    }

    public String log() {
        StringBuilder result = new StringBuilder();
        this.locker.obtainRead();
        try {
            int lastVersion = Integer.parseInt(this.config.get("version"));
            if (lastVersion == 0) {
                return "No operations";
            }
            HashSet<String> old = new HashSet<>();
            for (int version = 1; version <= lastVersion; version++) {
                result.append("[Commit ").append(version).append("]\n");
                String[] files = dataProvider.walk(this.repository + "/" + version);
                for (String file : files) {
                    file = file.split("\\\\", 3)[2];
                    if (file.equals(".deleted")) {
                        continue;
                    }
                    if (old.contains(file)) {
                        result.append("<modified> ").append(file).append("\n");
                    } else {
                        old.add(file);
                        result.append("<added> ").append(file).append("\n");
                    }
                }
                byte[] deletedData = dataProvider.readFile(this.repository + "/" + String.valueOf(version) + "/.deleted");
                if (deletedData != null) {
                    String[] deleted = new String(deletedData, Charset.forName("UTF-8")).split("\n");
                    for (String file : deleted) {
                        if (!file.equals("")) {
                            result.append("<deleted> ").append(file).append("\n");
                        }
                    }
                }
                result.append("\n");
            }
        } finally {
            this.locker.releaseRead();
        }
        return result.toString();
    }

    private FileContainer getFileVersion(String fileName, int version) {
        for (int i = version; i > 0; i--) {
            byte[] deletedData = dataProvider.readFile(this.repository + "/" + String.valueOf(i) + "/.deleted");
            if (deletedData != null) {
                String[] deleted = new String(deletedData, Charset.forName("UTF-8")).split("\n");
                if (Arrays.asList(deleted).contains(fileName)) {
                    return null;
                }
            }
            byte[] fileContent = dataProvider.readFile(this.repository + "/" + String.valueOf(i) + "/" + fileName);
            if (fileContent != null) {
                return new FileContainer(fileName, fileContent);
            }
        }
        return null;
    }

    private FileContainer getFileLastVersion(String name) {
        return getFileVersion(name, Integer.parseInt(this.config.get("version")));
    }

    private boolean isFileUpdated(FileContainer file) {
        String oldHash = this.hashes.get(file.name);
        String newHash = DatatypeConverter.printHexBinary(Utils.SHA_1(file.data));
        if (newHash.equals(oldHash)) {
            FileContainer oldVersion = getFileLastVersion(file.name);
            if (oldVersion.data.length != file.data.length) {
                return true;
            }
            for (int i = 0; i < file.data.length; i++) {
                if (oldVersion.data[i] != file.data[i]) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }
}
