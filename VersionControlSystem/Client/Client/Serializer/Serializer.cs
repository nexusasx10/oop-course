﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;

namespace Client
{
    class Serializer
    {
        private static Dictionary<Type, byte> codes = new Dictionary<Type, byte>()
        {
            { typeof(ISerializable), 0 },
            { typeof(byte), 1 },
            { typeof(short), 2 },
            { typeof(int), 3 },
            { typeof(long), 4 },
            { typeof(float), 5 },
            { typeof(double), 6 },
            { typeof(bool), 7 },
            { typeof(char), 8 },
            { typeof(string), 9 },
            { typeof(Array), 10 },
        };

        private static Dictionary<byte, Type> types = new Dictionary<byte, Type>()
        {
            { 0, typeof(ISerializable) },
            { 1, typeof(byte) },
            { 2, typeof(short) },
            { 3, typeof(int) },
            { 4, typeof(long) },
            { 5, typeof(float) },
            { 6, typeof(double) },
            { 7, typeof(bool) },
            { 8, typeof(char) },
            { 9, typeof(string) },
            { 10, typeof(Array) },
        };

        private static Dictionary<byte, int> sizes = new Dictionary<byte, int>()
        {
            { 1, 1 },
            { 2, 2 },
            { 3, 4 },
            { 4, 8 },
            { 5, 4 },
            { 6, 8 },
            { 7, 1 },
            { 8, 2 }
        };

        private Dictionary<string, string> typeMapping;

        public Serializer()
        {
            typeMapping = new Dictionary<string, string>();
        }

        public bool Learn(string dataFilePath)
        {
            Utils.Config config = new Utils.Config("config.txt", false);
            foreach (string key in config.Keys)
            {
                typeMapping[key] = config.Get(key);
            }
            return true;
        }

        private string chooseTypeName(string clsName)
        {
            if (typeMapping.ContainsKey(clsName))
            {
                return typeMapping[clsName];
            }
            return clsName;
        }

        private byte[] SerializeArray(Array value)
        {
            List<byte> bytes = new List<byte>();
            bytes.Add(10);
            Type contentType = value.GetType().GetElementType();
            bytes.Add(codes[contentType]);
            bytes.AddRange(BitConverter.GetBytes(value.Length));
            for (int i = 0; i < value.Length; i++)
            {
                bytes.AddRange(Serialize(value.GetValue(i)));
            }
            return bytes.ToArray();
        }

        private byte[] SerializeIS(ISerializable serializable)
        {
            HashSet<string> writtenFields = new HashSet<string>();
            List<byte> bytes = new List<byte>();
            bytes.Add(0);
            Type type = serializable.GetType();
            bytes.AddRange(Utils.StringToBytes(type.FullName));
            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            int back = bytes.Count;
            foreach (FieldInfo field in fields)
            {
                if (writtenFields.Contains(field.Name))
                {
                    continue;
                }
                else
                {
                    writtenFields.Add(field.Name);
                }
                bytes.AddRange(Utils.StringToBytes(field.Name));
                bytes.AddRange(Serialize(field.GetValue(serializable)));
            }
            bytes.Insert(back, (byte)fields.Length);
            return bytes.ToArray();
        }

        public byte[] Serialize(object value)
        {
            if (value == null)
            {
                return new byte[] { 255 };
            }
            else if (value is ISerializable)
            {
                return SerializeIS((ISerializable)value);
            }
            else if (value is byte)
            {
                return new byte[] { 1, (byte)value };
            }
            else if (value is short)
            {
                return new byte[] { 2 }
                .Concat(BitConverter.GetBytes((short)value))
                .ToArray();
            }
            else if (value is int)
            {
                return new byte[] { 3 }
                .Concat(BitConverter.GetBytes((int)value))
                .ToArray();
            }
            else if (value is long)
            {
                return new byte[] { 4 }
                .Concat(BitConverter.GetBytes((long)value))
                .ToArray();
            }
            else if (value is float)
            {
                return new byte[] { 5 }
                .Concat(BitConverter.GetBytes((float)value))
                .ToArray();
            }
            else if (value is double)
            {
                return new byte[] { 6 }
                .Concat(BitConverter.GetBytes((double)value))
                .ToArray();
            }
            else if (value is bool)
            {
                return new byte[] { 7 }
                .Concat(BitConverter.GetBytes((bool)value))
                .ToArray();
            }
            else if (value is char)
            {
                return new byte[] { 8 }
                .Concat(BitConverter.GetBytes((char)value))
                .ToArray();
            }
            else if (value is string)
            {
                return new byte[] { 9 }
                .Concat(Utils.StringToBytes((string)value))
                .ToArray();
            }
            else if (value is Array)
            {
                return SerializeArray((Array)value);
            }
            return new byte[0];
        }

        private Array DeserializeArray(byte[] bytes, Utils.Pointer pointer)
        {
            Type cls = types[bytes[pointer.GetValue()]];
            pointer.Shift(1);
            int size = BitConverter.ToInt32(bytes, pointer.GetValue());
            pointer.Shift(4);
            Array array = Array.CreateInstance(cls, size);
            for (int i = 0; i < size; i++)
            {
                bool shift = sizes.ContainsKey(bytes[pointer.GetValue()]);
                array.SetValue(Deserialize(bytes, pointer), i);
                if (shift)
                {
                    pointer.Shift(sizes[bytes[pointer.GetValue() - 1]]);
                }
            }
            return array;
        }

        private ISerializable DeserializeIS(byte[] bytes, Utils.Pointer pointer)
        {
            string typeName = Utils.StringFromBytes(bytes, pointer);
            typeName = chooseTypeName(typeName);
            Type type = Type.GetType(typeName);
            ConstructorInfo constructor = type.GetConstructor(new Type[0]);
            object instance = constructor.Invoke(new object[0]);
            int fieldCount = bytes[pointer.GetValue()];
            pointer.Shift(1);
            for (int i = 0; i < fieldCount; i++)
            {
                string fieldName = Utils.StringFromBytes(bytes, pointer);
                FieldInfo field = type.GetField(fieldName);
                bool shift = sizes.ContainsKey(bytes[pointer.GetValue()]);
                field.SetValue(instance, Deserialize(bytes, pointer));
                if (shift)
                {
                    pointer.Shift(sizes[bytes[pointer.GetValue() - 1]]);
                }
            }
            return (ISerializable)instance;
        }

        public object Deserialize(byte[] bytes, Utils.Pointer pointer = null)
        {
            if (pointer == null)
            {
                pointer = new Utils.Pointer(0);
            }
            if (bytes[pointer.GetValue()] == 255)
            {
                pointer.Shift(1);
                return null;
            }
            else if (bytes[pointer.GetValue()] == 0)
            {
                pointer.Shift(1);
                return DeserializeIS(bytes, pointer);
            }
            else if (bytes[pointer.GetValue()] == 1)
            {
                pointer.Shift(1);
                return bytes[pointer.GetValue()];
            }
            else if (bytes[pointer.GetValue()] == 2)
            {
                pointer.Shift(1);
                return BitConverter.ToInt16(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 3)
            {
                pointer.Shift(1);
                return BitConverter.ToInt32(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 4)
            {
                pointer.Shift(1);
                return BitConverter.ToInt64(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 5)
            {
                pointer.Shift(1);
                return BitConverter.ToSingle(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 6)
            {
                pointer.Shift(1);
                return BitConverter.ToDouble(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 7)
            {
                pointer.Shift(1);
                return BitConverter.ToBoolean(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 8)
            {
                pointer.Shift(1);
                return BitConverter.ToChar(bytes, pointer.GetValue());
            }
            else if (bytes[pointer.GetValue()] == 9)
            {
                pointer.Shift(1);
                return Utils.StringFromBytes(bytes, pointer);
            }
            else if (bytes[pointer.GetValue()] == 10)
            {
                pointer.Shift(1);
                return DeserializeArray(bytes, pointer);
            }
            else
            {
                return null;
            }
        }
    }
}
