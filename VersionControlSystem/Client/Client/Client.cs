﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Sockets;
using System.IO;
using static Client.Utils;

namespace Client
{
    class Client
    {
        public const string DefaultIP = "127.0.0.1";
        public const int DefaultPort = 17407;
        
        private bool error;
        private bool running;
        private Serializer serializer;
        private Socket socket;
        private Dictionary<string, Action<string[]>> commands;

        private bool Connected
        {
            get
            {
                return !(socket is null) && socket.Connected;
            }
        }

        public Client()
        {
            error = false;
            serializer = new Serializer();
            if (!serializer.Learn("config.txt"))
            {
                Console.WriteLine("Serializer config error");
                error = true;
                return;
            }
            commands = new Dictionary<string, Action<string[]>>
            {
                { "exit", Exit },
                { "reconnect", Connect },
                { "init", Init },
                { "commit", Commit },
                { "clone", Clone },
                { "update", Update },
                { "switch", Switch },
                { "log", Log },
                { "cd", CD }
            };
        }

        public void Start()
        {
            running = true;
            Connect(new string[] { "connect" });
            while (running && !error)
            {
                Console.Write("> ");
                string[] input = Console.ReadLine().Split(' ');
                ParseCommand(input);
            }
        }

        private void ParseCommand(string[] input)
        {
            if (input[0] == "")
            {
                return;
            }
            else if (commands.ContainsKey(input[0]))
            {
                commands[input[0]](input);
            }
            else
            {
                Console.WriteLine("Unknown command");
            }
        }

        private bool Send(Packet packet)
        {
            if (Connected)
            {
                byte[] data = serializer.Serialize(packet);
                try
                {
                    socket.Send(BitConverter.GetBytes(data.Length));
                    socket.Send(data);
                    return true;
                }
                catch (SocketException)
                {
                    socket.Close();
                }
            }
            Console.WriteLine("No connection with server");
            return false;
        }

        private Packet Receive()
        {
            if (Connected)
            {
                byte[] lengthBytes = new byte[4];
                try
                {
                    while (socket.Available < 4) {} //
                    socket.Receive(lengthBytes);
                    int length = BitConverter.ToInt32(lengthBytes, 0);
                    byte[] buffer = new byte[length];
                    while (socket.Available < length) {} //
                    socket.Receive(buffer);
                    return (Packet)serializer.Deserialize(buffer);
                }
                catch (SocketException)
                {
                    socket.Close();
                }
            }
            Console.WriteLine("No connection with server");
            return null;
        }

        public void CD(string[] args)
        {
            if (args.Length == 1)
            {
                Console.WriteLine(Directory.GetCurrentDirectory());
            }
            else if (args.Length == 2)
            {
                try
                {
                    Directory.SetCurrentDirectory(args[1]);
                }
                catch (IOException)
                {
                    Console.WriteLine("Wrong directory");
                }
            }
            else
            {
                Console.WriteLine("Bad arguments");
            }
        }

        public void Connect(string[] args)
        {
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            try
            {
                switch (CheckArgs(args, 1, 2))
                {
                    case -1:
                        return;
                    case 1:
                        socket.Connect(DefaultIP, DefaultPort);
                        break;
                    case 2:
                        socket.Connect(DefaultIP, int.Parse(args[1]));
                        break;
                }
            }
            catch (SocketException)
            {
                Console.WriteLine("Server connetcion error");
                return;
            }
            bool send = Send(new Packet("connect"));
            if (CheckPacket(send, Receive()))
            {
                Console.WriteLine("Connection established");
            }
            else
            {
                Console.WriteLine("Server connection error");
            }
        }

        public void Exit(string[] args)
        {
            if (CheckArgs(args, 1) == -1)
            {
                return;
            }
            running = false;
            if (Connected)
            {
                try
                {
                    Send(new Packet("disconnect"));
                    socket.Shutdown(SocketShutdown.Both);
                }
                finally
                {
                    socket.Close();
                }
            }
        }

        public void Init(string[] args)
        {
            if (CheckArgs(args, 2) == -1)
            {
                return;
            }
            string repository = args[1];
            bool send = Send(new Packet("init", repository: repository));
            if (CheckPacket(send, Receive()))
            {
                Console.WriteLine(string.Format("Repository {0} was created", repository));
            }
            else
            {
                Console.WriteLine(string.Format("Repository {0} already exist", repository));
            }
        }

        public void Clone(string[] args)
        {
            bool hard = false;
            switch (CheckArgs(args, 3, 4))
            {
                case -1:
                    return;
                case 4:
                    hard = (args[3] == "-hard");
                    break;
            }
            string repository = args[1];
            string path = args[2];
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (IOException)
            {
                Console.WriteLine("Bad operation");
                return;
            }
            Config config = new Config(path + "\\.config");
            config.Set("repository", repository);
            if (hard)
            {
                Update(new string[] { args[0], args[2], args[3]});
            }
            else
            {
                Update(new string[] { args[0], args[2] });
            }
        }

        public void Update(string[] args)
        {
            switch (CheckArgs(args, 1, 2, 3))
            {
                case -1:
                    return;
                case 1:
                    Switch(new string[] { args[0], "0" });
                    break;
                case 2:
                    Switch(new string[] { args[0], "0", args[1] });
                    break;
                case 3:
                    Switch(new string[] { args[0], "0", args[1], args[2] });
                    break;
            }
        }

        public void Switch(string[] args)
        {
            string path = null;
            string version = null;
            bool hard = false;
            switch (CheckArgs(args, 2, 3, 4))
            {
                case -1:
                    return;
                case 2:
                    path = Directory.GetCurrentDirectory();
                    version = args[1];
                    break;
                case 3:
                    if (args[2] == "-hard")
                    {
                        hard = true;
                    }
                    else
                    {
                        path = args[2];
                    }
                    version = args[1];
                    break;
                case 4:
                    hard = (args[3] == "-hard");
                    path = args[2];
                    version = args[1];
                    break;

            }
            string repository = GetRepository(path);
            if (repository == null)
            {
                Console.WriteLine("Wrong path");
                return;
            }
            if (int.Parse(version) < 0)
            {
                Console.WriteLine("Version must be greater then 0");
                return;
            }
            bool send = Send(new Packet("pull", repository: repository, info: version));
            Packet packet = Receive();
            if (CheckPacket(send, packet))
            {
                if (hard)
                {
                    Queue<string> dirs = new Queue<string>();
                    Stack<string> toDelete = new Stack<string>();
                    dirs.Enqueue(path);
                    try
                    {
                        while (dirs.Count > 0)
                        {
                            string dir = dirs.Dequeue();
                            foreach (string file in Directory.GetFiles(dir))
                            {
                                if (file.EndsWith(".config"))
                                {
                                    continue;
                                }
                                File.Delete(file);
                            }
                            foreach (string directory in Directory.GetDirectories(dir))
                            {
                                toDelete.Push(directory);
                                dirs.Enqueue(directory);
                            }
                        }
                        foreach (string directory in toDelete)
                        {
                            Directory.Delete(directory);
                        }
                    }
                    catch (IOException)
                    {
                    }
                }
                if (packet.flags.Contains("empty"))
                {
                    return;
                }
                List<FileContainer> files = new List<FileContainer>();
                while (packet.flags.Contains("moreFiles"))
                {
                    files.Add(packet.file);
                    packet = Receive();
                }
                files.Add(packet.file);                
                foreach (FileContainer file in files)
                {
                    new FileInfo(path + "\\" + file.name).Directory.Create();
                    File.WriteAllBytes(path + "\\" + file.name, file.data);
                }
                Console.WriteLine("Files successfuly updated");
            }
            else
            {
                Console.WriteLine("Bad operation");
            }
        }

        public void Commit(string[] args)
        {
            string path = null;
            switch(CheckArgs(args, 1, 2))
            {
                case -1:
                    return;
                case 1:
                    path = Directory.GetCurrentDirectory();
                    break;
                case 2:
                    path = args[1];
                    break;
            }
            string repository = GetRepository(path);
            if (repository is null)
            {
                Console.WriteLine("Wrong path");
                return;
            }
            FileContainer[] files = Walk(path)
                .Select(x => new FileContainer(x, File.ReadAllBytes(x)))
                .Select(x => new FileContainer(GetRelativePath(x.name, path), x.data))
                .Where(x => !x.name.EndsWith(".config"))
                .ToArray();
            bool send = true;
            if (files.Length == 0)
            {
                send = Send(new Packet("commit", repository: repository, flags: new string[] { "empty" }));
            }             
            for (int i = 0; i < files.Length; i++)
            {
                if (i == files.Length - 1)
                {
                    send &= Send(new Packet("commit", repository: repository, file: files[i]));
                }
                else
                {
                    send &= Send(new Packet("commit", repository: repository, flags: new string[] { "moreFiles" }, file: files[i]));
                }
            }
            if (CheckPacket(send, Receive()))
            {
                Console.WriteLine("Commit success");
            }
            else
            {
                Console.WriteLine("Bad operation");
            }
        }

        public void Log(string[] args)
        {
            if (CheckArgs(args, 2) == -1)
            {
                return;
            }
            string repository = args[1];
            bool send = Send(new Packet("log", repository: repository));
            Packet packet = Receive();
            if (CheckPacket(send, packet))
            {
                Console.WriteLine(packet.info);
            }
            else
            {
                Console.WriteLine("Bad operation");
            }
        }

        private string GetRepository(string path)
        {
            Config config = new Config(path + "\\.config");
            return config.Get("repository");
        }

        private bool CheckPacket(bool send, Packet packet)
        {
            return send && !(packet is null) && !(packet.flags is null) && packet.flags.Contains("confirm");
        }

        private int CheckArgs(string[] args, params int[] correct)
        {
            if (correct.Contains(args.Length))
            {
                return args.Length;
            }
            Console.WriteLine("Wrong arguments");
            return -1;
        }
    }
}
