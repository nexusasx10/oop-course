﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Utils
    {
        public class Config
        {
            private string path;
            private Dictionary<string, string> data;

            public Config(string path, bool create = true)
            {
                this.path = path;
                data = new Dictionary<string, string>();
                Load(create);
            }

            public string[] Keys
            {
                get
                {
                    return data.Keys.ToArray();
                }
            }

            public string Get(string key)
            {
                if (!data.ContainsKey(key))
                {
                    return null;
                }
                return data[key];
            }

            public void Set(string key, string value)
            {
                data[key] = value;
                Store();
            }

            private void Load(bool create)
            {
                string[] lines = null;
                if (!File.Exists(path))
                {
                    if (!create)
                    {
                        return;
                    }
                    FileInfo file = new FileInfo(path);
                    try
                    {
                        file.Directory.Create();
                        FileStream stream = File.Create(path);
                        stream.Close();
                    }
                    catch (IOException)
                    {

                    }
                }
                else
                {
                    try
                    {
                        lines = File.ReadAllLines(path);
                    }
                    catch (IOException)
                    {
                        return;
                    }
                    foreach (string line in lines)
                    {
                        string lineN = line.Trim();
                        if (lineN.StartsWith("#"))
                        {
                            continue;
                        }
                        string[] components = lineN.Split(new char[] { '=' }, 2);
                        data[components[0].TrimEnd()] = components[1].TrimStart();
                    }
                }
            }

            private void Store()
            {
                string[] lines = new string[data.Count];
                int i = 0;
                foreach (string key in data.Keys)
                {
                    lines[i] = key + "=" + data[key];
                }
                try
                {
                    File.WriteAllLines(path, lines);
                }
                catch (IOException)
                {
                }
            }
        }

        public class Pointer
        {
            public int value;

            public Pointer(int value)
            {
                this.value = value;
            }

            public void Shift(int offset)
            {
                value += offset;
            }

            public int GetValue()
            {
                return value;
            }

            public override string ToString()
            {
                return value.ToString();
            }
        }

        public static byte[] StringToBytes(string value)
        {
            List<byte> bytes = new List<byte>();
            byte[] str = Encoding.UTF8.GetBytes(value);
            bytes.AddRange(BitConverter.GetBytes((short)str.Length));
            bytes.AddRange(str);
            return bytes.ToArray();
        }

        public static string StringFromBytes(byte[] bytes, Pointer pointer)
        {
            int size = BitConverter.ToInt16(bytes, pointer.GetValue());
            pointer.Shift(2);
            string result = Encoding.UTF8.GetString(bytes, pointer.GetValue(), size);
            pointer.Shift(size);
            return result;
        }

        public static string[] Walk(string path, bool includeDirs = false)
        {
            List<string> files = new List<string>();
            Queue<string> directories = new Queue<string>();
            directories.Enqueue(path);
            while (directories.Count > 0)
            {
                string root = directories.Dequeue();
                files.AddRange(Directory.GetFiles(root));
                foreach (string directory in Directory.GetDirectories(root))
                {
                    if (includeDirs)
                    {
                        files.Add(directory);
                    }
                    directories.Enqueue(directory);
                }
            }
            return files.ToArray();
        }

        public static string GetRelativePath(string filePath, string root)
        {
            filePath = filePath.Replace('/', '\\');
            filePath = Path.GetFullPath(filePath);
            root = Path.GetFullPath(root);
            var fileUri = new Uri(filePath);
            var rootUri = new Uri(root);
            var result = rootUri.MakeRelativeUri(fileUri).ToString().Split(new char[] { '/' }, 2)[1];
            return result;
        }
    }
}
