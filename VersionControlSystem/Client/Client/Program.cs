﻿using System;
using System.Threading;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace Client
{
    class Program
    {
        static int number = 0;
        static void Main(string[] args)
        {
            //Tests();
            Client client = new Client();
            client.Start();
        }

        static void Tests()
        {
            Client client = new Client();
            client.Connect(new string[] { "" });
            client.Init(new string[] { "", "Test" });
            for (int i = 0; i < 16; i++)
            {
                Thread thread = new Thread(Test);
                thread.Start();
            }
            Console.ReadLine();
        }

        static void Test()
        {
            int number = Program.number++;
            Client client = new Client();
            client.Connect(new string[] { "" });
            client.Clone(new string[] { "", "Test", "C:/users/andre/desktop/Test/Test" + number });
            File.WriteAllText("C:/users/andre/desktop/Test/Test" + number + "/test.txt", "written by " + number);
            client.Commit(new string[] { "", "C:/users/andre/desktop/Test/Test" + number });
            client.Update(new string[] { "", "C:/users/andre/desktop/Test/Test" + number });
            client.Exit(new string[] { "" });
        }
    }
}
