﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class FileContainer : ISerializable
    {
        public string name;
        public byte[] data;

        public FileContainer()
        {
        }

        public FileContainer(string name, byte[] data)
        {
            this.name = name;
            this.data = data;
        }
    }
}
