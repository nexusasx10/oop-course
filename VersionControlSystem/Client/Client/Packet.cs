﻿

namespace Client
{
    class Packet : ISerializable
    {
        public string command;
        public string repository;
        public string[] flags;
        public string info;
        public FileContainer file;

        public Packet()
        {
        }

        public Packet(string command, string repository = null, string[] flags = null, string info = null, FileContainer file = null)
        {
            this.command = command;
            this.repository = repository;
            this.flags = flags;
            this.info = info;
            this.file = file;
        }
    }
}
