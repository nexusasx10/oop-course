﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Folding
{
    class Card
    {
        //enum Suit { Hearts, Diamonds, Clubs, Spades };
        //enum Rank { 1, 2, 3, 4, 5, 6, 7, 8, 9, J, Q, K, A };

        public string Suit { get; private set; }
        public int Rank { get; private set; }
        public Card(string suit, int rank)
        {
            Suit = suit;
            Rank = rank;
        }
        public override string ToString()
        {
            return string.Format("{0}-{1}", Suit, Rank);
        }
    }

    class CardDeck
    {
        public int Count { get { return Cards.Count; } }
        private List<Card> Cards;
        public CardDeck(int start)
        {
            Cards = new List<Card>();
            for (var s = 1; s <= 4; s++)
            {
                for (var r = start; r < 14; r++)
                {
                    switch(s)
                    {
                        case 1:
                            Cards.Add(new Card("Hearts", r));
                            break;
                        case 2:
                            Cards.Add(new Card("Diamonds", r));
                            break;
                        case 3:
                            Cards.Add(new Card("Clubs", r));
                            break;
                        case 4:
                            Cards.Add(new Card("Spades", r));
                            break;
                    }
                }
            }
        }
        public void Shuffle()
        {
            Random rnd = new Random();
            for(var i = 0; i < this.Count; i++)
            {
                var j = rnd.Next(0,i);
                var replace = this.Cards[i];
                this.Cards[i] = this.Cards[j];
                this.Cards[j] = replace;
            }
        }
        public override string ToString()
        {
            string allCards="";
            foreach (var card in this.Cards)
                allCards += string.Format("{0}\n",card.ToString());
            return allCards;
        }
    }
}
