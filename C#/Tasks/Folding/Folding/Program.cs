﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Folding
{
    class Game
    {
        static void Main()
        {
            int start = 2;
            var deck = new CardDeck(start);
            deck.Shuffle();
            Console.WriteLine(deck.ToString());
        }
    }
}
