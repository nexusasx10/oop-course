﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manipulator
{
    public static class Program
    {
        static string Message;
        static Form form;

        static double Wrist;
        static double Elbow;
        static double Shoulder;

        static void Paint(object sender, PaintEventArgs e)
        {
            //var Message2 = RobotMathematics.GetAngle(2.0, 2.0, 2.0);
            //graphics - это графический контекст.
            //с его помощью вы можете рисовать
            var graphics = e.Graphics;

            //"Очистка экрана"
            graphics.Clear(Color.Beige);

            //Закомментируйте эту строку и почувствуйте разницу
            graphics.SmoothingMode = SmoothingMode.HighQuality;
     

            //Основные методы рисования
            var pen = new Pen(Color.Black, 2);
            graphics.DrawLine(pen, 0, 0, 100, 200);
            graphics.DrawEllipse(Pens.Red, 50, 50, 20, 20);
            graphics.FillRectangle(Brushes.Green, 100, 100, 20, 20);
            if (Message!=null)
                graphics.DrawString(Message, new Font("Arial", 14), Brushes.Blue, 0, form.ClientSize.Height - 30);
            //graphics.DrawString(Message2.ToString(), new Font("Arial", 14), Brushes.Blue, 0, form.ClientSize.Height - 60);
        }

        static void KeyDown(object sender, KeyEventArgs key)
        {
            Message = "Вы нажали " + key.KeyCode.ToString();
            
            //Этот метод заставляет окно перерисовываться.
            //НЕ НАДО вызывать метод Paint вручную! 
            form.Invalidate();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            form = new AntiFlickerForm(); //можете заменить AntiFlickerForm на Form, зажать какую-нибудь клавишу и почувствовать разницу
            form.Paint += Paint;
            form.KeyDown += KeyDown;
            form.ClientSize = new Size(300, 300);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.Text = "Manipulator";
            form.MaximizeBox = false;
            form.MinimizeBox = false;
            Application.Run(form);
        }


    }
}
