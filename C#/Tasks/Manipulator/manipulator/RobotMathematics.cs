﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manipulator
{
	/// <summary>
	/// Для лучшего понимания условий задачи см. чертеж.
	/// </summary>
	public static class RobotMathematics
	{
		const double UpperArm = 15;
		const double Forearm = 12;
		const double Palm = 10;

		/// <summary>
		/// Возвращает угол (в радианах) между сторонами A и B в треугольнике со сторонами A, B, C 
		/// </summary>
		/// <param name="c"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double GetAngle(double c, double a, double b)
		{
            if ((a + b > c) && (a + c > b) && (b + c > a))
                return Math.Acos((b * b + c * c - a * a) / (2 * b * c));
            else return Double.NaN;
        }

		/// <summary>
		/// Возвращает массив углов (Shoulder, Elbow, Wrist),
		/// необходимых для приведения эффектора манипулятора в точку x и y 
		/// с углом между последним суставом и горизонталью, равному angle (в радианах)
		/// См. чертеж Schematics.png!
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="angle"></param>
		/// <returns></returns>
		public static double[] MoveTo(double x, double y, double angle)
		{
			throw new NotImplementedException();
		}

	}
}
