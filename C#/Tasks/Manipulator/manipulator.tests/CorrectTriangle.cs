﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace manipulator.tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void NormalTriangle()
        {
            var a = 1d;
            var b = 1d;
            var c = 1d;

            var result = Manipulator.RobotMathematics.GetAngle(a, b, c);

            Assert.AreEqual(true, result != Double.NaN);
        }

        [TestMethod]
        public void NotDegenerateTriangle()
        {
            var a = 1d;
            var b = 2d;
            var c = 3d;

            var result = Manipulator.RobotMathematics.GetAngle(a, b, c);

            Assert.AreEqual(Double.NaN, result);
        }

        [TestMethod]
        public void NullSides()
        {
            var a = 0d;
            var b = 0d;
            var c = 0d;

            var result = Manipulator.RobotMathematics.GetAngle(a, b, c);

            Assert.AreEqual(Double.NaN, result);
        }

        [TestMethod]
        public void NegativeSides()
        {
            var a = -3d;
            var b = -4d;
            var c = -5d;

            var result = Manipulator.RobotMathematics.GetAngle(a, b, c);

            Assert.AreEqual(Double.NaN, result);
        }

        [TestMethod]
        public void DoubleLimitExceed()
        {
            var a = 0;
            var b = Double.MaxValue;
            var c = Double.MaxValue;

            var result = Manipulator.RobotMathematics.GetAngle(a, b, c);

            Assert.AreEqual(Double.NaN, result);
        }
    }
}
