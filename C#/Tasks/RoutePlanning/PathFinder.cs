using System;
using System.Drawing;

namespace RoutePlanning
{
	public class PathFinder
	{
		public static int[] FindBestCheckpointsOrder(Point[] checkpoints)
		{
            // ������ 1-3
            // ����������� ���� ����� ���, ����� �� ������� ���������� ������� ��������� ����������.
            var bestOrder = MakeTrivialPermutation(checkpoints.Length);
            double[] distances = new double[checkpoints.Length - 1];
            //distances[i] = Math.Sqrt(Math.Pow(checkpoints[i + 1].X - checkpoints[i].X, 2) + Math.Pow(checkpoints[i + 1].Y - checkpoints[i].Y, 2));
            MakePermutations(new int[checkpoints.Length - 1], 0, checkpoints);
            return bestOrder;
        }
        static void Evaluate(int[] permutation, Point[] checkpoints)
        {
            double distance = 0;
            for (int i = 0; i < permutation.Length; i++)
                distance += Math.Sqrt(Math.Pow(checkpoints[i + 1].X - checkpoints[i].X, 2) + Math.Pow(checkpoints[i + 1].Y - checkpoints[i].Y, 2)); ;
            foreach (var e in permutation)
                Console.Write(e + " ");
            Console.Write(" " + distance);
            Console.WriteLine();
        }
        static void MakePermutations(int[] permutation, int position, Point[] checkpoints)
        {
            if (position == permutation.Length)
            {
                Evaluate(permutation, checkpoints);
                return;
            }

            for (int i = 0; i < permutation.Length; i++)
            {
                var index = Array.IndexOf(permutation, i, 0, position);
                if (index != -1)
                    continue;
                permutation[position] = i;
                MakePermutations(permutation, position + 1, checkpoints);
            }
        }


        public static int[] FindBestCheckpointsOrder(Point[] checkpoints, TimeSpan timeout)
		{
			//������ 4
			// ��������� ���� �����, ����� �� ����� ������� ������������ �������� ������������� ��������, � ��������� ����� ����� �������� tim�out.

			var startTime = DateTime.Now;
			var bestFoundSolution = MakeTrivialPermutation(checkpoints.Length);
			//����...
			//���� � ��� ��������� ����� � ���������� ������ �� ����, ��� �����.
			if (DateTime.Now - startTime > timeout) return bestFoundSolution;
			//�����, ���������� ������.
			return bestFoundSolution;
		}


		#region ��������������� �������, ������ ������������ ��, ���� �����������.

		public static int[] MakeTrivialPermutation(int size)
		{
			var bestOrder = new int[size];
			for (int i = 0; i < bestOrder.Length; i++)
				bestOrder[i] = i;
			return bestOrder;
		}

		public static double GetPathLen(Point[] checkpoints, int[] order)
		{
			Point prevPoint = checkpoints[0];
			var len = 0.0;
			foreach (int checkpointIndex in order)
			{
				len += Distance(prevPoint, checkpoints[checkpointIndex]);
				prevPoint = checkpoints[checkpointIndex];
			}
			return len;
		}

		public static double Distance(Point a, Point b)
		{
			var dx = a.X - b.X;
			var dy = a.Y - b.Y;
			return Math.Sqrt(dx * dx + dy * dy);
		}
		#endregion
	}
}