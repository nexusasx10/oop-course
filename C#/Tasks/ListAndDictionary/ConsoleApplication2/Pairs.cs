﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApplication
{
    class Program
    {
        static void Main()
        {
            // Выводим строки файла в переменную
            string text = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "Text.txt"));
            // Удаляем из текста лишние символы и делим на предложения
            string pattern = "[^\\w'.!?;:()-]+";
            string replacement = " ";
            Regex rgx = new Regex(pattern);
            text = rgx.Replace(text, replacement);
            var separator = new[] { '.', '!', '?', ';', ':', '(', ')', '-' };
            var sentences = text.Split(separator);

            // Создаём список всех пар
            List<string> pairList = new List<string>();
            foreach (var sentence in sentences)
            {
                string[] words = sentence.Split(' ');
                for (var i = 0; i < words.Length - 1; i++)
                {
                    if (words[i] != "" && words[i + 1] != "")
                    {
                        var pair = words[i] + " " + words[i + 1];
                        pairList.Add(pair.ToLower());
                    }
                }
            }

            // Создаём словарь
            var dictionary = new Dictionary<string, int>();
            foreach (var e in pairList)
            {
                if (!dictionary.ContainsKey(e)) dictionary[e] = 0;
                dictionary[e]++;
            }

            // Выводим первые 50 самых часто встречающихся пар
            var j = 0;
            foreach (var pair in dictionary.OrderByDescending(pair => pair.Value))
            {
                if (j == 20) break;
                Console.WriteLine("{0:D2}: {1,-20} {2,4}", j + 1, pair.Key, pair.Value);
                j++;
            }
            Console.WriteLine();
            
            // Продолжение текста
            string firstWord = Console.ReadLine();
            string secondWord = "";
            string result = firstWord;
            var max = 0;
            for (var k = 0; k < 20; k++ )
            {
                foreach (var pair in dictionary)
                {
                    var key = pair.Key.Split(' ');
                    if (key[0] == firstWord && pair.Value > max)
                    {
                        secondWord = key[1];
                        max = pair.Value;
                    }
                }
                max = 0;
                if (secondWord != "")
                {
                    result = result + " " + secondWord;
                    firstWord = secondWord;
                    secondWord = "";
                }
                else
                {
                    Console.WriteLine("Совпадений не найдено");
                    break;
                }
            }
            Console.WriteLine(result);
            
            Console.ReadKey();
        }
    }
}
