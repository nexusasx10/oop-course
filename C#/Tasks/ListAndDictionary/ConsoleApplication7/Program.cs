﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //Выводим строки файла в переменную
            string text = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "Text.txt")); 

            //Удаляем из строки лишние символы и делим её по пробелу
            string pattern = "[^a-zA-Z']+";
            string replacement = " ";
            Regex rgx = new Regex(pattern);
            text = rgx.Replace(text, replacement);
            var wordsArr = text.Split(' ');

            //Создаём список всех слов
            List<string> wordList = new List<string>();
            var ignore = new[] { "a", "to", "of", "in", "for", "on", "at", "from", "about", "into", "the" };
            foreach (var word in wordsArr)
            {
                if (Array.IndexOf(ignore, word)<0)
                {
                    wordList.Add(word.ToLower());
                }
            }

            //Создаём словарь
            var dictionary = new Dictionary<string, int>();
            foreach (var e in wordList)
            {
                if (!dictionary.ContainsKey(e)) dictionary[e] = 0;
                dictionary[e]++;
            }

            // Выводим первые 50 самых часто встречающихся слов
            var j = 0;
            foreach (var pair in dictionary.OrderByDescending(pair => pair.Value))
            {
                if (j == 50) break;
                Console.WriteLine("{0:D2}: {1,-10} {2,4}", j+1, pair.Key, pair.Value);
                j++;
            }
            Console.ReadKey();
        }
    }
}
