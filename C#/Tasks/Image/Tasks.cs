﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recognizer
{
    class Tasks
    {
        /* Задача #1 (1 балл)
		 * Переведите изображение в серую гамму.
		 * 
		 * original[x,y,i] - это красный (i=0), зеленый (1) и синий (2) 
		 * каналы пикселя с координатами x,y. Каждый канал лежит в диапазоне от 0 до 255.
		 * 
		 * Получившийся массив должен иметь тот же размер, 
		 * grayscale[x,y] - яркость от 0 до 1 пикселя в координатах x,y
		 *
		 * Используйте формулу http://ru.wikipedia.org/wiki/Оттенки_серого
		 */
        public static double[,] Grayscale(byte[,,] original)
        {
            var grayscale = new double[original.GetLength(0), original.GetLength(1)];
            for (var x = 0; x < original.GetLength(0); x++)
            {
                for (var y = 0; y < original.GetLength(1); y++)
                {
                    grayscale[x, y] = (0.2126 * original[x, y, 0] + 0.7152 * original[x, y, 1] + 0.0722 * original[x, y, 2])/255;
                }
            }
            return grayscale;
        }


        /* Задача #2 (1 балл)
		 * Очистите переданное изображение от шума.
		 * 
		 * Пиксели шума (и только они) имеют черный цвет (0,0,0) или белый цвет (255,255,255)
		 * Вам нужно заменить эти цвета на средний цвет соседних пикселей.
		 * Соседними считаются 4 ближайших пиксела, если пиксел не у края изображения.
		 */
        public static void ClearNoise(byte[,,] original)
        {
            for (var x = 0; x < original.GetLength(0); x++)
            {
                for (var y = 0; y < original.GetLength(1); y++)
                {
                    var summOfColors = original[x, y, 0] + original[x, y, 1] + original[x, y, 2];
                    if (summOfColors == 0 || summOfColors == 765)
                    {
                        var replacedPixels = getColorFromCross(original, x, y);
                        original[x, y, 0] = replacedPixels[0];
                        original[x, y, 1] = replacedPixels[1];
                        original[x, y, 2] = replacedPixels[2];
                    }
                }
            }
        }
        public static byte[] getColorFromCross(byte[,,] original, int x, int y)
        {
            var newColors = new byte[3];
            for (var i = 0; i < 3; i++)
            {
                int summ = 0;
                byte count = 0;
                if (x != 0 && original[x - 1, y, 0] + original[x - 1, y, 1] + original[x - 1, y, 2] != 0 && original[x - 1, y, 0] + original[x - 1, y, 1] + original[x - 1, y, 2] != 765)
                {
                    summ += original[x - 1, y, i];
                    count++;
                }
                if (x != original.GetLength(0)-1 && original[x + 1, y, 0] + original[x + 1, y, 1] + original[x + 1, y, 2] != 0 && original[x + 1, y, 0] + original[x + 1, y, 1] + original[x + 1, y, 2] != 765)
                {
                    summ += original[x + 1, y, i];
                    count++;
                }
                if (y != 0 && original[x, y - 1, 0] + original[x, y - 1, 1] + original[x, y - 1, 2] != 0 && original[x, y - 1, 0] + original[x, y - 1, 1] + original[x, y - 1, 2] != 765)
                {
                    summ += original[x, y - 1, i];
                    count++;
                }
                if (y != original.GetLength(1)-1 && original[x, y + 1, 0] + original[x, y + 1, 1] + original[x, y + 1, 2] != 0 && original[x, y + 1, 0] + original[x, y + 1, 1] + original[x, y + 1, 2] != 765)
                {
                    summ += original[x, y + 1, i];
                    count++;
                }
                if (count > 0)
                    newColors[i] = (byte)(summ / count);
            }
            return newColors;
        }

        /* Задача #3 (1 балл)
		* Замените все цвета, встречающиеся у 10% самых ярких пикселей на белый, а остальные — на черный цвет. 
		* Этот процесс называется пороговая бинаризация изображения.
		*/
        public static void ThresholdFiltering(double[,] original)
        {
            for (var x = 0; x < original.GetLength(0); x++)
            {
                for (var y = 0; y < original.GetLength(1); y++)
                {
                    if (original[x, y] >= 0.9)
                    {
                        original[x, y] = 1;
                    }
                    else
                    {
                        original[x, y] = 0;
                    }
                }
            }
        }


        /* Задача #4 (1 балл)
		Разберитесь, как работает нижеследующий код (называемый фильтрацией Собеля), 
		и какое отношение к нему имеют эти матрицы:
		
		   | -1 -2 -1 |         | -1  0  1 |  
		Gx=|  0  0  0 |      Gy=| -2  0  2 |       
		   |  1  2  1 |         | -1  0  1 |    
		
		https://ru.wikipedia.org/wiki/%D0%9E%D0%BF%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D1%80_%D0%A1%D0%BE%D0%B1%D0%B5%D0%BB%D1%8F
		
		Попробуйте заменить фильтр Собеля 3x3 на фильтр Собеля 5x5 и сравните результаты. 
		http://www.cim.mcgill.ca/~image529/TA529/Image529_99/assignments/edge_detection/references/sobel.htm

		Выберите фильтр, который лучше выделяет границы на изображении.

		Обобщите код применения фильтра так, чтобы можно было передавать ему любые матрицы, любого нечетного размера.
		Фильтры Собеля размеров 3 и 5 должны быть частным случаем. 
		После такого обобщения менять фильтр Собеля одного размера на другой будет легко.
		*/
        public static double[,] SobelFiltering(double[,] g)
        {
            var width = g.GetLength(0);
            var height = g.GetLength(1);
            var result = new double[width, height];
            var matrixTemplate = new int[] { 1, 2, 1 };
            var matrixSize = matrixTemplate.Length;
            var matrixRadius = matrixSize / 2;
            for (int x = matrixRadius; x < width - matrixRadius; x++)
            {
                for (int y = matrixRadius; y < height - matrixRadius; y++)
                {
                    double gx = 0;
                    double gy = 0;
                    for (var i = 0; i < matrixSize; i++)
                    {
                        for (var j = 0; j < matrixSize; j++)
                        {
                            if (j < matrixRadius)
                            {
                                gx -= Math.Pow(2, j) * matrixTemplate[i] * g[(x - matrixRadius) + i, (y - matrixRadius) + j];
                                gy -= Math.Pow(2, j) * matrixTemplate[i] * g[(x - matrixRadius) + j, (y + matrixRadius) - i];
                            }
                            if (j > matrixRadius)
                            {
                                gx += Math.Pow(2, j) * matrixTemplate[i] * g[(x - matrixRadius) + i, (y - matrixRadius) + j];
                                gy += Math.Pow(2, j) * matrixTemplate[i] * g[(x - matrixRadius) + j, (y + matrixRadius) - i];
                            }
                        }
                    }
                    result[x, y] = Math.Sqrt(gx * gx + gy * gy);
                }
            }
            return result;
        }



        /* Задача #5 (без баллов): 
		Реализуйте или используйте готовый алгоритм Хафа для поиска аналитических координат прямых на изображений
		http://ru.wikipedia.org/wiki/Преобразование_Хафа
		*/
        public static Line[] HoughAlgorithm(double[,] original)
        {
            var width = original.GetLength(0);
            var height = original.GetLength(1);
            return new[] { new Line(0, 0, width, height), new Line(0, height, width, 0) };
        }


    }
}

