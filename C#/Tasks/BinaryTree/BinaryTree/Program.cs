﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            string result = "";
            List<double> expected = new List<double>();
            string expectedStr = "";
            var rand = new Random();
            Tree<double> tree = new Tree<double>();
            for (var i = 0; i < 25; i++)
            {
                var element = rand.NextDouble();
                tree.Add(element);
                expected.Add(element);
            }
            foreach (var e in tree)
            {
                result += e.ToString() + "\n";
            }
            expected.Sort();
            foreach (var e in expected.Select(s => s.ToString()))
                expectedStr += e + "\n";
                Console.Write(expectedStr == result);
        }
    }
}
