﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    public enum Sides
    {
        Left,
        Right,
        None
    }

    public class TreeNode<T>
    {
        public TreeNode(T value, TreeNode<T> parent = null, Sides side = Sides.None)
        {
            Value = value;
            Parent = parent;
            Side = side;
            SubtreeCount = 0;
        }

        public readonly T Value;

        public TreeNode<T> Left { get; set; }

        public TreeNode<T> Right { get; set; }

        public TreeNode<T> Parent { get; }

        public int SubtreeCount { get; set; }

        public Sides Side { get; }
    }
    
    public class Tree<T> : IEnumerable<T>
        where T : IComparable
    {
        public TreeNode<T> Root { get; set; }

        public Tree(IEnumerable<T> objects)
        {
            foreach (var obj in objects)
            {
                Add(obj);
            }
        }

        public Tree(params T[] objects)
            : this((IEnumerable<T>)objects)
        { }

        public Tree()
        { }

        public int Count
        {
            get
            {
                return Root.SubtreeCount + 1;
            }
        }

        public void Add(T key)
        {
            if (Root == null)
            {
                Root = new TreeNode<T>(key);
                return;
            }
            var current = Root;
            while (true)
            {
                if (key.CompareTo(current.Value) < 0)
                {
                    if (current.Left == null)
                    {
                        current.Left = new TreeNode<T>(key, current, Sides.Left);
                        current = current.Left;
                        while (true)
                        {
                            if (current.Parent != null)
                            {
                                current.Parent.SubtreeCount++;
                                current = current.Parent;
                            }
                            else
                                break;
                        }
                        break;
                    }
                    current = current.Left;
                }
                else
                {
                    if (current.Right == null)
                    {
                        current.Right = new TreeNode<T>(key, current, Sides.Right);
                        current = current.Right;
                        while (true)
                        {
                            if (current.Parent != null)
                            {
                                current.Parent.SubtreeCount++;
                                current = current.Parent;
                            }
                            else
                                break;
                        }
                        break;
                    }
                    current = current.Right;
                }
            }
        }

        public TreeNode<T> GetMinElement(TreeNode<T> root)
        {
            var current = root;
            while (true)
            {
                if (current.Left == null)
                {
                    return current;
                }
                current = current.Left;
            }
        }

        public TreeNode<T> GetMaxElement(TreeNode<T> root)
        {
            var current = root;
            while (true)
            {
                if (current.Right == null)
                {
                    return current;
                }
                current = current.Right;
            }
        }

        public T this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool Contains(T key)
        {
            var current = Root;
            while (true)
            {
                if (key.CompareTo(current.Value) == 0)
                {
                    return true;
                }
                else if(key.CompareTo(current.Value) < 0)
                {
                    if (current.Left == null)
                        return false;
                    current = current.Left;
                }
                else
                {
                    if (current.Right == null)
                        return false;
                    current = current.Right;
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            var begin = GetMinElement(Root);
            var end = GetMaxElement(Root);
            TreeNode<T> topRoot = null;
            var current = begin;
            while (true)
            {
                yield return current.Value;
                if (current == end)
                    break;
                if (current.Right != null)
                {
                    if (topRoot == null)
                        topRoot = current.Parent;
                    current = GetMinElement(current.Right);
                }
                else
                {
                    if (current.Side == Sides.Left)
                        current = current.Parent;
                    else
                    {
                        current = topRoot;
                        topRoot = null;
                    }                       
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
