﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BinaryTree.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void AddCheck()
        {
            var tree = new Tree<int>();
            tree.Add(2);
            tree.Add(1);
            tree.Add(3);
            tree.Add(1);
            tree.Add(0);

            Assert.AreEqual(2, tree.Root.Value);
            Assert.AreEqual(1, tree.Root.Left.Value);
            Assert.AreEqual(1, tree.Root.Left.Right.Value);
            Assert.AreEqual(3, tree.Root.Right.Value);
            Assert.AreEqual(0, tree.Root.Left.Left.Value);
        }

        [TestMethod]
        public void ContainsCheck()
        {
            var tree = new Tree<int>(42, 41, 44, 40, 41, 43, 45);

            Assert.AreEqual(true, tree.Contains(40));
            Assert.AreEqual(true, tree.Contains(41));
            Assert.AreEqual(true, tree.Contains(42));
            Assert.AreEqual(true, tree.Contains(43));
            Assert.AreEqual(true, tree.Contains(44));
            Assert.AreEqual(true, tree.Contains(45));
        }

        [TestMethod]
        public void EnumerableCheck()
        {
            string result = "";
            List<double> expected = new List<double>();
            string expectedStr = "";
            var rand = new Random();
            Tree<double> tree = new Tree<double>();
            for (var i = 0; i < 25; i++)
            {
                var element = rand.NextDouble();
                tree.Add(element);
                expected.Add(element);
            }
            foreach (var e in tree)
            {
                result += e.ToString() + "\n";
            }
            expected.Sort();
            foreach (var e in expected.Select(s => s.ToString()))
                expectedStr += e + "\n";
            Assert.AreEqual(expectedStr, result);
        }

        //[TestMethod]
        //public void IndexingCheck()
        //{
        //    var tree = new Tree<int>(3, 5, 7, 2, 4, 8, 6, 1);

        //    foreach (var e in tree)
        //        Assert.AreEqual(e, tree[e]);
        //}

        [TestMethod]
        public void AddPerformanceTest1()
        {
            var tree = new Tree<double>();
            var rand = new Random();
            for (var i = 0; i < 100000; i++)
                tree.Add(rand.NextDouble());
        }

        [TestMethod]
        public void AddPerformanceTest2()
        {
            var tree = new Tree<double>();
            var rand = new Random();
            for (var i = 0; i < 200000; i++)
                tree.Add(rand.NextDouble());
        }

        [TestMethod]
        public void AddPerformanceTest3()
        {
            var tree = new Tree<double>();
            var rand = new Random();
            for (var i = 0; i < 400000; i++)
                tree.Add(rand.NextDouble());
        }

        [TestMethod]
        public void EnumerablePerformanceTest1()
        {
            var tree = new Tree<double>();
            var rand = new Random();
            double a;
            for (var i = 0; i < 1000; i++)
                tree.Add(rand.NextDouble());
            foreach (var e in tree)
            {
                a = e;
            }
        }

        [TestMethod]
        public void EnumerablePerformanceTest2()
        {
            var tree = new Tree<double>();
            var rand = new Random();
            double a;
            for (var i = 0; i < 2000; i++)
                tree.Add(rand.NextDouble());
            foreach (var e in tree)
            {
                a = e;
            }
        }

        [TestMethod]
        public void EnumerablePerformanceTest3()
        {
            var tree = new Tree<double>();
            var rand = new Random();
            double a;
            for (var i = 0; i < 4000; i++)
                tree.Add(rand.NextDouble());
            foreach (var e in tree)
            {
                a = e;
            }
        }
    }
}
