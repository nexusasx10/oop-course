using System;
using System.Collections.Generic;

namespace yield
{
	public class DataPoint
	{
		public double X;
		public double OriginalY;
		public double ExpSmoothedY;
		public double AvgSmoothedY;
		public double MinY;
		public double MaxY;
	}
	
	public static class DataTask
	{
		public static IEnumerable<DataPoint> GetData(Random random)
		{
			return GenerateOriginalData(random).SmoothExponentialy(0.1).MovingAverage(5).MinMax(200);
		}

		public static IEnumerable<DataPoint> GenerateOriginalData(Random random)
		{
			var x = 0;
			while (true)
			{
				x++;
				var y = 10 * (1 - (x / 100) % 2) + 3 * Math.Sin(x / 40.0) + 2*random.NextDouble() - 1 + 3;
				yield return new DataPoint { X = x, OriginalY = y };
			}
		}

		public static IEnumerable<DataPoint> SmoothExponentialy(this IEnumerable<DataPoint> data, double alpha)
		{
            double prePoint = double.NaN;
            double expSmoothedY;
            foreach(var dataPoint in data)
            {
                if (double.IsNaN(prePoint))
                {
                   expSmoothedY = dataPoint.OriginalY;
                }
                else
                {
                   expSmoothedY = prePoint + alpha * (dataPoint.OriginalY - prePoint);
                }
                prePoint = expSmoothedY;
                yield return new DataPoint { X = dataPoint.X, OriginalY = dataPoint.OriginalY, ExpSmoothedY = expSmoothedY };
            }
        }

		public static IEnumerable<DataPoint> MovingAverage(this IEnumerable<DataPoint> data, int windowWidth)
		{   
            double sum = 0;
            int pointsCount = 0;
            double avgSmoothedY;
            Queue<double> calculationRange = new Queue<double>();
            foreach (var dataPoint in data)
            {
                calculationRange.Enqueue(dataPoint.ExpSmoothedY);
                sum += dataPoint.ExpSmoothedY;
                if (pointsCount == windowWidth)
                {
                    sum -= calculationRange.Dequeue();
                }
                else
                {
                    pointsCount++;
                }
                avgSmoothedY = sum / pointsCount;
                yield return new DataPoint { X = dataPoint.X, OriginalY = dataPoint.OriginalY, ExpSmoothedY = dataPoint.ExpSmoothedY, AvgSmoothedY = avgSmoothedY };
                
            }
        }
		
		public static IEnumerable<DataPoint> MinMax(this IEnumerable<DataPoint> data, int windowWidth)
		{
            double maxY = double.NaN;
            double minY = double.NaN;
            Queue<double> CalculationRange = new Queue<double>();
            foreach (var dataPoint in data)
            {
                if (double.IsNaN(maxY))
                {
                    yield return new DataPoint { X = dataPoint.X, OriginalY = dataPoint.OriginalY, ExpSmoothedY = dataPoint.ExpSmoothedY, AvgSmoothedY = dataPoint.AvgSmoothedY, MaxY = dataPoint.AvgSmoothedY, MinY = dataPoint.AvgSmoothedY };
                }
                maxY = double.NegativeInfinity;
                minY = double.PositiveInfinity;
                CalculationRange.Enqueue(dataPoint.OriginalY);
                if (CalculationRange.Count == windowWidth)
                {
                    CalculationRange.Dequeue();
                }
                foreach (var value in CalculationRange)
                {
                    if (value < minY)
                        minY = value;
                    if (value > maxY)
                        maxY = value;
                }
                yield return new DataPoint { X = dataPoint.X, OriginalY = dataPoint.OriginalY, ExpSmoothedY = dataPoint.ExpSmoothedY, AvgSmoothedY = dataPoint.AvgSmoothedY, MaxY = maxY, MinY = minY };
            }
        }
	}
}