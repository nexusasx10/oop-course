﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace func_rocket
{
	public class GameForm : Form
	{
        private Func<Rocket, Vector, Turn> controller;
		private Image rocket;
        private Image robot;
        private Image target;
		private Timer timer;
		private ComboBox spaces;
		private GameSpace space;
		private bool right;
		private bool left;
        private int[] scores;

		public GameForm(IEnumerable<GameSpace> gameSpaces, Func<Rocket, Vector, Turn> controller)
		{
			DoubleBuffered = true;
            scores = new int[] { 0, 0 };
            Text = "";
            UpdateText();
            rocket = Image.FromFile("images/rocket.png");
            robot = Image.FromFile("images/robot.png");
            target = Image.FromFile("images/target.png");
			timer = new Timer();
			timer.Interval = 10;
			timer.Tick += TimerTick;
			timer.Start();
			WindowState = FormWindowState.Maximized;
			spaces = new ComboBox();
            spaces.Width = 150;
			KeyPreview = true;
			spaces.SelectedIndexChanged += SpaceChanged;
			spaces.Parent = this;
			spaces.Items.AddRange(gameSpaces.Cast<object>().ToArray());
			Controls.Add(spaces);
            this.controller = controller;
			this.Focus();
		}

        private void UpdateText()
        {
            Text = "Use Left and Right arrows to control rocket. Scores: |you| " + scores[0] + " : " + scores[1] + " |bot|";
        }

		protected override void OnShown(EventArgs e)
		{
            MessageBox.Show("Ready to the race?", "Rocket Races");
            base.OnShown(e);
			if (spaces.Items.Count > 0) 
				spaces.SelectedIndex = 0;
            spaces.Enabled = false;
		}

		private void SpaceChanged(object sender, EventArgs e)
		{
			space = (GameSpace) spaces.SelectedItem;
			timer.Start();
		}

		private void TimerTick(object sender, EventArgs e)
		{
			if (space == null) return;
			Turn controlRocket = left ? Turn.Left : (right ? Turn.Right : Turn.None);
            Turn controlRobot = controller(space.Robot, space.Target);
            space.Move(ClientRectangle, controlRocket, controlRobot);

            bool rocketGets = (space.Rocket.Location - space.Target).Length < 20;
            bool robotGets = (space.Robot.Location - space.Target).Length < 20;
            if (robotGets || rocketGets)
            {
                timer.Stop();
                if (rocketGets)
                    scores[0]++;
                if (robotGets)
                    scores[1]++;
                UpdateText();
                if (spaces.SelectedIndex != spaces.Items.Count - 1)
                {
                    spaces.SelectedIndex += 1;
                }
                else
                {
                    if (scores[0] + scores[1] < 5)
                    {
                        if (scores[0] > scores[1])
                            MessageBox.Show("You are cheater! And that brings you to victory");
                        else
                            MessageBox.Show("You are cheater! Anyway, you lose");
                    }
                    else
                    {
                        if (scores[0] > scores[1])
                            MessageBox.Show("Congratulations, you won!");
                        else
                            MessageBox.Show("You lose, try again");
                        
                    }
                    Environment.Exit(0);
                }
                
            }
			Invalidate();
			Update();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			if (e.KeyCode == Keys.Left) left = true;
			if (e.KeyCode == Keys.Right) right = true;
            if (e.KeyCode == Keys.N)
                if (spaces.SelectedIndex != spaces.Items.Count - 1)
                {
                    spaces.SelectedIndex += 1;
                }
        }

		protected override void OnKeyUp(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			if (e.KeyCode == Keys.Left) left = false;
			if (e.KeyCode == Keys.Right) right = false;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			var g = e.Graphics;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.FillRectangle(Brushes.Beige, ClientRectangle);

			if (space == null) return;

			DrawGravity(g);
			var matrix = g.Transform;

			g.TranslateTransform((float)space.Target.X, (float)space.Target.Y);
			g.DrawImage(target, new Point(-target.Width / 2, -target.Height / 2));

			if (timer.Enabled)
			{
				g.Transform = matrix;
				g.TranslateTransform((float) space.Rocket.Location.X, (float) space.Rocket.Location.Y);
				g.RotateTransform(90 + (float) (space.Rocket.Direction * 180/Math.PI));
				g.DrawImage(rocket, new Point(-rocket.Width/2, -rocket.Height/2));

                g.Transform = matrix;
                g.TranslateTransform((float)space.Robot.Location.X, (float)space.Robot.Location.Y);
                g.RotateTransform(90 + (float)(space.Robot.Direction * 180 / Math.PI));
                g.DrawImage(robot, new Point(-robot.Width / 2, -robot.Height / 2));
            }
		}

		private void DrawGravity(Graphics g)
		{
			Action<Vector, Vector> draw = (a,b) => g.DrawLine(Pens.DeepSkyBlue, (int)a.X, (int)a.Y, (int)b.X, (int)b.Y);
			for (int x = 0; x < ClientRectangle.Width; x += 50)
				for (int y = 0; y < ClientRectangle.Height; y += 50)
				{
					var p1 = new Vector(x, y);
					var v = space.Gravity(p1);
					var p2 = p1 + 20 * v;
					var end1 = p2 - 8*v.Rotate(0.5);
					var end2 = p2 - 8*v.Rotate(-0.5);
					draw(p1, p2);
					draw(p2, end1);
					draw(p2, end2);
				}
		}
	}
}