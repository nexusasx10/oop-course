using System;
using System.Collections.Generic;

namespace func_rocket
{
    public static class RandomExtensions
    {
        public static double NextDouble(this Random random, double min, double max)
        {
            return random.NextDouble() * (max - min) + min;
        }
        public static bool NextBool(this Random random)
        {
            return random.NextDouble() > 0.5;
        }
    }

	public class Tasks
	{
        static Random random = new Random();
        static Vector GetTargetPosition
        {
            get
            {
                return new Vector(random.NextDouble(100, 1000), random.NextDouble(50, 200));
            }
        }
        static Vector GetRocketPosition
        {
            get
            {
                return new Vector(random.NextDouble(100, 1000), random.NextDouble(700, 900));
            }
        }

        public static IEnumerable<GameSpace> GetGameSpaces()
		{
            var rocketPosition = GetRocketPosition;
            var targetPosition = GetTargetPosition;


            yield return new GameSpace(
                "Level 1: Zero-gravity",
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                targetPosition,
                _ => Vector.Zero
            );


            Vector position;
            yield return new GameSpace(
                "Level 2: White-hole",
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                position = targetPosition,
                _ =>
                {
                    var vector = (position - _);
                    var distanse = vector.Length;
                    vector = Vector.Zero - vector.Normalize() * (50 * distanse / Math.Pow(distanse + 1, 2));
                    return vector.Length > 50 ? Vector.Zero : vector;
                }
            );

            
            var anomalyVector = new Vector(1,0);
            yield return new GameSpace(
                "Level 3: Anomaly",
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                targetPosition,
                _ => anomalyVector = anomalyVector.Rotate(Math.PI / 13500)
            );

            
            yield return new GameSpace(
                "Level 4: Heavy",
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                targetPosition,
                _ => new Vector(0, 0.9)
            );


            var Wvector = new Vector(0, -10);
            yield return new GameSpace(
                "Level 5: Strings of wind",
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                new Rocket(rocketPosition, Vector.Zero, 1.5 * Math.PI),
                targetPosition,
                _ =>
                {
                    return Wvector.Rotate(random.NextDouble(-0.01, 0.01));
                }
            );
        }

		public static Turn ControlRocket(Rocket rocket, Vector target)
		{
            Vector targetDirection = target - rocket.Location;
            Vector offset = (new Vector(target.X, rocket.Location.Y) - target);
            double correctDirection = (targetDirection + (offset * 0.7)).Angle;
            Turn turn = Turn.None;

            if (Math.Abs(rocket.Location.X - target.X) < 5)
                rocket.Velocity = new Vector(0, rocket.Velocity.Y);

            if (rocket.Direction < correctDirection)
            {
                turn = Turn.Right;
            }
            else if (rocket.Direction > correctDirection)
            {
                turn = Turn.Left;
            }
            return turn;
        }
	}
}