﻿using System;

namespace Geometry
{
    public static class Geometry
    {
        public static Segment CreateSegment(Point pointA, Point pointB)
        {
            return new Segment 
            { 
                A = pointA,
                B = pointB,
                
                Length = DistanceBetweenPoints(pointA, pointB) };
        }

        public static Rectangle CreateRectangle(Point pointA, Point pointB, Point pointC)
        {
            return new Rectangle 
            {
                A = pointA,
                B = pointB,
                C = pointC,
                D = new Point { X = pointA.X + Math.Abs(pointB.X - pointC.X), Y = pointA.Y - Math.Abs(pointB.Y - pointC.Y) },
                Width = DistanceBetweenPoints(pointA, pointB),
                Height = DistanceBetweenPoints(pointB, pointC) 
            };
        }

        public static double DistanceBetweenPoints(Point firstPoint, Point secondPoint)
        {
            return Math.Sqrt(Math.Pow(firstPoint.X - secondPoint.X, 2) + Math.Pow(firstPoint.Y - secondPoint.Y, 2));
        }

        public static bool IsPointBelongsToSegment(Point point, Segment segment)
        {
            return segment.Length == DistanceBetweenPoints(point, segment.A) + DistanceBetweenPoints(point, segment.B);
        }
    }

    public class Point
    {
        public double X;
        public double Y;
        public bool BelongsToSegment(Segment segment)
        {
            //return segment.Length == Geometry.DistanceBetweenPoints(this, segment.A) + Geometry.DistanceBetweenPoints(this, segment.B);

            return Geometry.IsPointBelongsToSegment(this, segment);
        }
    }

    public class Segment
    {
        public Point A;
        public Point B;
        public double Length;
        public bool IncludePoint(Point point)
        {
            //return this.Length == Geometry.DistanceBetweenPoints(point, this.A) + Geometry.DistanceBetweenPoints(point, this.B);

            return Geometry.IsPointBelongsToSegment(point, this);
        }
    }

    public class Rectangle
    {
        public Point A;
        public Point B;
        public Point C;
        public Point D;
        public double Width;
        public double Height;
    }
}
