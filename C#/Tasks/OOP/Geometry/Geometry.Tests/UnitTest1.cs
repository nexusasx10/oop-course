﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Geometry.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void DistanceBetweenPointsNormal()
        {
            var pointA = new Point { X = 0, Y = 0 };
            var pointB = new Point { X = 0, Y = 1 };

            var result = Geometry.DistanceBetweenPoints(pointA, pointB);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void DistanceBetweenPointsNull()
        {
            var pointA = new Point { X = 0, Y = 0 };
            var pointB = new Point { X = 0, Y = 0 };

            var result = Geometry.DistanceBetweenPoints(pointA, pointB);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void PointBelongsToSegment()
        {
            var segment = Geometry.CreateSegment(new Point { X = 0, Y = 0 }, new Point { X = 4, Y = 4 });
            var point = new Point { X = 2, Y = 2 };

            var result = Geometry.IsPointBelongsToSegment(point, segment);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void PointNotBelongsToSegment()
        {
            var segment = Geometry.CreateSegment(new Point { X = 0, Y = 0 }, new Point { X = 4, Y = 4 });
            var point = new Point { X = 2, Y = 0 };

            var result = Geometry.IsPointBelongsToSegment(point, segment);

            Assert.AreEqual(false, result);
        }

         [TestMethod]
        public void BelongsToSegment()
        {
            var segment = Geometry.CreateSegment(new Point { X = 0, Y = 0 }, new Point { X = 4, Y = 4 });
            var point = new Point { X = 2, Y = 2 };

            var result = point.BelongsToSegment(segment);

            Assert.AreEqual(true, result);
        }

         [TestMethod]
         public void IncludePoint()
         {
             var segment = Geometry.CreateSegment(new Point { X = 0, Y = 0 }, new Point { X = 4, Y = 4 });
             var point = new Point { X = 2, Y = 2 };

             var result = segment.IncludePoint(point);

             Assert.AreEqual(true, result);
         }
        
        //Уже не нужно?

        //[TestMethod]
        //public void DistanceBetweenPointsNormal()
        //{
        //    var pointA = new Point { X = 0, Y = 0 };
        //    var pointB = new Point { X = 0, Y = 0 };

        //    var segment = Geometry.DistanceBetweenPoints(pointA, pointB);

        //    Assert.AreEqual(0, segment.Length);
        //}
        
    }
}
