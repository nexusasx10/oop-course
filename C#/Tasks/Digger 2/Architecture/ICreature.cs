﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digger
{
    public interface ICreature
    {
        string ImageFileName { get; }
        int DrawingPriority { get; }
        CreatureCommand Act(Game game, int x, int y);
        bool DeadInConflict(Game game, ICreature conflictedObject);
    }
}