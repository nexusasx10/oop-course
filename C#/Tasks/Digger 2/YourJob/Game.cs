﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.IO;

namespace Digger
{
    public class Terrain : ICreature
    {
        public string ImageFileName
        {
            get { return "Terrain.png"; }
        }
        
        public int DrawingPriority
        {
            get { return 3; }
        }

        public bool DeadInConflict(Game game, ICreature conflictedObject)
        {
            if (conflictedObject is Digger || conflictedObject is Monster)
                return true;
            return false;
        }

        public CreatureCommand Act(Game game, int x, int y)
        {
            return new CreatureCommand { DeltaX=0, DeltaY=0 };
        }
    }

    public class Digger : ICreature
    {
        public string ImageFileName
        {
            get { return "Digger.png"; }
        }

        public int DrawingPriority
        {
            get { return 2; }            
        }
        
        public bool DeadInConflict(Game game, ICreature conflictedObject)
        {
            if (conflictedObject is Sack)
            {
                var sack = (Sack)conflictedObject;
                if (sack.fallen)
                {
                    game.IsOver = true;
                    return true;
                }
            }
            if (conflictedObject is Monster)
                return true;
            return false;
        }
        
        public CreatureCommand Act(Game game, int x, int y)
        {
            var dx = 0;
            var dy = 0;
            var done = false;
            if ((Keyboard.IsKeyDown(Key.W) || Keyboard.IsKeyDown(Key.Up)) && !done)
            {
                dy--;
                done = true;
            }
            if ((Keyboard.IsKeyDown(Key.A) || Keyboard.IsKeyDown(Key.Left)) && !done)
            {
                dx--;
                done = true;
            }
            if ((Keyboard.IsKeyDown(Key.S) || Keyboard.IsKeyDown(Key.Down)) && !done)
            {
                dy++;
                done = true;
            }
            if ((Keyboard.IsKeyDown(Key.D) || Keyboard.IsKeyDown(Key.Right)) && !done)
            {
                dx++;
                done = true;
            }
            if (x + dx >= game.MapWidth || x + dx < 0)
                dx = 0;
            if (y + dy >= game.MapHeight || y + dy < 0)
                dy = 0;
            if (game.Map[x + dx, y + dy] is Sack)
            {
                dx = 0;
                dy = 0;
            }
            return new CreatureCommand { DeltaX = dx, DeltaY = dy };
        }
    }

    public class Monster : ICreature
    {
        public string ImageFileName
        {
            get { return "Monster.png"; }
        }

        public int DrawingPriority
        {
            get { return 1; }
        }

        public bool DeadInConflict(Game game, ICreature conflictedObject)
        {
            if (conflictedObject is Sack)
            {
                var sack = (Sack)conflictedObject;
                if (sack.fallen)
                {
                    game.IsOver = true;
                    return true;
                }
            }
            return false;
        }

        public CreatureCommand Act(Game game, int x, int y)
        {
            var dx = 0;
            var dy = 0;
            var done = false;
            if (Keyboard.IsKeyDown(Key.W) && !done)
            {
                dy--;
                done = true;
            }
            if (Keyboard.IsKeyDown(Key.A) && !done)
            {
                dx--;
                done = true;
            }
            if (Keyboard.IsKeyDown(Key.S) && !done)
            {
                dy++;
                done = true;
            }
            if (Keyboard.IsKeyDown(Key.D) && !done)
            {
                dx++;
                done = true;
            }
            if (x + dx >= game.MapWidth || x + dx < 0)
                dx = 0;
            if (y + dy >= game.MapHeight || y + dy < 0)
                dy = 0;
            if (game.Map[x + dx, y + dy] is Sack)
            {
                dx = 0;
                dy = 0;
            }
            return new CreatureCommand { DeltaX = dx, DeltaY = dy };
        }
    }

    public class Sack : ICreature
    {
        public bool fallen = false;
        public string ImageFileName
        {
            get { return "Sack.png"; }
        }

        public int DrawingPriority
        {
            get { return 1; }
        }

        public bool DeadInConflict(Game game, ICreature conflictedObject)
        {
            if (conflictedObject is Terrain)
            {
                return true;
            }
            return false;
        }

        public CreatureCommand Act(Game game, int x, int y)
        {
            var dx = 0;
            var dy = 0;
                
            if (y + 1 < game.MapHeight && (game.Map[x, y + 1] == null || game.Map[x, y + 1] is Digger || game.Map[x, y + 1] is Monster))
            {
                fallen = true;
                dy++;
            }
            if (x + dx < 0 || x + dx >= game.MapWidth)
            {
                dx = 0;                
            }
            if (y + dy < 0 || y + dy >= game.MapHeight)
            {
                dy = 0;
            }
            if (fallen && (y == game.MapHeight-1 || game.Map[x, y + 1] is Terrain || game.Map[x, y + 1] is Sack || game.Map[x, y + 1] is Gold))
            {
                return new CreatureCommand { DeltaX = dx, DeltaY = dy, TransformTo = new Gold() };
            }
            return new CreatureCommand { DeltaX = dx, DeltaY = dy};
        }
    }

    public class Gold : ICreature
    {
        public string ImageFileName
        {
            get { return "Gold.png"; }
        }

        public int DrawingPriority
        {
            get { return 4; }
        }

        public bool DeadInConflict(Game game, ICreature conflictedObject)
        {
            if (conflictedObject is Digger || conflictedObject is Monster)
            {
                game.Scores += 100;
                return true;
            }
            return false;
        }

        public CreatureCommand Act(Game game, int x, int y)
        {
            var dx = 0;
            var dy = 0;
            if (y + 1 < game.MapHeight && game.Map[x, y + 1] == null)
                dy++;
            if (x + dx >= game.MapWidth || x + dx < 0)
                dx = 0;
            if (y + dy >= game.MapHeight || y + dy < 0)
                dy = 0;
                return new CreatureCommand { DeltaX = dx, DeltaY = dy};
        }
    }

    public class Game
    {
        public ICreature[,] Map { get; set; }
        public int MapWidth { get; private set; }
        public int MapHeight { get; private set; }
        public int Scores { get; set; }
        public bool IsOver { get; set; }
        
        public Game()
        {
            MapHeight = 18;
            MapWidth = 32;
            Scores = 0;
            IsOver = false;
            string[] MapFromFile;
            bool useMapFromFile = true;
            if (File.Exists("Map.txt") && useMapFromFile)
            {
                MapFromFile = File.ReadAllLines("Map.txt");
                MapWidth = MapFromFile[0].Length;
                MapHeight = MapFromFile.Length-1;
                Map = new ICreature[MapWidth, MapHeight];
                for (var i = 0; i < MapHeight; i++)
                {
                    for (var j = 0; j < MapWidth; j++)
                    {
                        switch (MapFromFile[i][j])
                        {
                            case 'D':
                                {
                                    Map[j, i] = new Digger();
                                    break;
                                }
                            case 'T':
                                {
                                    Map[j, i] = new Terrain();
                                    break;
                                }
                            case 'S':
                                {
                                    Map[j, i] = new Sack();
                                    break;
                                }
                            case 'G':
                                {
                                    Map[j, i] = new Gold();
                                    break;
                                }
                            case 'M':
                                {
                                    Map[j, i] = new Monster();
                                    break;
                                }
                        }
                    }
                }
            }
            else
            {
                Map = new ICreature[MapWidth, MapHeight];
                var random = new Random();

                for (var i = 0; i < MapHeight; i++)
                {
                    for (var j = 0; j < MapWidth; j++)
                    {
                        Map[j, i] = new Terrain();
                    }
                }

                for (var i = 0; i < MapHeight - 1; i++)
                {
                    for (var j = 0; j < MapWidth; j++)
                    {
                        if (random.NextDouble() > 0.95)
                        {
                            Map[j, i] = new Sack();
                        }
                    }
                }

                for (var i = 0; i < MapHeight - 1; i++)
                {
                    for (var j = 0; j < MapWidth; j++)
                    {
                        if (random.NextDouble() > 0.99)
                        {
                            Map[j, i] = new Monster();
                        }
                    }
                }
                
                Map[0, 0] = new Digger();
            }
        }
    }
}
