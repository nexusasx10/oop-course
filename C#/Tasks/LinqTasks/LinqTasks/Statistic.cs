﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethods;
using System.IO;

namespace LinqTasks
{
    class Statistic
    {
        public static Dictionary<string, string[]> GetSlides()
        {
            string[] slides = File.ReadAllLines("Statistic/slides.txt");
            return slides
                .Skip(1)
                .Select(s => s.Split(';'))
                .GroupBy(s => s[1])
                .ToDictionary(group => group.Key, group => group.Select(s => s[0]).ToArray());
        }

        public static Dictionary<string, string[][]> GetTime()
        {
            // образец AF2DDCC68861428091BBC437CA9D825F; 3645CCF5FA0A4DAFAD5356A58C8827B1; 2014-12-12; 10:43:22.860
            string[] visits = File.ReadAllLines("Statistic/visits.txt");
            var time = visits
                .Skip(1)
                .Select(s => s.Split(';'))
                .GroupBy(s => s[0])
                .ToDictionary(group => group.Key, group => group.GetBigrams().Select(x =>
                {
                    double res = 
                    (int.Parse(x.Item2[2].Split('-')[2]) * 1440
                    + int.Parse(x.Item2[3].Split(':')[0]) * 60
                    + int.Parse(x.Item2[3].Split(':')[1]))
                    -
                    (int.Parse(x.Item1[2].Split('-')[2]) * 1440
                    + int.Parse(x.Item1[3].Split(':')[0]) * 60
                    + int.Parse(x.Item1[3].Split(':')[1]));

                    return res > 1 && res < 120 ? res : -1;
                }).Where(x => x!=-1).Median());
            return visits
                .Skip(1)
                .Select(s => s.Split(';'))
                .GroupBy(s => s[0])
                .ToDictionary(group => group.Key, group => group.ToArray());
        }

        //public static Dictionary<string, string[][]> GetUserBySlide(Dictionary<string, string[]> slides)
        //{
            
        //    return slides
                
        //}

        //public static int CountTime(Dictionary<string, string[][]> dictionary)
        //{
        //    return dictionary
        //        .Where(s => s.Value.Where(s => s[]))
        //}


        //public static void PrintStatistics(string[] data)
        //{
        //    var T1 = CountTime(GetUserBySlide(GetSlides("theory")));
        //    Console.WriteLine(data.Select(s => s));
        //}
    }
} 
