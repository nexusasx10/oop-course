﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods
{
    public static class EnumerableExtensions
    {
        public static double Median(this IEnumerable<double> items)
        {
            var count = items.ToList().Count;
            return items
                .OrderBy(x => x)
                .Take(count / 2 + 1)
                .Skip(count % 2 == 0 ? count / 2 - 1 : count / 2)
                .Average();
        }
        public static IEnumerable<Tuple<T, T>> GetBigrams<T>(this IEnumerable<T> items)
        {
            var prev = items.First();
            foreach (var item in items.Skip(1))
            {
                yield return Tuple.Create(prev, item);
                prev = item;
            }
        }
    }
}
