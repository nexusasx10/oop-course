﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using ExtensionMethods;
using System.Collections;
using LinqTasks;
using System.Collections.Generic;

namespace LinqTasks.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void EvenSequenseMedian()
        {
            Assert.AreEqual(2.75, EnumerableExtensions.Median(new double[] { 1.1, 2.2, 3.3, 4.4 }));
        }

        [TestMethod]
        public void OddSequenseMedian()
        {
            Assert.AreEqual(2.2, EnumerableExtensions.Median(new double[] { 1.1, 2.2, 3.3 }));
        }

        [TestMethod]
        public void SingularSequenseMedian()
        {
            Assert.AreEqual(4.12, EnumerableExtensions.Median(new double[] { 4.12 }));
        }

        [TestMethod]
        public void SingularSequenseBigrams()
        {
            Assert.IsTrue(new double[] { 1 }.GetBigrams().Count() == 0);
        }

        [TestMethod]
        public void DualSequenseBigrams()
        {
            var s = new Tuple[] { Tuple.Create(1, 2) };
            var s2 = new Tuple[] { Tuple.Create(1, 2) };
            Assert.AreEqual(
                new Tuple<double, double>[] { Tuple.Create(1.0, 2.0) },
                new double[] { 1, 2 }.GetBigrams().ToArray()
                );
        }

        [TestMethod]
        public void TripleSequenseBigrams()
        {
            Assert.AreEqual(
                new Tuple<double, double>[] { Tuple.Create(1.0, 2.0), Tuple.Create(2.0, 3.0) },
                new double[] { 1, 2, 3 }.GetBigrams().ToArray()
                );
        }
        
    }
}
