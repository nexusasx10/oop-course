﻿using System;

namespace autocomplete
{
	// Внимание!
	// Есть одна распространенная ловушка при сравнении строк: строки можно сравнивать по-разному:
	// с учетом регистра, без учета, зависеть от кодировки и т.п.
	// В файле словаря все слова отсортированы мтодом StringComparison.OrdinalIgnoreCase.
	// Во всех функциях сравнения строк в C# можно передать способ сравнения.
    class Autocompleter
    {
        private readonly string[] items;

        public Autocompleter(string[] loadedItems)
        {
            items = loadedItems;
        }

        // Найти произвольный элемент словаря, начинающийся с prefix.
        // Ускорьте эту фунцию так, чтобы она работала за O(log(n))
        public string FindByPrefix(string prefix)
        {
            var leftEdge = 0;
            var rightEdge = items.Length - 1;
            while (leftEdge < rightEdge)
            {
                var middle = (rightEdge + leftEdge) / 2;
                if (string.Compare(prefix, items[middle]) <= 0)
                    rightEdge = middle;
                else leftEdge = middle + 1;
            }
            if (items[leftEdge].StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
                return items[rightEdge];

            //foreach (var item in items)
            //{
            //    if (item.StartsWith(prefix, StringComparison.OrdinalIgnoreCase)) return item;
            //}
            return null;
        }

        // Найти первые (в порядке следования в файле) 10 (или меньше, если их меньше 10) элементов словаря, 
        // начинающийся с prefix.
        // Эта функция должна работать за O(log(n) + count)
        public string[] FindByPrefix(string prefix, int count)
        {
            var result = new string[count];
            for (var j = 0; j < count; j++)
            {
                result[j] = "";
            }
            var leftEdge = 0;
            var rightEdge = items.Length - 1;
            while (leftEdge < rightEdge)
            {
                var middle = (rightEdge + leftEdge) / 2;
                if (String.Compare(prefix, items[middle]) <= 0)
                {
                    rightEdge = middle;
                }
                else leftEdge = middle + 1;
            }
            for (var i = 0; i < count; i++)
            {
                if (leftEdge + i < items.Length)
                {
                    if (items[leftEdge + i].StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
                    {
                        result[i] = items[leftEdge + i];
                    }
                }
            }
            return result;
        }

        // Найти количество слов словаря, начинающихся с prefix
        // Эта функция должна работать за O(log(n))
        public int FindCount(string prefix)
        {
            var i = 0;
            var itemsCount = 0;
            var leftEdge = 0;
            var rightEdge = items.Length - 1;
            while (leftEdge < rightEdge)
            {
                var middle = (rightEdge + leftEdge) / 2;
                if (String.Compare(prefix, items[middle]) <= 0)
                {
                    rightEdge = middle;
                }
                else leftEdge = middle + 1;
            }
            while (items[leftEdge + i].StartsWith(prefix, StringComparison.OrdinalIgnoreCase) && leftEdge + i < items.Length-1)
                {
                    itemsCount++;
                    i++;
                }
            return itemsCount;
        }
    }
}
