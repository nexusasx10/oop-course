﻿using System;

namespace hashes
{
	public class GhostKey
	{
		public string Name { get; private set; }

		public GhostKey(string name)
		{
			if (name == null) throw new ArgumentNullException("name");
			Name = name;
		}

		public void DoSomething()
		{
            DoSomething();
        }
	}
}