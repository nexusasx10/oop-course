﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace hashes
{
	public class ReadonlyBytes : IEnumerable<byte>
	{
		private readonly byte[] bytes;

        public int Length { get; set; }


		public ReadonlyBytes(byte[] bytes)
		{
			this.bytes = bytes;
            Length = bytes.Length;
		}

        public byte this[int index]
        {
            get
            {
                if (index < 0 || index > Length - 1)
                    throw new IndexOutOfRangeException();
                return bytes[index];
            }
            set
            {
                if (index < 0 || index > Length - 1)
                    throw new IndexOutOfRangeException();
                bytes[index] = value;
            }
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<byte> GetEnumerator()
        {
            for (var i = 0; i < Length; i++)
            {
                yield return bytes[i];
            }
        }
    }
}