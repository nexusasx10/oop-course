﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Digger
{
    public class Terrain : ICreature
    {
        public string GetImageFileName()
        {
            return "Terrain.png";
        }

        public int GetDrawingPriority()
        {
            return 3;
        }

    public bool DeadInConflict(ICreature conflictedObject)
        {
            if (conflictedObject is Digger)
                return true;
            return false;

        }

        public CreatureCommand Act(int x, int y)
        {
            return new CreatureCommand { DeltaX=0, DeltaY=0 };
        }
    }

    public class Digger : ICreature
    {
        public string GetImageFileName()
        {
            return "Digger.png";
        }

        public int GetDrawingPriority()
        {
            return 2;
        }
        
        public bool DeadInConflict(ICreature conflictedObject)
        {
            if (conflictedObject is Sack)
            {
                var sack = (Sack)conflictedObject;
                if (sack.isFalling)
                {
                    Game.IsOver = true;
                    return true;
                }
            }
            return false;
        }
        
        public CreatureCommand Act(int x, int y)
        {
            var dx = 0;
            var dy = 0;
            var done = false;
            if (Keyboard.IsKeyDown(Key.W) && !done)
            {
                dy--;
                done = true;
            }
            if (Keyboard.IsKeyDown(Key.A) && !done)
            {
                dx--;
                done = true;
            }
            if (Keyboard.IsKeyDown(Key.S) && !done)
            {
                dy++;
                done = true;
            }
            if (Keyboard.IsKeyDown(Key.D) && !done)
            {
                dx++;
                done = true;
            }
            if (x + dx >= Game.MapWidth || x + dx < 0)
                dx = 0;
            if (y + dy >= Game.MapHeight || y + dy < 0)
                dy = 0;
            if (Game.Map[x + dx, y + dy] is Sack)
            {
                dx = 0;
                dy = 0;
            }
            return new CreatureCommand { DeltaX = dx, DeltaY = dy };
        }
    }

    public class Sack : ICreature
    {
        public bool isFalling;
        public bool fell;
        public string GetImageFileName()
        {
            return "Sack.png";
        }

        public int GetDrawingPriority()
        {
            return 1;
        }

        public bool DeadInConflict(ICreature conflictedObject)
        {
            if (conflictedObject is Terrain)
            {
                return true;
            }
            return false;
        }

        public CreatureCommand Act(int x, int y)
        {
            var dx = 0;
            var dy = 0;
            if (y + 1 < Game.MapHeight)
                if (Game.Map[x, y + 1] == null || Game.Map[x, y + 1] is Digger)
                {
                    isFalling = true;
                    fell = true;
                    dy++;
                }
            if (x + dx >= Game.MapWidth || x + dx < 0)
                dx = 0;
            if (y + dy >= Game.MapHeight || y + dy < 0)
                dy = 0;
            if (y == Game.MapHeight - 1)
            {
                isFalling = false;
                return new CreatureCommand { DeltaX = dx, DeltaY = dy, TransformTo = new Gold() };
            }
            if (y + 1 < Game.MapHeight)
                if (Game.Map[x, y + 1] != null)
                    if (dy == 0 && Game.Map[x, y + 1] is Terrain && Game.Map[x, y - 1] == null)
                    {
                        isFalling = false;
                        return new CreatureCommand { DeltaX = dx, DeltaY = dy, TransformTo = new Gold() };
                    }
            return new CreatureCommand { DeltaX = dx, DeltaY = dy};
        }
    }

    public class Gold : ICreature
    {
        public string GetImageFileName()
        {
            return "Gold.png";
        }

        public int GetDrawingPriority()
        {
            return 4;
        }

        public bool DeadInConflict(ICreature conflictedObject)
        {
            var name = conflictedObject.GetType().Name;
            if (name == "Digger")
            {
                Game.Scores += 1000000;
                return true;
            }
            return false;
        }

        public CreatureCommand Act(int x, int y)
        {
            var dx = 0;
            var dy = 0;
            if (y + 1 < Game.MapHeight)
                if (Game.Map[x, y + 1] == null)
                    dy++;
            if (x + dx >= Game.MapWidth || x + dx < 0)
                dx = 0;
            if (y + dy >= Game.MapHeight || y + dy < 0)
                dy = 0;
                return new CreatureCommand { DeltaX = dx, DeltaY = dy};
        }
    }

    public class Game
    {
        public static ICreature[,] Map;
        public static int MapWidth;
        public static int MapHeight;
        public static int Scores;
        public static bool IsOver;


        public static void CreateMap()
        {
            MapHeight = 30;
            MapWidth = 30;
            Map = new ICreature[MapHeight, MapWidth];            
            for(var i = 0; i < MapHeight; i++)
            {
                for (var j = 0; j < MapWidth; j++)
                {
                    Map[i, j] = new Terrain();
                }
            }
            
            Map[0, 0] = new Digger();

            for (var i = 1; i < MapWidth-1; i++)
            {
                for (var j = 1; j < MapHeight-1; j++)
                {
                    Map[i, j] = new Sack();
                }
            }
            Scores = 0;
            IsOver = false;
        }
    }
}
