using System;
using System.Collections.Generic;

namespace func.brainfuck
{
    public class Brainfuck
    {
        public static void Run(string program, Func<int> read, Action<char> write)
        {
            var runner = new ProgramRunner(program);
            runner.RegisterCommand('<', b => 
            {
                b.MemoryPointer--;
            });
            runner.RegisterCommand('>', b => 
            {
                b.MemoryPointer++;
            });
            runner.RegisterCommand('+', b => 
            {
                b.Memory[b.MemoryPointer]++;
            });
            runner.RegisterCommand('-', b => 
            {
                b.Memory[b.MemoryPointer]--;
            });
            runner.RegisterCommand('.', b => 
            {
                write(Convert.ToChar(b.Memory[b.MemoryPointer]));
            });
            runner.RegisterCommand(',', b => 
            {
                b.Memory[b.MemoryPointer] = Convert.ToByte(read());
            });
            for (var i = '0'; i <= '9'; i++)
            {
                runner.RegisterCommand(i, b => 
                {
                    b.Memory[b.MemoryPointer] = Convert.ToByte(b.Program[b.InstructionPointer]);
                });
            }
            for (var i = 'A'; i <= 'Z'; i++)
            {
                runner.RegisterCommand(i, b => 
                {
                    b.Memory[b.MemoryPointer] = Convert.ToByte(b.Program[b.InstructionPointer]);
                });
            }
            for (var i = 'a'; i < 'z'; i++)
            {
                runner.RegisterCommand(i, b => 
                {
                    b.Memory[b.MemoryPointer] = Convert.ToByte(b.Program[b.InstructionPointer]);
                });
            }
            runner.RegisterCommand('[', b =>
            {
                if (b.Memory[b.MemoryPointer] == 0)
                {
                    b.InstructionPointer = b.Loops[b.InstructionPointer];
                }
            });
            runner.RegisterCommand(']', b =>
            {
                if (b.Memory[b.MemoryPointer] != 0)
                {
                    b.InstructionPointer = b.Loops[b.InstructionPointer];
                }
            });

            runner.Run();
        }
    }


    public class ProgramRunner
    {
        private Dictionary<char, Action<ProgramRunner>> Commands = new Dictionary<char, Action<ProgramRunner>>();
        public List<char> Program = new List<char>();
        public byte[] Memory = new byte[30000];
        private int memoryPointer;
        public int MemoryPointer
        {
            get
            {
                return memoryPointer;
            }
            set
            {
                memoryPointer = value;
                if (memoryPointer < 0)
                {
                    memoryPointer += Memory.Length;
                }
                if (memoryPointer > Memory.Length - 1)
                {
                    memoryPointer -= Memory.Length;
                }
            }
        }
        public int InstructionPointer { get; set; }
        public Stack<Tuple<int, char>> LoopPairs = new Stack<Tuple<int, char>>();
        public Dictionary<int, int> Loops = new Dictionary<int, int>();

        public ProgramRunner(string program)
        {
            for (var command = 0; command < program.Length; command++)
            {
                if (program[command] == '[')
                {
                    LoopPairs.Push(new Tuple<int, char>(command, '['));
                }
                if (program[command] == ']' && LoopPairs.Peek().Item2 == '[')
                {
                    Loops.Add(LoopPairs.Peek().Item1, command);
                    Loops.Add(command, LoopPairs.Pop().Item1);
                }
                Program.Add(program[command]);
            }
        }

        public void RegisterCommand(char name, Action<ProgramRunner> executeCommand)
        {
            Commands.Add(name, executeCommand);
        }

        public void Run()
        {
            for (InstructionPointer = 0; InstructionPointer < Program.Count; InstructionPointer++)
            {
                Console.Write(Program[InstructionPointer]);
                if (Commands.ContainsKey(Program[InstructionPointer]))
                {
                    Commands[Program[InstructionPointer]](this);
                }
            }
        }
    }
}