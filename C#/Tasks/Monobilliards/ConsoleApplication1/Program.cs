﻿using System;
using System.Collections.Generic;

public class Monobilliards
{
    private static void Main()
    {   String [] process  = Console.ReadLine().Split(' ');
        int inputLength = int.Parse(process[0]);
        int[] input = new int[inputLength];
        for (int i = 0; i < inputLength; i++)
        {
            String[] tokens = Console.ReadLine().Split(' ');
            input[i] = int.Parse(tokens[0]);
        }
        Check(inputLength, input);
    }
    public static void Check(int inputLength, int[] input)
    { 
        Stack<int> pocket = new Stack<int>();
        int current = 0;
        
        for (var i = 1; i <= 2*inputLength; i++)
        {
            if (i <= inputLength)
            {
                pocket.Push(i);
            }
            if (pocket.Count == 0)
                break;
            else if (pocket.Peek() == input[current])
            {
                pocket.Pop();
                current++;
            }
        }
        if (pocket.Count == 0)
        {
            Console.WriteLine("Not a proof");
        }
        else
        {
            Console.WriteLine("Cheater");
        }
        Console.ReadKey();
    }
}