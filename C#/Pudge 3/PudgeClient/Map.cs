﻿using System;
using System.Collections.Generic;

namespace PudgeClient
{
    public class WayPoint
    {
        public enum runeType
        {
            None,
            Normal,
            Large
        }

        public readonly int X;
        public readonly int Y;
        public readonly runeType Rune;

        public WayPoint(int x, int y, runeType rune)
        {
            X = x;
            Y = y;
            Rune = rune;
        }
        
        public override string ToString()
        {
            return string.Format("({0}, {1}, rune: {2})", X, Y, Rune);
        }
    }

    class Map
    {
        public WayPoint[] Points;
        public Graph Graph;
        public Dictionary<Edge, double> Weights;

        public Map(params WayPoint[] points)
        {
            Points = points;
            Graph = new Graph(points.Length);
            Weights = new Dictionary<Edge, double>();
            foreach (var edge in Graph.Edges)
            {
                var ind1 = edge.First.Number;
                var ind2 = edge.Second.Number;
                Weights.Add(edge, Math.Sqrt(Math.Pow(Points[ind1].X - Points[ind2].X, 2) + Math.Pow(Points[ind1].Y - Points[ind2].Y, 2)));
            }
        }
    }
}