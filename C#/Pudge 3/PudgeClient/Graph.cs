﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PudgeClient
{
    public class Node
    {
        private readonly List<Edge> incidentEdges = new List<Edge>();
        public readonly int Number;
        public Node(int number)
        {
            Number = number;
        }
        public IEnumerable<Node> IncidentNodes
        {
            get
            {
                return incidentEdges.Select(z => z.OtherNode(this));
            }
        }
        public IEnumerable<Edge> IncidentEdges
        {
            get
            {
                return incidentEdges;
            }
        }
        public static void Connect(Node first, Node second)
        {
            var edge1 = new Edge(first, second);
            var edge2 = new Edge(second, first);
            first.incidentEdges.Add(edge1);
            second.incidentEdges.Add(edge2);
        }
        public override string ToString()
        {
            return Number.ToString();
        }
    }

    public class Edge
    {
        public readonly Node First;
        public readonly Node Second;
        public Edge(Node first, Node second)
        {
            First = first;
            Second = second;
        }
        public bool IsIncident(Node node)
        {
            return First == node || Second == node;
        }
        public Node OtherNode(Node node)
        {
            if (!IsIncident(node)) throw new ArgumentException();
            if (First == node) return Second;
            return Second;
        }
        public override string ToString()
        {
            return string.Format("[{0},{1}]", First.ToString(), Second.ToString());
        }
    }

    public class Graph
    {
        private Node[] nodes;
        public Graph(int nodesCount)
        {
            nodes = Enumerable.Range(0, nodesCount).Select(z => new Node(z)).ToArray();
        }
        public int Length { get { return nodes.Length; } }
        public Node this[int index] { get { return nodes[index]; } }
        public Edge this[Node first, Node second] { get { return Edges.Where(x => (x.First == first && x.Second == second) || (x.First == second && x.Second == first)).First(); } }
        public IEnumerable<Node> Nodes
        {
            get
            {
                foreach (var node in nodes) yield return node;
            }
        }
        public IEnumerable<Edge> Edges
        {
            get
            {
                return nodes.SelectMany(z => z.IncidentEdges).Distinct();
            }
        }
        public void Connect(int index1, int index2)
        {
            Node.Connect(nodes[index1], nodes[index2]);
        }
    }
}