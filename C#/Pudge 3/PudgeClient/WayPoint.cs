﻿using System;
using System.Linq;

namespace PudgeClient
{
    public class WayPoint
    {
        public enum runeType
        {
            None,
            Normal,
            Large
        }

        public static readonly WayPoint[] Points;
        public readonly int X;
        public readonly int Y;
        public readonly runeType Rune;
        public readonly int Number;

        static WayPoint()
        {
            Points = new WayPoint[]
            {
                new WayPoint(-130, -130, runeType.None, 0),

                new WayPoint(-70, -120, runeType.None, 1),
                new WayPoint(-120, -70, runeType.None, 2),
                new WayPoint(-90, -90, runeType.None, 3),

                new WayPoint(0, -100, runeType.Normal, 4),
                new WayPoint(-100, 0, runeType.Normal, 5),

                new WayPoint(-30, 30, runeType.None, 6),
                new WayPoint(-88, 88, runeType.None, 7),
                new WayPoint(-130, 130, runeType.Large, 8),

                new WayPoint(0, 0, runeType.Large, 9),

                new WayPoint(130, -130, runeType.Large, 10),
                new WayPoint(88, -88, runeType.None, 11),
                new WayPoint(30, -30, runeType.None, 12),

                new WayPoint(100, 0, runeType.Normal, 13),
                new WayPoint(0, 100, runeType.Normal, 14),

                new WayPoint(90, 90, runeType.None, 15),
                new WayPoint(120, 70, runeType.None, 16),
                new WayPoint(70, 120, runeType.None, 17),

                new WayPoint(130, 130, runeType.None, 18)
            };
        }

        public WayPoint(int x, int y, runeType rune, int number)
        {
            X = x;
            Y = y;
            Rune = rune;
            Number = number;
        }
        
        public override string ToString()
        {
            return string.Format("{0}) X = {1}, Y = {2}, rune: {3}", Number, X, Y, Rune);
        }

        public static bool IsOnWayPoint(CVARC.V2.LocatorItem coordinats, WayPoint wayPoint, int accuracy = 10)
        {
            if (coordinats == null)
                return true;
            var current = GetWayPoint(coordinats, accuracy);
            if (current == null)
                return false;
            if (current.Number == wayPoint.Number)
                return true;
            return false;
        }

        public static WayPoint GetWayPoint(CVARC.V2.LocatorItem coordinats, int accuracy = 10)
        {
            if (coordinats == null)
                return null;
            var currentPoint = Points
                  .Where(p => Math.Abs(p.X - coordinats.X) < accuracy && Math.Abs(p.Y - coordinats.Y) < accuracy)
                  .ToArray();
            if (currentPoint.Length == 1)
                return currentPoint[0];
            else
                return null;
        }
    }
}