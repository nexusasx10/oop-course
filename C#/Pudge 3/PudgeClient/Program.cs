﻿using System;
using Pudge;
using Pudge.Player;
using System.Collections.Generic;
using System.Linq;

namespace PudgeClient
{
    class Program
    {
        const string CvarcTag = "e6dd0704-a9d4-4ff0-b252-30a9edbcb5fe";

        static void Print(PudgeSensorsData data)
        {
            Console.WriteLine("---------------------------------");
            if (data.IsDead)
            {
                Console.WriteLine("Ooops, i'm dead :(");
                return;
            }
            Console.WriteLine("I'm here: " + data.SelfLocation);
            Console.WriteLine("My score now: {0}", data.SelfScores);
            Console.WriteLine("Current time: {0:F}", data.WorldTime);
            foreach (var rune in data.Map.Runes)
                Console.WriteLine("Rune! Type: {0}, Size = {1}, Location: {2}", rune.Type, rune.Size, rune.Location);
            foreach (var heroData in data.Map.Heroes)
                Console.WriteLine("Enemy! Type: {0}, Location: {1}, Angle: {2:F}", heroData.Type, heroData.Location, heroData.Angle);
            foreach (var eventData in data.Events)
                Console.WriteLine("I'm under effect: {0}, Duration: {1}", eventData.Event,
                    eventData.Duration - (data.WorldTime - eventData.Start));
            Console.WriteLine("---------------------------------");
            Console.WriteLine();
        }

        static void ConnectNodes(Graph graph)
        {
            graph.Connect(0, 1);
            graph.Connect(0, 2);
            graph.Connect(0, 3);
            graph.Connect(1, 4);
            graph.Connect(2, 5);
            graph.Connect(3, 9);
            graph.Connect(4, 11);
            graph.Connect(5, 7);
            graph.Connect(6, 7);
            graph.Connect(6, 9);
            graph.Connect(7, 8);
            graph.Connect(9, 12);
            graph.Connect(9, 15);
            graph.Connect(10, 11);
            graph.Connect(11, 12);
            graph.Connect(11, 13);
            graph.Connect(13, 16);
            graph.Connect(14, 17);
            graph.Connect(15, 18);
            graph.Connect(16, 18);
            graph.Connect(17, 18);
        }

        static bool IsOnWaypoint(CVARC.V2.LocatorItem location, WayPoint point)
        {
            return Math.Abs(location.X - point.X) + Math.Abs(location.Y - point.Y) <= 15;
        }

        static bool NotMove(CVARC.V2.LocatorItem previous, CVARC.V2.LocatorItem current)
        {
            if (previous == null || current == null) return false;
            var previousVector = new Vector(previous.X, previous.Y);
            var currentVector = new Vector(current.X, current.Y);
            var differenceVector = currentVector - previousVector;
            return (differenceVector.Length <= 0.5) ? true : false;
        }

        static PudgeSensorsData MoveTo(PudgeSensorsData startState, PudgeClientLevel3 client, WayPoint start, WayPoint destination)
        {
            var currentState = startState;
            var destinationVector = new Vector(destination.X, destination.Y);
            while (!IsOnWaypoint(currentState.SelfLocation, destination))
            {
                var myVector = new Vector(currentState.SelfLocation.X, currentState.SelfLocation.Y);
                var toDestination = destinationVector - myVector;
                var angle = toDestination.Angle - currentState.SelfLocation.Angle;
                if (Math.Abs(angle) > 1)
                    client.Rotate(angle);
                var previousState = currentState;
                currentState = client.Move(5);
                while (NotMove(previousState.SelfLocation, currentState.SelfLocation))
                {
                    client.Rotate(5);
                    currentState = client.Move(5);
                    previousState = currentState;
                }
                //if (currentState.Map.Heroes.Count != 0)
                //    currentState = MeetingWithOneGuy(currentState, client);
            }
            return currentState;
        }

        static Dictionary<WayPoint, double> UpdateDistanses(CVARC.V2.LocatorItem position, Map map)
        {
            var res = new Dictionary<WayPoint, double>();
            var currentPosition = map.Points.Where(p => Math.Abs(position.X - p.X) + Math.Abs(position.Y - p.Y) <= 15).ToArray();
            var currentPositionNumber = Array.IndexOf(map.Points, currentPosition[0]);
            if (currentPosition.Length < 1) throw new Exception("Can't define self location");
            double distance;
            var iterCount = map.Graph.Nodes.ToArray().Length;
            for (var i = 0; i < iterCount; i++)
            {
                if (map.Points[i].Rune != WayPoint.runeType.None)
                {
                    var path = Dijkstra.FindPath(map.Graph, map.Weights, map.Graph[currentPositionNumber], map.Graph[i]);
                    distance = GetPathLength(path, map.Graph, map.Weights);
                    res[map.Points[i]] = distance;
                }
            }
            return res;
        }

        static double GetPathLength(List<Node> path, Graph graph, Dictionary<Edge, double> weights)
        {
            double distance = 0;
            for (var i = 0; i < path.Count-1; i++)
                distance += weights[graph[path[i], path[i-1]]];
            return distance;
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
                args = new[] { "127.0.0.1", "14000" };
            var ip = args[0];
            var port = int.Parse(args[1]);

            var client = new PudgeClientLevel3();

            var currentState = client.Configurate(ip, port, CvarcTag);

            var map = new Map(
                new WayPoint(-130, -130, WayPoint.runeType.None),

                new WayPoint(-70, -120, WayPoint.runeType.None),
                new WayPoint(-120, -70, WayPoint.runeType.None),
                new WayPoint(-90, -90, WayPoint.runeType.None),

                new WayPoint(0, -100, WayPoint.runeType.Normal),
                new WayPoint(-100, 0, WayPoint.runeType.Normal),

                new WayPoint(30, -30, WayPoint.runeType.None),
                new WayPoint(-88, 88, WayPoint.runeType.None),
                new WayPoint(-130, 130, WayPoint.runeType.Large),

                new WayPoint(0, 0, WayPoint.runeType.Large),

                new WayPoint(130, -130, WayPoint.runeType.Large),
                new WayPoint(88, -88, WayPoint.runeType.None),
                new WayPoint(30, -30, WayPoint.runeType.None),

                new WayPoint(100, 0, WayPoint.runeType.Normal),
                new WayPoint(0, 100, WayPoint.runeType.Normal),

                new WayPoint(90, 90, WayPoint.runeType.None),
                new WayPoint(120, 70, WayPoint.runeType.None),
                new WayPoint(70, 120, WayPoint.runeType.None),

                new WayPoint(130, 130, WayPoint.runeType.None)
                );
            ConnectNodes(map.Graph);

            var wayPointsDistanses = new Dictionary<WayPoint, double>();

            while (true)
            {
                currentState = client.Wait(0.1);
                var currentPoints = map.Points.Where(p => IsOnWaypoint(currentState.SelfLocation, p)).ToArray();
                if (currentPoints.Length < 1) throw new Exception("Can't define self location.");
                var currentPoint = currentPoints[0];
                wayPointsDistanses = UpdateDistanses(currentState.SelfLocation, map);
                var shortestWay = Double.PositiveInfinity;
                var destination = new WayPoint(0, 0, WayPoint.runeType.None);
                foreach (var el in wayPointsDistanses)
                {
                    if (el.Value < shortestWay)
                    {
                        shortestWay = el.Value;
                        destination = el.Key;
                    }
                }

                var path = Dijkstra.FindPath(map.Graph, map.Weights, map.Graph[Array.IndexOf(map.Points, currentPoint)], map.Graph[Array.IndexOf(map.Points, destination)]).ToList();
                var pathToGo = new List<WayPoint>();
                foreach(var node in path)
                {
                    pathToGo.Add(map.Points[node.Number]);
                }

                foreach (var el in pathToGo.Skip(1))
                {
                    currentState = MoveTo(currentState, client, currentPoint, el);
                    currentPoint = el;
                }
            }
        }
    } 
}
