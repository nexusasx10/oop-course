package lexer.converters;


import lexer.IConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;
import utils.Pair;


public class TextConverter implements IConverter {
    private final String text;
    private final String type;

    public TextConverter(String text, String type) {
        this.text = text;
        this.type = type;
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (input.startsWith(this.text)) {
            return new Pair<>(
                new Token(this.getType(), input.substring(0, this.text.length())),
                input.substring(this.text.length())
            );
        }
        return null;
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        return this.text;
    }

    @Override
    public String getType() {
        return this.type;
    }

    public String getText() {
        return this.text;
    }
}