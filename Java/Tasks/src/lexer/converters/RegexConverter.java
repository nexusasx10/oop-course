package lexer.converters;


import lexer.IConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;
import utils.Pair;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegexConverter implements IConverter {
    private final Pattern pattern;
    private final String type;
    private int lastLength = 0;

    public RegexConverter(String regex, String type) {
        this.pattern = Pattern.compile(regex, Pattern.DOTALL);
        this.type = type;
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (matchNext(input)) {
            return new Pair<>(
                new Token(this.type, input.substring(0, getLastLength())),
                input.substring(getLastLength())
            );
        }
        return null;
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        Token tToken = (Token)token;
        return tToken.getValue().get("value");
    }

    @Override
    public String getType() {
        return this.type;
    }

    public boolean matchNext(String regex, String input) {
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(input);
        lastLength = matcher.find() ? matcher.end() : 0;
        return (lastLength != 0) && (matcher.start() == 0);
    }

    public boolean matchNext(String input) {
        Matcher matcher = this.pattern.matcher(input);
        lastLength = matcher.find() ? matcher.end() : 0;
        return (lastLength != 0) && (matcher.start() == 0);
    }

    public String readSpace(String input) {
        if (matchNext("\\s*", input)) {
            input = input.substring(getLastLength());
        }
        return input;
    }

    public int getLastLength() {
        return this.lastLength;
    }
}