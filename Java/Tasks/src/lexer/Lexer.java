package lexer;


import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.tokens.Token;
import utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;


public class Lexer {
    private Hashtable<String, AbstractSyntax> languages = new Hashtable<>();

    public void register(AbstractSyntax syntax) {
        languages.put(syntax.getName(), syntax);
    }

    public ArrayList<AbstractToken> getTokens(String language, String input) {
        if (!languages.containsKey(language))
            return null;
        AbstractSyntax languageSyntax = languages.get(language);
        ArrayList<AbstractToken> tokens = parseTokens(input, languageSyntax.getReaders());
        ArrayList<ComplexToken> toProcess = new ArrayList<>();
        for (AbstractToken token : tokens) {
            if (token instanceof ComplexToken) {
                toProcess.add((ComplexToken)token);
            }
        }
        for (int i = 0; i < toProcess.size(); i ++) {
            ComplexToken token = toProcess.get(i);
            HashMap<String, ArrayList<AbstractToken>> value = new HashMap<>();
            for (String key : token.getValueBuffer().keySet()) {
                ArrayList<AbstractToken> inner = parseTokens(token.getValueBuffer().get(key), languages.get(language).getReaders());
                value.put(key, inner);
                for (AbstractToken inToken : inner) {
                    if (inToken instanceof ComplexToken) {
                        toProcess.add((ComplexToken)inToken);
                    }
                }
            }
            token.process(value);
        }
        return tokens;
    }

    private ArrayList<AbstractToken> parseTokens(String input, ArrayList<IReader> readers) {
        ArrayList<AbstractToken> tokens = new ArrayList<>();
        while (input.length() > 0) {
            Pair<AbstractToken, String> result = null;
            for (IReader reader : readers) {
                Pair<AbstractToken, String> buffer = reader.tryReadToken(input);
                if (buffer != null && (result == null || result.getSecond().length() > buffer.getSecond().length())) {
                    result = buffer;
                }
            }
            if (result != null) {
                tokens.add(result.getFirst());
                input = result.getSecond();
            } else {
                tokens.add(new Token("unknown", input.substring(0, 1)));
                input = input.substring(1);
            }
        }
        return tokens;
    }
}