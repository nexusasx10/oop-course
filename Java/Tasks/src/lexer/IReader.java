package lexer;


import lexer.tokens.AbstractToken;
import utils.Pair;


public interface IReader {
    Pair<AbstractToken, String> tryReadToken(String input);
    String getType();
}