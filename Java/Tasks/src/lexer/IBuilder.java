package lexer;


import lexer.tokens.AbstractToken;
import lexer.tokens.Token;


public interface IBuilder {
    String tryBuildCode(AbstractToken token);
    String getType();
}