package lexer;


import lexer.tokens.AbstractToken;

import java.util.ArrayList;
import java.util.HashMap;


public interface IStaticTyped {
    HashMap<String, String> getTypeModel(ArrayList<AbstractToken> tokens);
    void applyTypeModel(ArrayList<AbstractToken> tokens, HashMap<String, String> typeModel);
}