package lexer;

import lexer.IReader;
import lexer.IBuilder;


public interface IConverter extends IReader, IBuilder {}