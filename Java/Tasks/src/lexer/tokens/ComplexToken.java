package lexer.tokens;


import java.util.ArrayList;
import java.util.HashMap;


public class ComplexToken extends AbstractToken {
    private HashMap<String, String> valueBuffer;
    private HashMap<String, ArrayList<AbstractToken>> value;

    public ComplexToken(String type, HashMap<String, String> value) {
        super(type);
        this.valueBuffer = value;
    }

    public HashMap<String, ArrayList<AbstractToken>> getValue() {
        return this.value;
    }

    public ArrayList<AbstractToken> getValue(String key) {
        if (this.isProcessed()) {
            return this.value.get(key);
        } else {
            return null;
        }
    }

    public HashMap<String, String> getValueBuffer() {
        return this.valueBuffer;
    }

    public String getValueBuffer(String key) {
        if (!this.isProcessed()) {
            return this.valueBuffer.get(key);
        } else {
            return null;
        }
    }

    public void process(HashMap<String, ArrayList<AbstractToken>> value) {
        this.valueBuffer = null;
        this.value = value;
    }

    public void deProcess(HashMap<String, String> value) {
        this.value = null;
        this.valueBuffer = value;
    }

    public boolean isProcessed() {
        return this.value != null;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("<%s(", this.getType()));
        boolean first = true;
        if (this.isProcessed()) {
            for (String key : this.getValue().keySet()) {
                if (!first) {
                    result.append(", ");
                } else {
                    first = false;
                }
                result.append(String.format("%s: ", key));
                for (AbstractToken token : this.getValue().get(key)) {
                    result.append(token);
                }
            }
        } else {
            for (String key : this.getValueBuffer().keySet()) {
                if (!first) {
                    result.append(", ");
                } else {
                    first = false;
                }
                result.append(String.format("%s: %s", key, this.getValueBuffer().get(key)));
            }
        }
        result.append(")>");
        return result.toString();
    }
}