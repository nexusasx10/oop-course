package lexer.tokens;


import java.util.HashMap;

public class Token extends AbstractToken {
    private HashMap<String, String> value;

    public Token(String type, HashMap<String, String> value) {
        super(type);
        this.value = value;
    }

    public Token(String type, String value) {
        this(type, new HashMap<String, String>() {{put("value", value);}});
    }

    public HashMap<String, String> getValue() {
        return this.value;
    }

    public String getValue(String key) {
        return this.value.get(key);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("<%s(", this.getType()));
        boolean first = true;
        for (String key : this.getValue().keySet()) {
            if (!first) {
                result.append(", ");
            } else {
                first = false;
            }
            result.append(String.format("%s: %s", key, this.getValue().get(key)));
        }
        result.append(")>");
        return result.toString();
    }
}