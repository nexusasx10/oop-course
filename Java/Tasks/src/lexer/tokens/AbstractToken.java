package lexer.tokens;


public abstract class AbstractToken {
    private final String type;

    public AbstractToken(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public abstract Object getValue();

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof AbstractToken))
            return false;
        AbstractToken other = (AbstractToken)obj;
        return type.equals(other.type);
    }

    @Override
    public int hashCode() {
        return this.type.hashCode();
    }
}