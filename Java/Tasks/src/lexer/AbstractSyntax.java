package lexer;


import java.util.ArrayList;
import java.util.HashMap;


public abstract class AbstractSyntax {
    private ArrayList<IReader> readers;
    private HashMap<String, IBuilder> builders;

    public AbstractSyntax() {
        this.readers = new ArrayList<>();
        this.builders = new HashMap<>();
    }

    public void addToken(IReader reader, IBuilder builder) {
        this.readers.add(reader);
        this.builders.put(builder.getType(), builder);
    }

    public void addToken(IConverter converter) {
        this.addToken(converter, converter);
    }

    public boolean containsToken(String type) {
        return this.builders.keySet().contains(type);
    }

    public ArrayList<IReader> getReaders() {
        return this.readers;
    }

    public IBuilder getBuilder(String type) {
        return this.builders.get(type);
    }

    public abstract String getName();

    public abstract boolean isCaseSensitive();

    public boolean isStaticTyped() {
        return this instanceof IStaticTyped;
    }
}