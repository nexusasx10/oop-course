package languages.math;


import lexer.AbstractSyntax;
import lexer.converters.RegexConverter;
import lexer.converters.TextConverter;


public class MathSyntax extends AbstractSyntax {

    public MathSyntax() {
        addToken(new NumberConverter());
        addToken(new VectorConverter());
        addToken(new TextConverter("+", "operator_sum"));
        addToken(new TextConverter("-", "operator_diff"));
        addToken(new TextConverter("*", "operator_mul"));
        addToken(new TextConverter("/", "operator_div"));
        addToken(new TextConverter("(", "open_bracket"));
        addToken(new TextConverter(")", "close_bracket"));
        addToken(new IncorrectOperationConverter());
    }

    @Override
    public String getName() {
        return "Math";
    }

    @Override
    public boolean isCaseSensitive() {
        return true;
    }
}