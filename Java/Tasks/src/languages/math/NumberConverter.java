package languages.math;


import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;
import utils.Pair;

import java.util.HashMap;

public class NumberConverter extends RegexConverter{
    public NumberConverter() {
        super("", "number");
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        String number = "\\d+(\\.\\d+)?";
        String real;
        String imag;
        if (matchNext(number + "i", input)) {
            real = "0";
            imag = input.substring(0, getLastLength() - 1);
            input = input.substring(getLastLength());
        } else if (matchNext("i", input)) {
            real = "0";
            imag = "1";
            input = input.substring(1);
        } else if (matchNext(number, input)) {
                real = input.substring(0, getLastLength());
                imag = "0";
                input = input.substring(getLastLength());
        } else {
                return null;
        }
        String realF = real;
        String imagF = imag;
        HashMap<String, String> value = new HashMap<String, String>() {
            { put("real", realF); put("imag", imagF); }
        };
        return new Pair<>(new Token(this.getType(), value), input);
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        String result = "";
        Token tokenT = (Token)token;
        Double dReal = Double.parseDouble(tokenT.getValue().get("real"));
        Double dImag = Double.parseDouble(tokenT.getValue().get("imag"));
        String real = dReal.toString();
        String imag = dImag.toString();
        if (dReal != 0) {
            if (dReal > 0) {
                result += real;
            } else {
                result += "(" + real + ")";
            }
        } else if (dImag == 0) {
            return "0";
        }
        if (dImag != 0) {
            if (dReal != 0) {
                if (dImag > 0) {
                    result += " + " + imag + "i";
                } else {
                    result += " " + imag + "i";
                }
            } else {
                if (dImag > 0) {
                    result += imag + "i";
                } else {
                    result += "(" + imag + "i" + ")";
                }
            }
        }
        return result;
    }
}