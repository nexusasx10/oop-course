package languages.math;

import lexer.IConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;


public class IncorrectOperationConverter implements IConverter {
    @Override
    public String tryBuildCode(AbstractToken token) {
        StringBuilder result = new StringBuilder();
        HashMap<String, String> value = ((ComplexToken)token).getValueBuffer();
        boolean first = true;
        ArrayList<String> keys = new ArrayList<>(value.keySet());
        keys.sort(String::compareTo);
        for (String key : keys) {
            if (!first) {
                result.append(" + ");
            } else {
                first = false;
            }
            result.append(value.get(key));
        }
        return result.toString();
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        return null;
    }

    @Override
    public String getType() {
        return "incorrect";
    }
}
