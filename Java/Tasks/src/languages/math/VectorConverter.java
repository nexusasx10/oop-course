package languages.math;


import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.converters.RegexConverter;
import utils.Pair;

import java.util.HashMap;


public class VectorConverter extends RegexConverter {
    public VectorConverter() {
        super("", "vector");
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (!matchNext("<", input)) {
            return null;
        }
        input = input.substring(1);
        HashMap<String, String> components = new HashMap<>();
        Integer i = 0;
        boolean start = true;
        do {
            if (!start) {
                input = input.substring(1);
            }
            start = false;
            StringBuilder component = new StringBuilder();
            while (!matchNext(",", input) && ! matchNext(">", input)) {
                component.append(input.substring(0, 1));
                input = input.substring(1);
            }
            components.put(i.toString(), component.toString());
            i++;
        } while (matchNext(",", input));
        if (!matchNext(">", input)) {
            return null;
        }
        input = input.substring(1);
        return new Pair<>(new ComplexToken(this.getType(), components), input);
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        ComplexToken complexToken = (ComplexToken)token;
        StringBuilder result = new StringBuilder("<");
        HashMap<String, String> value = complexToken.getValueBuffer();
        boolean start = true;
        for (String key : value.keySet()) {
            if (!start) {
                result.append(", ");
            }
            result.append(value.get(key));
            start = false;
        }
        result.append(">");
        return result.toString();
    }
}