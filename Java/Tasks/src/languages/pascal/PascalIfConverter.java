package languages.pascal;


import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import utils.CorrectWrapperParser;
import utils.Pair;

import java.util.HashMap;


public class PascalIfConverter extends RegexConverter {
    public PascalIfConverter() {
        super("if", "condition");
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (!matchNext(input)) {
            return null;
        }
        input = input.substring(2);
        input = readSpace(input);
        String condition = "";
        while (!matchNext("then", input)) {
            condition += input.substring(0, 1);
            input = input.substring(1);
            if (input.length() == 0) {
                return null;
            }
        }
        input = input.substring(4);
        input = readSpace(input);
        StringBuilder trueCode = new StringBuilder();
        if (matchNext("begin", input)) {
            String codeBuff = new CorrectWrapperParser("begin", "end;").parse(input);
            trueCode.append(codeBuff);
            input = input.substring(codeBuff.length());
        } else {
            while (input.charAt(0) != ';') {
                trueCode.append(input.substring(0, 1));
                input = input.substring(1);
            }
            trueCode.append(input.substring(0, 1));
            input = input.substring(1);
        }
        input = readSpace(input);
        StringBuilder falseCode = new StringBuilder();
        if (matchNext("else", input)) {
            input = input.substring(4);
            input = readSpace(input);
            if (matchNext("begin", input)) {
                String codeBuff = new CorrectWrapperParser("begin", "end;").parse(input);
                falseCode.append(codeBuff);
                input = input.substring(codeBuff.length());
            } else {
                while (input.charAt(0) != ';') {
                    falseCode.append(input.substring(0, 1));
                    input = input.substring(1);
                }
                falseCode.append(input.substring(0, 1));
                input = input.substring(1);
            }
        }
        String conditionF = condition;
        String trueCodeF = trueCode.toString();
        String falseCodeF = falseCode.toString();
        HashMap<String, String> value = new HashMap<String, String>() {
            {
                put("condition", conditionF);
                put("trueCode", trueCodeF);
                if (falseCode.length() > 0) {
                    put("falseCode", falseCodeF);
                }
            }
        };
        return new Pair<>(new ComplexToken(this.getType(), value), input);
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        ComplexToken complexToken = (ComplexToken)token;
        String condition = complexToken.getValueBuffer("condition");
        String trueCode = complexToken.getValueBuffer("trueCode");
        String falseCode = complexToken.getValueBuffer("falseCode");
        if (falseCode == null) {
            return String.format("if %s then\n%s", condition, trueCode);
        } else {
            return String.format("if %s then\n%s\nelse\n%s", condition, trueCode, falseCode);
        }
    }
}
