package languages.pascal;


import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import utils.CorrectWrapperParser;
import utils.Pair;

import java.util.HashMap;


public class PascalWhileConverter extends RegexConverter {
    public PascalWhileConverter() {
        super("while", "while_cycle");
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (!matchNext(input)) {
            return null;
        }
        input = input.substring(5);
        input = readSpace(input);
        String condition = "";
        while (!matchNext("do", input)) {
            condition += input.substring(0, 1);
            input = input.substring(1);
            if (input.length() == 0) {
                return null;
            }
        }
        input = input.substring(2);
        input = readSpace(input);
        StringBuilder code = new StringBuilder();
        if (matchNext("begin", input)) {
            String codeBuff = new CorrectWrapperParser("begin", "end;").parse(input);
            code.append(codeBuff);
            input = input.substring(codeBuff.length());
        } else {
            while (input.charAt(0) != ';') {
                code.append(input.substring(0, 1));
                input = input.substring(1);
            }
            code.append(input.substring(0, 1));
            input = input.substring(1);
        }
        String conditionF = condition;
        String codeF = code.toString();
        HashMap<String, String> value = new HashMap<String, String>() {
            { put("condition", conditionF); put("code", codeF); }
        };
        return new Pair<>(new ComplexToken(this.getType(), value), input);
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        ComplexToken complexToken = (ComplexToken)token;
        String condition = complexToken.getValueBuffer("condition");
        String code = complexToken.getValueBuffer("code");
        return String.format("while %s do\n%s", condition, code);
    }
}