package languages.pascal;


import lexer.AbstractSyntax;
import lexer.IReader;
import lexer.converters.RegexConverter;
import lexer.converters.TextConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;
import utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;


public class PascalVarConverter extends RegexConverter {
    private ArrayList<IReader> readers;
    private HashMap<String, String> typeTable;
    private HashMap<String, String> reverseTypeTable;

    public PascalVarConverter(AbstractSyntax syntax) {
        super("var", "variable_definition");
        this.readers = syntax.getReaders();
    }

    private void buildTypeTable() {
        this.typeTable = new HashMap<>();
        this.reverseTypeTable = new HashMap<>();
        for (IReader reader : this.readers) {
            String type = reader.getType();
            if (type.startsWith("type")) {
                TextConverter converter = (TextConverter)reader;
                this.typeTable.put(converter.getText(), type);
                this.reverseTypeTable.put(type, converter.getText());
            }
        }
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (this.typeTable == null) {
            buildTypeTable();
        }
        String identifier = "[a-zA-Z_][\\w_]*";
        if (!matchNext(input))
            return null;
        input = input.substring(3);
        input = readSpace(input);
        HashMap<String, String> value = new HashMap<>();
        ArrayList<String> identifiers = new ArrayList<>();
        boolean first = true;
        while (true) {
            HashMap<String, String> valueBuffer = new HashMap<>();
            String startInput = input;
            if (matchNext(identifier, input)) {
                identifiers.add(input.substring(0, getLastLength()));
                input = input.substring(getLastLength());
                input = readSpace(input);
            } else {
                return null;
            }
            while (matchNext(",", input)) {
                input = input.substring(1);
                input = readSpace(input);
                if (matchNext(identifier, input)) {
                    identifiers.add(input.substring(0, getLastLength()));
                    input = input.substring(getLastLength());
                } else {
                    if (!first) {
                        input = startInput;
                        break ;
                    }
                    return null;
                }
                input = readSpace(input);
            }
            if (matchNext(":", input)) {
                input = input.substring(1);
                input = readSpace(input);
                if (matchNext("[^\\s;]*", input)) {
                    String type = input.substring(0, getLastLength());
                    input = input.substring(getLastLength());
                    for (String ident : identifiers) {
                        valueBuffer.put(ident, this.typeTable.get(type));
                    }
                    identifiers.clear();
                }
                input = readSpace(input);
                if (!matchNext(";", input)) {
                    if (!first) {
                        input = startInput;
                        break;
                    }
                    return null;
                }
                input = input.substring(1);
                input = readSpace(input);
                value.putAll(valueBuffer);
                valueBuffer.clear();
            } else if (!first) {
                input = startInput;
                break;
            }
            first = false;
        }
        return new Pair<>(new Token("variable_definition", value), input);
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        if (this.reverseTypeTable == null) {
            buildTypeTable();
        }
        Token tokenT = (Token)token;
        StringBuilder result = new StringBuilder();
        result.append("var ");
        for (String key : tokenT.getValue().keySet()) {
            result.append(key);
            result.append(": ");
            result.append(this.reverseTypeTable.get(tokenT.getValue().get(key)));
            result.append("; ");
        }
        result.append("\n");
        return result.toString();
    }
}
