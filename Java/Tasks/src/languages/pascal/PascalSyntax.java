package languages.pascal;


import lexer.AbstractSyntax;
import lexer.IStaticTyped;
import lexer.converters.TextConverter;
import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;

import java.util.ArrayList;
import java.util.HashMap;


public class PascalSyntax extends AbstractSyntax implements IStaticTyped{
    public PascalSyntax() {
        addToken(new RegexConverter("\\s", "space"));
        addToken(new PascalLineCommentConverter());
        addToken(new PascalMultilineCommentConverter());

        addToken(new PascalVarConverter(this));
        addToken(new PascalWhileConverter());
        addToken(new PascalIfConverter());
        addToken(new TextConverter("begin", "subprogram_begin"));
        addToken(new TextConverter("end;", "subprogram_end"));
        addToken(new TextConverter("(", "arguments_begin"));
        addToken(new TextConverter(")", "arguments_end"));
        addToken(new TextConverter(";", "line_end"));
        addToken(new TextConverter(",", "comma"));

        addToken(new TextConverter(":=", "operator_assignment"));
        addToken(new TextConverter("=", "operator_eq"));
        addToken(new TextConverter(">", "operator_gt"));
        addToken(new TextConverter("<", "operator_lt"));
        addToken(new TextConverter(">=", "operator_ge"));
        addToken(new TextConverter("<=", "operator_le"));
        addToken(new TextConverter("<>", "operator_ne"));
        addToken(new TextConverter("not", "operator_not"));
        addToken(new TextConverter("and", "operator_and"));
        addToken(new TextConverter("or", "operator_or"));
        addToken(new TextConverter("+", "operator_sum"));
        addToken(new TextConverter("-", "operator_difference"));
        addToken(new TextConverter("*", "operator_multiply"));
        addToken(new TextConverter("/", "operator_divide"));

        addToken(new TextConverter("integer", "type_integer"));
        addToken(new TextConverter("double", "type_double"));
        addToken(new TextConverter("boolean", "type_boolean"));
        addToken(new TextConverter("char", "type_char"));
        addToken(new TextConverter("string", "type_string"));

        addToken(new RegexConverter("\\d+", "constant_integer"));
        addToken(new RegexConverter("\\d+\\.\\d+", "constant_double"));
        addToken(new PascalStringConverter());
        addToken(new RegexConverter("(true)|(false)", "constant_boolean"));

        addToken(new RegexConverter("[a-zA-Z_][\\w_]*", "identifier"));
    }

    @Override
    public String getName() {
        return "Pascal";
    }

    @Override
    public boolean isCaseSensitive() {
        return false;
    }

    @Override
    public HashMap<String, String> getTypeModel(ArrayList<AbstractToken> tokens) {
        for (int i = 0; i < tokens.size(); i++) {
            if (tokens.get(i).getType().equals("variable_definition")) {
                Token tokenT = (Token)tokens.get(i);
                tokens.remove(i);
                return tokenT.getValue();
            }
        }
        return new HashMap<>();
    }

    @Override
    public void applyTypeModel(ArrayList<AbstractToken> tokens, HashMap<String, String> typeModel) {
        tokens.add(0, new Token("variable_definition", typeModel));
    }
}
