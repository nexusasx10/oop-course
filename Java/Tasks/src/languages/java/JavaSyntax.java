package languages.java;


import lexer.AbstractSyntax;
import lexer.IReader;
import lexer.IStaticTyped;
import lexer.converters.TextConverter;
import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;

import java.util.ArrayList;
import java.util.HashMap;


public class JavaSyntax extends AbstractSyntax implements IStaticTyped{
    public JavaSyntax() {
        addToken(new RegexConverter("\\s", "space"));
        addToken(new JavaLineCommentConverter());
        addToken(new JavaMultilineCommentConverter());

        addToken(new JavaWhileConverter());
        addToken(new JavaIfConverter());
        addToken(new TextConverter("{", "subprogram_begin"));
        addToken(new TextConverter("}", "subprogram_end"));
        addToken(new TextConverter("(", "arguments_begin"));
        addToken(new TextConverter(")", "arguments_end"));
        addToken(new TextConverter(";", "line_end"));
        addToken(new TextConverter(",", "comma"));

        addToken(new TextConverter("=", "operator_assignment"));
        addToken(new TextConverter("==", "operator_eq"));
        addToken(new TextConverter(">", "operator_gt"));
        addToken(new TextConverter("<", "operator_lt"));
        addToken(new TextConverter(">=", "operator_ge"));
        addToken(new TextConverter("<=", "operator_le"));
        addToken(new TextConverter("!=", "operator_ne"));
        addToken(new TextConverter("!", "operator_not"));
        addToken(new TextConverter("&&", "operator_and"));
        addToken(new TextConverter("||", "operator_or"));
        addToken(new TextConverter("+", "operator_sum"));
        addToken(new TextConverter("-", "operator_difference"));
        addToken(new TextConverter("*", "operator_multiply"));
        addToken(new TextConverter("/", "operator_divide"));

        addToken(new TextConverter("int", "type_integer"));
        addToken(new TextConverter("double", "type_double"));
        addToken(new TextConverter("boolean", "type_boolean"));
        addToken(new TextConverter("char", "type_char"));
        addToken(new TextConverter("String", "type_string"));

        addToken(new RegexConverter("\\d+", "constant_integer"));
        addToken(new RegexConverter("\\d+\\.\\d+", "constant_double"));
        addToken(new JavaStringConverter());
        addToken(new RegexConverter("(true)|(false)", "constant_boolean"));

        addToken(new RegexConverter("[a-zA-Z_][\\w_]*", "identifier"));
    }

    @Override
    public String getName() {
        return "Java";
    }

    @Override
    public boolean isCaseSensitive() {
        return true;
    }

    @Override
    public HashMap<String, String> getTypeModel(ArrayList<AbstractToken> tokens) {
        HashMap<String, String> result = new HashMap<>();
        int prevIndex = 0;
        for (int i = 0; i < tokens.size(); i++) {
            AbstractToken token = tokens.get(i);
            AbstractToken previous = tokens.get(prevIndex);
            if (prevIndex > -1 && previous.getType().startsWith("type") && token.getType().equals("identifier")) {
                Token tokenT = (Token)token;
                result.put(tokenT.getValue("value"), previous.getType());
                tokens.remove(prevIndex);
                i--;
            }
            if (!token.getType().equals("space")) {
                prevIndex = i;
            }
        }
        return result;
    }

    @Override
    public void applyTypeModel(ArrayList<AbstractToken> tokens, HashMap<String, String> typeModel) {
        for (String key : typeModel.keySet()) {
            tokens.add(0, new Token(typeModel.get(key), ""));
            tokens.add(1, new Token("space", " "));
            tokens.add(2, new Token("identifier", key));
            tokens.add(3, new Token("line_end", ";"));
            tokens.add(4, new Token("space", "\n"));
        }
    }
}