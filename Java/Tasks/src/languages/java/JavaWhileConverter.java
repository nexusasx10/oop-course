package languages.java;


import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import utils.CorrectWrapperParser;
import utils.Pair;

import java.util.HashMap;


public class JavaWhileConverter extends RegexConverter {
    public JavaWhileConverter() {
        super("while", "while_cycle");
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (!matchNext(input)) {
            return null;
        }
        input = input.substring(5);
        input = readSpace(input);
        String condition = new CorrectWrapperParser("(", ")").parse(input);
        input = input.substring(condition.length());
        condition = condition.substring(1, condition.length() - 1);
        input = readSpace(input);
        StringBuilder code = new StringBuilder();
        if (input.charAt(0) == '{') {
            code.append(new CorrectWrapperParser("{", "}").parse(input));
            input = input.substring(code.length());
        } else {
            while (input.charAt(0) != ';') {
                code.append(input.substring(0, 1));
                input = input.substring(1);
            }
            code.append(input.substring(0, 1));
            input = input.substring(1);
        }
        String conditionF = condition;
        String codeF = code.toString();
        HashMap<String, String> value = new HashMap<String, String>() {
            { put("condition", conditionF); put("code", codeF); }
        };
        return new Pair<>(new ComplexToken(this.getType(), value), input);
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        ComplexToken complexToken = (ComplexToken)token;
        String condition = complexToken.getValueBuffer("condition");
        String code = complexToken.getValueBuffer("code");
        return String.format("while (%s)\n%s", condition, code);
    }
}