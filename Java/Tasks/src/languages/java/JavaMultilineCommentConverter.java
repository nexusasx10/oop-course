package languages.java;


import lexer.converters.RegexConverter;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;
import utils.Pair;


public class JavaMultilineCommentConverter extends RegexConverter {
    public JavaMultilineCommentConverter() {
        super("/\\*.*?\\*/", "multiline_comment");
    }

    @Override
    public Pair<AbstractToken, String> tryReadToken(String input) {
        if (matchNext(input)) {
            return new Pair<>(
                new Token(this.getType(), input.substring(2, getLastLength() - 2)),
                input.substring(getLastLength())
            );
        }
        return null;
    }

    @Override
    public String tryBuildCode(AbstractToken token) {
        Token tToken = (Token)token;
        return String.format("*/%s/*", tToken.getValue().get("value"));
    }
}