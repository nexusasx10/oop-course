package tests;


import calculator.Calculator;
import calculator.IncorrectExpressionException;

import java.util.Scanner;


public class CalculatorTests {
    public static void main(String[] args) {
        calculatorTest("i * i");
        calculatorTest("2.1 + 10i");
        calculatorTest("2.1 * 10i");
        calculatorTest("4 - 5");
        calculatorTest("10.2 / 2");
        calculatorTest("2 + 2 * 2");
        calculatorTest("(2 + 2) * 2");
        calculatorTest("3 + <12> - 4 + <3, 4.6> + <12 * 2> + <3, 1> * 2 + <1, 2, 3 + 1>+1-1+1-1+1-1+1-1+1-1+1/2-1/2+2*(3-3+i/i-i/i)");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter expression:");
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("")) {
                break;
            } else {
                calculatorTest(input, false);
            }
        }
    }

    private static void calculatorTest(String input) {
        calculatorTest(input, true);
    }

    private static void calculatorTest(String input, boolean printExpression) {
        Calculator calculator = new Calculator();
        try {
            String result = calculator.calculate(input);
            if (printExpression) {
                System.out.println(String.format("%s = %s", input, result));
            } else {
                System.out.println(result);
            }
        } catch (IncorrectExpressionException exception) {
            if (exception.getMessage() != null) {
                System.err.println(String.format("Incorrect expression: %s", exception.getMessage()));
            } else {
                System.err.println("Incorrect expression");
            }
        }
    }
}