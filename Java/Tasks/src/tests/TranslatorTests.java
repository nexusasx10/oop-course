package tests;


import languages.java.JavaSyntax;
import languages.pascal.PascalSyntax;
import translator.Translator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class TranslatorTests {
    public static void main(String[] args) {
        File file = new File("src/tests/javaCode.txt");
        Scanner scanner;
        StringBuilder input = new StringBuilder();
        try{
            scanner = new Scanner(file);
        } catch (FileNotFoundException exception) {
            System.err.println("File not found");
            return;
        }
        while (scanner.hasNextLine())  {
            input.append(scanner.nextLine());
            input.append("\n");
        }
        translatorTest(input.toString());
    }

    private static void translatorTest(String input) {
        Translator translator = new Translator();
        System.out.println(input);
        System.out.println(input = translator.translate(new JavaSyntax(), new PascalSyntax(), input));
        System.out.println(translator.translate(new PascalSyntax(), new JavaSyntax(), input));
    }
}