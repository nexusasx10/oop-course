int i = 0;
int j = 67.6;
String str = "some text printing while (true)";
boolean flag = true;
while((true || true) && (true || false)) {
    if (true && true) {
        do_something();
    } else {
        if (go(1)) do(1);
    }
    // print(1); This code not execute
    print(str);
    /*
       and this too
       print(2);
       print(3);
    */
    while (someFunction(flag, true)) {
        do_something();
    }
}