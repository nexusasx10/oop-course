package calculator.operators.binary;


import calculator.CalculationToken;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.tokens.Token;

import java.util.ArrayList;
import java.util.HashMap;


public class MulOperator extends BinaryOperator {
    public AbstractToken apply() {
        if (checkType("number", "number", false)) {
            Token first = (Token)this.first;
            Token second = (Token)this.second;
            HashMap<String, String> value = new HashMap<>();
            value.put("real", DiffOperator.diff(mul(first.getValue("real"), second.getValue("real")), mul(first.getValue("imag"), second.getValue("imag"))));
            value.put("imag", SumOperator.sum(mul(first.getValue("real"), second.getValue("imag")), mul(second.getValue("real"), first.getValue("imag"))));
            return new Token("number", value);
        } else if (checkType("vector", "number", true)) {
            ComplexToken first = (ComplexToken)this.first;
            Token second = (Token)this.second;
            HashMap<String, ArrayList<AbstractToken>> value = new HashMap<>();
            for (Integer i = 0; i < first.getValue().size(); i++) {
                ArrayList<AbstractToken> component = new ArrayList<>();
                component.addAll(first.getValue(i.toString()));
                component.add(second);
                component.add(new Token("operator_mul", "*"));
                value.put(i.toString(), component);
            }
            ComplexToken result = new ComplexToken("vector", null);
            result.process(value);
            return result;
        } else if (checkType("incorrect", "number", true)) {
            CalculationToken first = (CalculationToken)this.first;
            Token second = (Token)this.second;
            HashMap<String, AbstractToken> result = new HashMap<>();
            for (String key : first.getNValue().keySet()) {
                result.put(key, apply(first.getNValue(key), second));
            }
            return new CalculationToken("incorrect", result);
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return "operator_mul";
    }

    public static String mul(String firstOperand, String secondOperand) {
        double first = Double.parseDouble(firstOperand);
        double second = Double.parseDouble(secondOperand);
        return Double.toString(first * second);
    }
}