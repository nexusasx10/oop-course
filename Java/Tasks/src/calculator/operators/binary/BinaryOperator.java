package calculator.operators.binary;

import calculator.operators.IOperator;
import lexer.tokens.AbstractToken;


public abstract class BinaryOperator implements IOperator {
    protected AbstractToken first;
    protected AbstractToken second;

    public AbstractToken apply(AbstractToken second, AbstractToken first) {
        this.first = first;
        this.second = second;
        return this.apply();
    }

    public abstract AbstractToken apply();

    public boolean checkType(String firstType, String secondType, boolean canChange) {
        if (canChange && this.second.getType().equals(firstType) && this.first.getType().equals(secondType)) {
            AbstractToken buffer = this.first;
            this.first = this.second;
            this.second = buffer;
            return true;
        }
        return this.first.getType().equals(firstType) && this.second.getType().equals(secondType);
    }
}