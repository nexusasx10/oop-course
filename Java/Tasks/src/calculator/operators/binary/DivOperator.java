package calculator.operators.binary;


import calculator.IncorrectExpressionException;
import lexer.tokens.AbstractToken;
import lexer.tokens.Token;

import java.util.HashMap;


public class DivOperator extends BinaryOperator {
    public AbstractToken apply() {
        if (checkType("number", "number", false)) {
            Token first = (Token)this.first;
            Token second = (Token)this.second;
            HashMap<String, String> resultValue = new HashMap<>();
            String rNumerator = SumOperator.sum(
                    MulOperator.mul(first.getValue("real"), second.getValue("real")),
                    MulOperator.mul(first.getValue("imag"), second.getValue("imag"))
            );
            String iNumerator = DiffOperator.diff(
                    MulOperator.mul(second.getValue("real"), first.getValue("imag")),
                    MulOperator.mul(first.getValue("real"), second.getValue("imag"))
            );
            String denominator = SumOperator.sum(
                    MulOperator.mul(second.getValue("real"), second.getValue("real")),
                    MulOperator.mul(second.getValue("imag"), second.getValue("imag"))
            );
            resultValue.put("real", div(rNumerator, denominator));
            resultValue.put("imag", div(iNumerator, denominator));
            return new Token("number", resultValue);
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return "operator_div";
    }

    public static String div(String firstOperand, String secondOperand) {
        double first = Double.parseDouble(firstOperand);
        double second = Double.parseDouble(secondOperand);
        if (second == 0) {
            throw new IncorrectExpressionException("Division by zero");
        }
        return Double.toString(first / second);
    }
}