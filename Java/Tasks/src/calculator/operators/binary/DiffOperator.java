package calculator.operators.binary;


import calculator.CalculationToken;
import calculator.operators.unary.MinusOperator;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.tokens.Token;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class DiffOperator extends BinaryOperator {
    public AbstractToken apply() {
        if (checkType("number", "number", false)) {
            Token first = (Token)this.first;
            Token second = (Token)this.second;
            HashMap<String, String> resultValue = new HashMap<>();
            resultValue.put("real", diff(first.getValue("real"), second.getValue("real")));
            resultValue.put("imag", diff(first.getValue("imag"), second.getValue("imag")));
            return new Token("number", resultValue);
        } else if (checkType("vector", "vector", false)) {
            ComplexToken first = (ComplexToken)this.first;
            ComplexToken second = (ComplexToken)this.second;
            HashMap<String, ArrayList<AbstractToken>> resultValue = new HashMap<>();
            if (first.getValue().size() != second.getValue().size()) {
                second = (ComplexToken) new MinusOperator().apply(second);
                HashMap<String, AbstractToken> resultValueAlt = new HashMap<>();
                resultValueAlt.put(String.format("vector%s", first.getValue().size()), first);
                resultValueAlt.put(String.format("vector%s", second.getValue().size()), second);
                return new CalculationToken("incorrect", resultValueAlt);
            } else {
                for (Integer i = 0; i < first.getValue().size(); i++) {
                    ArrayList<AbstractToken> component = new ArrayList<>();
                    component.addAll(first.getValue(i.toString()));
                    component.addAll(second.getValue(i.toString()));
                    component.add(new Token("operator_diff", "-"));
                    resultValue.put(i.toString(), component);
                }
                ComplexToken result = new ComplexToken("vector", null);
                result.process(resultValue);
                return result;
            }
        } else if (checkType("vector", "number", false)) {
            ComplexToken first = (ComplexToken)this.first;
            Token second = (Token)new MinusOperator().apply(this.second);
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            resultValue.put(String.format("vector%s", first.getValue().size()), first);
            resultValue.put("number", second);
            return new CalculationToken("incorrect", resultValue);
        } else if (checkType("number", "vector", false)) {
            Token first = (Token)this.first;
            ComplexToken second = (ComplexToken)new MinusOperator().apply(this.second);
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            resultValue.put(String.format("vector%s", second.getValue().size()), second);
            resultValue.put("number", first);
            return new CalculationToken("incorrect", resultValue);
        } else if (checkType("incorrect", "number", false)) {
            CalculationToken first = (CalculationToken) this.first;
            Token second = (Token)this.second;
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            resultValue.putAll(first.getNValue());
            if (resultValue.containsKey("number")) {
                resultValue.put("number", apply(second, first.getNValue("number")));
            } else {
                resultValue.put("number", second);
            }
            return new CalculationToken("incorrect", resultValue);
        } else if (checkType("number", "incorrect", false)) {
            Token first = (Token) this.first;
            CalculationToken second = (CalculationToken)new MinusOperator().apply(this.second);
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            resultValue.putAll(second.getNValue());
            resultValue.put("number", apply(first, second.getNValue("number")));
            return new CalculationToken("incorrect", resultValue);
        } else if (checkType("incorrect", "vector", false)) {
            CalculationToken first = (CalculationToken) this.first;
            ComplexToken second = (ComplexToken) new MinusOperator().apply(this.second);
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            resultValue.putAll(first.getNValue());
            if (first.getNValue().containsKey(String.format("vector%s", second.getValue().size()))) {
                resultValue.put(String.format("vector%s", second.getValue().size()), apply(first.getNValue(String.format("vector%s", second.getValue().size())), second));
            } else {
                resultValue.put(String.format("vector%s", second.getValue().size()), second);
            }
            return new CalculationToken("incorrect", resultValue);
        } else if (checkType("vector", "incorrect", false)) {
            ComplexToken first = (ComplexToken) this.first;
            CalculationToken second = (CalculationToken) new MinusOperator().apply(this.second);
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            resultValue.putAll(second.getNValue());
            if (second.getNValue().containsKey(String.format("vector%s", first.getValue().size()))) {
                resultValue.put(String.format("vector%s", first.getValue().size()), apply(first, second.getNValue(String.format("vector%s", first.getValue().size()))));
            } else {
                resultValue.put(String.format("vector%s", first.getValue().size()), first);
            }
            return new CalculationToken("incorrect", resultValue);
        } else if (checkType("incorrect", "incorrect", false)) {
            CalculationToken first = (CalculationToken) this.first;
            CalculationToken second = (CalculationToken) new MinusOperator().apply(this.second);
            HashMap<String, AbstractToken> resultValue = new HashMap<>();
            HashSet<String> keys = new HashSet<>();
            keys.addAll(first.getNValue().keySet());
            keys.addAll(second.getNValue().keySet());
            for (String key : keys) {
                if (first.getNValue().containsKey(key)) {
                    if (second.getNValue().containsKey(key)) {
                        resultValue.put(key, apply(first.getNValue(key), second.getNValue(key)));
                    } else {
                        resultValue.put(key, first.getNValue(key));
                    }
                } else {
                    resultValue.put(key, second.getNValue(key));
                }
            }
            return new CalculationToken("incorrect", resultValue);
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return "operator_diff";
    }

    public static String diff(String firstOperand, String secondOperand) {
        double first = Double.parseDouble(firstOperand);
        double second = Double.parseDouble(secondOperand);
        return Double.toString(first - second);
    }
}