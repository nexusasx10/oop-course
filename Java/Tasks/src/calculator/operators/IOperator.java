package calculator.operators;


public interface IOperator {
    String getType();
}