package calculator.operators.unary;


import lexer.tokens.AbstractToken;

public class PlusOperator extends UnaryOperator {
    public AbstractToken apply() {
        return this.first;
    }

    @Override
    public String getType() {
        return "operator_unary_plus";
    }
}