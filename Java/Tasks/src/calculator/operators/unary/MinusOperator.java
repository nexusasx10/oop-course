package calculator.operators.unary;


import calculator.operators.binary.MulOperator;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.tokens.Token;

import java.util.HashMap;


public class MinusOperator extends UnaryOperator {
    public AbstractToken apply() {
        return new MulOperator().apply(
            this.first, new Token("number", new HashMap<String, String>() {{put("real", "-1"); put("imag", "0");}})
        );
    }

    @Override
    public String getType() {
        return "operator_unary_minus";
    }
}