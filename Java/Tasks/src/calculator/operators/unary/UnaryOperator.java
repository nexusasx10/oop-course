package calculator.operators.unary;


import calculator.operators.IOperator;
import lexer.tokens.AbstractToken;


public abstract class UnaryOperator implements IOperator {
    protected AbstractToken first;

    public AbstractToken apply(AbstractToken first) {
        this.first = first;
        try {
            return this.apply();
        } finally {
            this.first = null;
        }
    }

    public abstract AbstractToken apply();

    public boolean checkType(String firstType) {
        return this.first.getType().equals(firstType);
    }
}