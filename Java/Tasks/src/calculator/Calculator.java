package calculator;


import languages.math.MathSyntax;
import lexer.Lexer;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.tokens.Token;
import translator.Translator;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;


public class Calculator {
    public String calculate(String input){
        Lexer lexer = new Lexer();
        lexer.register(new MathSyntax());
        input = input.replaceAll("\\s", "");
        try {
            ArrayList<AbstractToken> tokens = lexer.getTokens("Math", input);
            preProcess(tokens);
            ReversePolishNotation rpn = new ReversePolishNotation();
            StackMachine stackMachine= new StackMachine();
            tokens = rpn.getRPN(tokens);
            for (AbstractToken token : tokens) {
                HashMap<String, ArrayList<AbstractToken>> value = new HashMap<>();
                if (token instanceof ComplexToken) {
                    ComplexToken tokenC = (ComplexToken)token;
                    for (String key : tokenC.getValue().keySet()) {
                        ArrayList<AbstractToken> inner = rpn.getRPN(tokenC.getValue().get(key));
                        value.put(key, inner);
                    }
                    tokenC.process(value);
                }
            }
            tokens = stackMachine.calculate(tokens);
            for (AbstractToken token : tokens) {
                if (token instanceof ComplexToken) {
                    HashMap<String, ArrayList<AbstractToken>> value = new HashMap<>();
                    ComplexToken tokenC = (ComplexToken)token;
                    for (String key : tokenC.getValue().keySet()) {
                        ArrayList<AbstractToken> inner = stackMachine.calculate(tokenC.getValue().get(key));
                        for (AbstractToken innerToken : inner) {
                            if (innerToken instanceof ComplexToken) {
                                HashMap<String, ArrayList<AbstractToken>> innerValue = new HashMap<>();
                                ComplexToken innerTokenC = (ComplexToken)innerToken;
                                for (String iKey : innerTokenC.getValue().keySet()) {
                                    ArrayList<AbstractToken> innerInner = stackMachine.calculate(innerTokenC.getValue().get(iKey));
                                    innerValue.put(iKey, innerInner);
                                }
                                innerTokenC.process(innerValue);
                            }
                        }
                        value.put(key, inner);
                    }
                    tokenC.process(value);
                }
            }
            Translator translator = new Translator();
            return translator.buildCode(tokens, new MathSyntax());
        } catch (NullPointerException | StringIndexOutOfBoundsException | EmptyStackException exception) {
            throw new IncorrectExpressionException();
        }
    }

    private void preProcess(ArrayList<AbstractToken> tokens) {
        boolean nextUnary = true;
        for (int i = 0; i < tokens.size(); i++) {
            AbstractToken token = tokens.get(i);
            if (nextUnary) {
                if (token.getType().equals("operator_sum")) {
                    tokens.set(i, new Token("operator_unary_plus", "+"));
                } else if (token.getType().equals("operator_diff")) {
                    tokens.set(i, new Token("operator_unary_minus", "-"));
                }
            }
            nextUnary = token.getType().equals("open_bracket");
            if (token instanceof ComplexToken) {
                ComplexToken tokenC = (ComplexToken)token;
                for (String key : tokenC.getValue().keySet()) {
                    preProcess(tokenC.getValue().get(key));
                }
            }
        }
    }
}