package calculator;


import lexer.tokens.AbstractToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;


public class ReversePolishNotation {
    private final HashMap<String, Integer> priority;

    public ReversePolishNotation() {
        this.priority = new HashMap<String, Integer>() {
            {
                put("operator_sum", 0);
                put("operator_diff", 0);
                put("operator_mul", 1);
                put("operator_div", 1);
                put("operator_unary_plus", 2);
                put("operator_unary_minus", 2);
            }
        };
    }

    public ArrayList<AbstractToken> getRPN(ArrayList<AbstractToken> expression) {
        Stack<AbstractToken> stack = new Stack<>();
        ArrayList<AbstractToken> result = new ArrayList<>();
        for (AbstractToken token : expression) {
            if (token.getType().startsWith("operator")) {
                while (stack.size() > 0 && stack.peek().getType().startsWith("operator") && priority.get(stack.peek().getType()) >= priority.get(token.getType())) {
                    result.add(stack.pop());
                }
                stack.push(token);
            } else if (token.getType().equals("open_bracket")) {
                stack.push(token);
            } else if (token.getType().equals("close_bracket")) {
                while (!stack.peek().getType().equals("open_bracket")) {
                    result.add(stack.pop());
                }
                stack.pop();
            }
            else {
                result.add(token);
            }
        }
        while (stack.size() > 0) {
            result.add(stack.pop());
        }
        return result;
    }
}