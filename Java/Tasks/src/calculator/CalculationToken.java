package calculator;


import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;

import java.util.ArrayList;
import java.util.HashMap;


public class CalculationToken extends ComplexToken {
    private HashMap<String, AbstractToken> newValue;

    public CalculationToken(String type, HashMap<String, AbstractToken> value) {
        super(type, null);
        HashMap<String, ArrayList<AbstractToken>> NValue = new HashMap<>();
        for (String key : value.keySet()) {
            NValue.put(key, new ArrayList<AbstractToken>() {{add(value.get(key));}});
        }
        this.process(NValue);
        this.newValue = value;
    }

    public HashMap<String, AbstractToken> getNValue() {
        return this.newValue;
    }

    public AbstractToken getNValue(String key) {
        if (this.isProcessed()) {
            return this.newValue.get(key);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("<%s(", this.getType()));
        boolean first = true;
        for (String key : this.getNValue().keySet()) {
            if (!first) {
                result.append(", ");
            } else {
                first = false;
            }
            result.append(String.format("%s: %s", key, this.getNValue().get(key)));
        }
        result.append(")>");
        return result.toString();
    }
}