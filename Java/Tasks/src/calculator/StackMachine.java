package calculator;


import calculator.operators.IOperator;
import calculator.operators.binary.*;
import calculator.operators.unary.*;
import lexer.tokens.AbstractToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;


public class StackMachine {
    private HashMap<String, IOperator> operators;

    private void addOperator(IOperator operator) {
        this.operators.put(operator.getType(), operator);
    }

    public StackMachine() {
        this.operators = new HashMap<>();
        addOperator(new SumOperator());
        addOperator(new DiffOperator());
        addOperator(new MulOperator());
        addOperator(new DivOperator());

        addOperator(new PlusOperator());
        addOperator(new MinusOperator());
    }

    public ArrayList<AbstractToken> calculate(ArrayList<AbstractToken> tokens) {
        Stack<AbstractToken> stack = new Stack<>();
        ArrayList<AbstractToken> result = new ArrayList<>();
        if (tokens.size() == 1 && tokens.get(0).getType().startsWith("operator")) {
            return tokens;
        }
        for (AbstractToken token : tokens) {
            if (token.getType().equals("number") || token.getType().equals("vector")) {
                stack.push(token);
            } else {
                IOperator operator = this.operators.get(token.getType());
                if (operator instanceof BinaryOperator) {
                    stack.push(((BinaryOperator)operator).apply(stack.pop(), stack.pop()));
                } else if (operator instanceof UnaryOperator) {
                    stack.push(((UnaryOperator)operator).apply(stack.pop()));
                }
            }
        }
        result.add(stack.pop());
        return result;
    }
}