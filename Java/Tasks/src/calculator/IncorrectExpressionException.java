package calculator;


public class IncorrectExpressionException extends RuntimeException {
    public IncorrectExpressionException(String message) {
        super(message);
    }

    public IncorrectExpressionException() {}
}