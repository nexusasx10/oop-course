package utils;


public class Pair<T1, T2> {
    private final T1 first;
    private final T2 second;

    public Pair(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }

    public T1 getFirst() {
        return this.first;
    }

    public T2 getSecond() {
        return this.second;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        return this.first.equals(((Pair) obj).first) && this.second.equals(((Pair) obj).second);
    }

    @Override
    public int hashCode() {
        return (this.first.hashCode() * 13) ^ this.second.hashCode();
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", this.first, this.second);
    }
}