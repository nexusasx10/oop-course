package utils;


import java.util.Stack;


public class CorrectWrapperParser {
    private String open;
    private String close;

    public CorrectWrapperParser(String open, String close) {
        this.open = open;
        this.close = close;
    }

    public String parse(String input) {
        String start = input;
        Stack<String> stack = new Stack<>();
        if (!input.startsWith(open)) {
            return null;
        }
        stack.push(input.substring(0, open.length()));
        input = input.substring(open.length());
        while (stack.size() > 0 && input.length() > 0) {
            if (input.startsWith(open)) {
                stack.push(input.substring(0, open.length()));
                input = input.substring(open.length());
            } else if (input.startsWith(close)) {
                stack.pop();
                input = input.substring(close.length());
            } else if (input.length() > 0) {
                input = input.substring(1);
            } else if (stack.size() > 0) {
                return null;
            }
        }
        return start.substring(0, start.length() - input.length());
    }
}
