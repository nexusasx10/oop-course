package translator;


import lexer.IStaticTyped;
import lexer.tokens.AbstractToken;
import lexer.tokens.ComplexToken;
import lexer.Lexer;
import lexer.AbstractSyntax;
import lexer.IBuilder;
import lexer.tokens.Token;

import java.util.ArrayList;
import java.util.HashMap;


public class Translator {
    public String translate(AbstractSyntax inputLanguage, AbstractSyntax outputLanguage, String input) {
        Lexer lexer = new Lexer();
        lexer.register(inputLanguage);
        lexer.register(outputLanguage);
        ArrayList<AbstractToken> inputTokens = lexer.getTokens(inputLanguage.getName(), input);
        if (inputLanguage.isStaticTyped() && outputLanguage.isStaticTyped()) {
            IStaticTyped inputLanguageST = (IStaticTyped)inputLanguage;
            IStaticTyped outputLanguageST = (IStaticTyped)outputLanguage;
            outputLanguageST.applyTypeModel(inputTokens, inputLanguageST.getTypeModel(inputTokens));
        }
        return buildCode(inputTokens, outputLanguage);
    }

    public String buildCode(ArrayList<AbstractToken> tokens, AbstractSyntax outputLanguage) {
        ArrayList<ComplexToken> toDeProcess = new ArrayList<>();
        for (AbstractToken token : tokens) {
            if (token instanceof ComplexToken) {
                toDeProcess.add((ComplexToken)token);
            }
        }
        for (int i = 0; i < toDeProcess.size(); i ++) {
            ComplexToken token = toDeProcess.get(i);
            for (String key : token.getValue().keySet()) {
                for (AbstractToken inToken : token.getValue().get(key)) {
                    if (inToken instanceof ComplexToken) {
                        toDeProcess.add((ComplexToken)inToken);
                    }
                }
            }
        }
        for (int i = toDeProcess.size() - 1; i >= 0 ; i--) {
            ComplexToken token = toDeProcess.get(i);
            HashMap<String, String> value = new HashMap<>();
            for (String key : token.getValue().keySet()) {
                String inner = build(token.getValue().get(key), outputLanguage);
                value.put(key, inner);
            }
            token.deProcess(value);
        }
        return build(tokens, outputLanguage);
    }

    private String build(ArrayList<AbstractToken> tokens, AbstractSyntax outputLanguage) {
        StringBuilder result = new StringBuilder();
        for (AbstractToken token : tokens) {
            if (outputLanguage.containsToken(token.getType())) {
                IBuilder builder = outputLanguage.getBuilder(token.getType());
                result.append(builder.tryBuildCode(token));
            } else if (outputLanguage.containsToken("multiline_comment")) {
                IBuilder builder = outputLanguage.getBuilder("multiline_comment");
                result.append(builder.tryBuildCode(new Token("", String.format("Unknown token: %s", token.getType()))));
            }
        }
        return result.toString();
    }
}