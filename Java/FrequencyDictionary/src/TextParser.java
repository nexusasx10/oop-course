import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TextParser {
    public static ArrayList<String> parse(String input) {
        ArrayList<String> words = new ArrayList<>();
        Pattern pattern = Pattern.compile("[a-zA-Z]{3,}");
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()){
            words.add(matcher.group().toLowerCase());
        }
        return words;
    }
}