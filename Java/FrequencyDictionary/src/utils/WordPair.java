package utils;

public class WordPair {
    private String word;
    private Integer count;

    public WordPair(String word, Integer count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return this.word;
    }

    public Integer getCount() {
        return this.count;
    }

    public void incrementCount() {
        this.count += 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WordPair)) {
            return false;
        }
        return this.word.equals(((WordPair) obj).word) && this.count.equals(((WordPair) obj).count);
    }

    @Override
    public int hashCode() {
        return (this.word.hashCode() * 13) ^ this.count.hashCode();
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", this.word, this.count);
    }
}