package utils;

import java.util.Comparator;


public class CountComparator implements Comparator<WordPair> {
    @Override
    public int compare(WordPair a, WordPair b) {
        if (a.getCount().equals(b.getCount())) {
            return a.getWord().compareTo(b.getWord());
        }
        return - a.getCount().compareTo(b.getCount());
    }
}
