import java.util.ArrayList;
import java.util.List;
import utils.WordPair;
import utils.CountComparator;


public class ListDictionary implements IFrequencyDictionary {
    private List<WordPair> collection;

    public ListDictionary(List<WordPair> list) {
        this.collection = list;
    }

    @Override
    public void add(String word) {
        for (WordPair wordPair : collection) {
            if (wordPair.getWord().equals(word)) {
                wordPair.incrementCount();
                return;
            }
        }
        collection.add(new WordPair(word, 1));
    }

    @Override
    public List<WordPair> search(int wordsCount, String prefix) {
        ArrayList<WordPair> result = new ArrayList<>();
        for (WordPair wordPair: collection) {
            if (wordPair.getWord().startsWith(prefix))
                result.add(wordPair);
        }
        result.sort(new CountComparator());
        if (result.size() < wordsCount) {
            return result;
        }
        return result.subList(0, wordsCount);
    }

    @Override
    public void clear() {
        this.collection.clear();
    }

    @Override
    public int size() {
        return this.collection.size();
    }
}
