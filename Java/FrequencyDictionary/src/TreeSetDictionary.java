import utils.WordPair;
import utils.CountComparator;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;


public class TreeSetDictionary implements IFrequencyDictionary {
    private TreeSet<WordPair> collection;

    public TreeSetDictionary() {
        this.collection = new TreeSet<>(new CountComparator());
    }

    @Override
    public void add(String word) {
        for(WordPair pair : collection) {
            if (pair.getWord().equals(word)) {
                collection.remove(pair);
                collection.add(new WordPair(pair.getWord(), pair.getCount() + 1));
                return;
            }
        }
        collection.add(new WordPair(word, 1));
    }

    @Override
    public List<WordPair> search(int wordsCount, String prefix) {
        ArrayList<WordPair> result = new ArrayList<>();
        for (WordPair pair : collection) {
            if (pair.getWord().startsWith(prefix)) {
                result.add(pair);
                if (result.size() == wordsCount) {
                    return result;
                }
            }
        }
        return result;
    }

    @Override
    public void clear() {
        this.collection.clear();
    }

    @Override
    public int size() {
        return this.collection.size();
    }
}