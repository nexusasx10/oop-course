import java.util.List;
import utils.WordPair;

public interface IFrequencyDictionary {
    void add(String word);
    List<WordPair> search(int wordsCount, String prefix);
    void clear();
    int size();
}