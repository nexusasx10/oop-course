import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


public class Program {
    public static void main(String[] args) {
        Class[] collections = new Class[] {
            ArrayListDictionary.class,
            LinkedListDictionary.class,
            HashMapDictionary.class,
            TreeSetDictionary.class
        };
        Scanner scanner;
        StringBuilder lines = new StringBuilder();
        try {
            scanner = new Scanner(new File("src/source.txt"));
            while (scanner.hasNextLine()) {
                lines.append(scanner.nextLine());
                lines.append("\n");
            }
        } catch (FileNotFoundException ex) {
            return;
        }
        ArrayList<String> words = TextParser.parse(lines.toString());
        for (Class collection : collections) {
            ArrayList<Long> adding = new ArrayList<>();
            ArrayList<Long> searching = new ArrayList<>();
            for (int i = 0; i <= words.size(); i += 20000) {
                adding.add(testAdd(collection, words.subList(0, i), 1));
                IFrequencyDictionary dictionary;
                try {
                    dictionary = (IFrequencyDictionary) collection.newInstance();
                } catch (InstantiationException | IllegalAccessException ex) {
                    throw new RuntimeException(); //?
                }
                for (String word : words.subList(0, i)) {
                    dictionary.add(word);
                }
                searching.add(testSearch(dictionary, "a", 10, 1000));
            }
            System.out.println(String.format("Collection: %s", collection));
            System.out.println("Adding:");
            for (Long time : adding) {
                System.out.println(time);
            }
            System.out.println("Searching:");
            for (Long time : searching) {
                System.out.println(time);
            }
        }
    }

    private static long testAdd(Class collection, List<String> words, int testCount) {
        IFrequencyDictionary dictionary;
        long addingTime = 0;
        long beginTime;
        for (int j = 0; j < testCount; j++) {
            try {
                dictionary = (IFrequencyDictionary)collection.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                return 0;
            }
            //System.gc();
            for (String word : words) {
                beginTime = System.nanoTime();
                dictionary.add(word);
                addingTime += System.nanoTime() - beginTime;
            }
        }
        return addingTime / testCount;
    }

    private static long testSearch(IFrequencyDictionary dictionary, String prefix, int limit, int testCount) {
        long searchingTime = 0;
        long beginTime;
        for (int j = 0; j < testCount; j++) {
            //System.gc();
            beginTime = System.nanoTime();
            dictionary.search(limit, prefix);
            searchingTime += System.nanoTime() - beginTime;
        }
        return searchingTime / testCount;
    }
}
