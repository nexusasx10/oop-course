import utils.WordPair;
import utils.CountComparator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HashMapDictionary implements IFrequencyDictionary {
    private HashMap<String, Integer> collection;

    public HashMapDictionary() {
        this.collection = new HashMap<>();
    }

    @Override
    public void add(String word) {
        if (collection.containsKey(word))
            collection.put(word, collection.get(word) + 1);
        else
            collection.put(word, 1);
    }

    @Override
    public List<WordPair> search(int wordsCount, String prefix) {
        ArrayList<WordPair> list = new ArrayList<>();
        for(String word : collection.keySet()) {
            if (word.startsWith(prefix))
                list.add(new WordPair(word, collection.get(word)));
        }
        list.sort(new CountComparator());
        if (list.size() <= wordsCount)
            return list;
        return list.subList(0, wordsCount);
    }

    @Override
    public void clear() {
        this.collection.clear();
    }

    @Override
    public int size() {
        return this.collection.size();
    }
}