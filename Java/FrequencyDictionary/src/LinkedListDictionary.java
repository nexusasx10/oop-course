import java.util.LinkedList;


public class LinkedListDictionary extends ListDictionary {
    public LinkedListDictionary() {
        super(new LinkedList<>());
    }
}
