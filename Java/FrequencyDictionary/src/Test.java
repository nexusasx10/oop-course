import java.util.HashSet;

public class Test {
    public static void main(String[] args) {
        long time = 0;
        for (int j = 0; j < 1000000000; j++) {
            // System.gc();
            long startTime = System.nanoTime();
            time += System.nanoTime() - startTime;
        }
        time /= 1000;
        System.out.println(time);
    }
}
